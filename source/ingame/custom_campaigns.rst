Custom Campaigns
================

.. note::
    Booleans in XMLs need to be either ``True``, ``true``, ``False`` or ``false``.

Custom Campaigns can be loaded into the game by defining events via XML files. These provide you with everything that was made to make the internal campaigns.

All Attributes
--------------

Settings
^^^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Name                  string        | The name that will be given the campaign on the 
                                    | menu.
BarracudaAllowed      boolean       | Whether Barracuda and Caliburn can be selected in the 
                                    | campaign.
CustomShipsAllowed    boolean       | Whether Custom Ships can be selected in the campaign.
Video                 string        | The relative path to a video file to use for the campaign 
                                    | graphic.
FallbackTexture       string        | The relative path to a PNG image to use for the campaign 
                                    | graphic. Only used if a video file isn't provided or available.
===================== ============= ============

Group
^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
PointsToUnlock        integer       | How many points the player needs to unlock the group.
===================== ============= ============

Frontend
^^^^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Name                  string        | The name that will be displayed in the event list.
                                    | menu.
Description           string        | Currently unused.
===================== ============= ============

Awards
^^^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
BronzeValue           float         | The value needed to obtain a bronze medal. For instance, enter ``3`` 
                                    | to award the player a bronze medal for coming in 3rd place.

SilverValue           float         | The value needed to obtain a silver medal. For instance, enter ``2``
                                    | to award the player a silver medal for coming in 2nd place.
GoldValue             float         | The value needed to obtain a gold medal. For instance, enter ``1``
                                    | to award the player a gold medal for coming in 1st place.
PlatinumValue         float         | The value needed to obtain a platinum medal. For instance, enter `1`
                                    | to award the player a platinum medal for coming in 1st place when 
                                    | playing in hard mode.
EasyScore             float         | The target score when in easy mode. Used in gamemodes 
                                    | such as Eliminator.
HardScore             float         | The target score when in hard mode. Used in gamemodes 
                                    | such as Eliminator.
===================== ============= ============

Mode
^^^^
.. note::
    Any parsed attributes that aren't one of the ones listed below will be treated as a custom gamemode attribute. These allow gamemodes that have a static integer declared and then enter the variables name and the value they want it set to.

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Gamemode              string        | The name of the gamemode to load for the event.
ModernPhysics         boolean       | Whether 2280 should be used for the event.
FloorHugger           boolean       | Whether floor hugger should be used for the event.        
                                    | Overrides 2280.
SpeedClass            string        | The speed class to use for the event. ``Toxic``, ``Apex``, ``Halberd``, 
                                    | ``Spectre`` or ``Zen``
===================== ============= ============

Level
^^^^^
Level is part of the levels element which takes multiple level attributes. Providing more then one level attribute will automatically setup a tournament.

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Name                  string        | The name of the track to load.
===================== ============= ============

Modifiers
^^^^^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Hardcore              boolean       | Whether double damage should be enabled.
Weapons               boolean       | Whether weapons should be enabled.
Mirror                boolean       | Whether mirror mode should be enabled.
Hyperspeed            boolean       | Whether hyper speed should be enabled.
Extralaps             integer       | The number of additional laps for the event.
ForcePlayerShip       boolean       | Whether to force the player ship using the ForcedPlayerShip 
                                    | setting below.
ForceAiShip           boolean       | Whether to force the ai ship using the ForcedAiShip   
                                    | setting below.
ForcedPlayerShip      string        | The name of the ship to force the player to fly.
ForcedAiShip          string        | The name of the ship to force the Ai to fly.
ForcedPlayerLivery    integer       | When forcing a player ship, the index of the livery to 
                                    | force.
ForcedPlayerScheme    integer       | When forcing a player ship, the index of the livery
                                    | scheme to force.
BlockShipSet          string        | **Supports multiiple attributes**. When set, will unlock all 
                                    | ship sets and then use every BlockShipSet entry to 
                                    | blacklist ship sets that you don't want to be selected. 
                                    | Valid options are ``Standard``, ``Drag``, ``Drift``, ``Survival`` and 
                                    | ``Custom``
===================== ============= ============

Ai
^^^^^^^^^

===================== ============= ============
Name                  Data Type     Description
===================== ============= ============
Count                 boolean       | The number of AI ships to spawn.
UseSpeedMult          boolean       | Whether the speed multiplier below will be used.
SpeedMult             boolean       | Optional top speed multiplier for the AI.
Difficulty            boolean       | The difficulty level to use for the AI. Valid options are ``Novice``, 
                                    | ``Experienced``, ``Expert``, ``Elite`` and ``Hardcore``. 
                                    | Hard mode will force this to ``Hardcore``
===================== ============= ============

Example
-------
This example is a re-creation of the Reflex campaign from the game. This is an older XML file and doesn't include every attribute listed above, but serves fine as a full example of a campaign. Some attributes have also been changed over the years. It's reccomended you use this as a structure reference only.

.. code-block:: xml

    <Settings Name="Not Reflex" BarracudaAllowed="False" CustomShipsAllowed="False" Video="" FallbackTexture="BNG-1.jpg" />
    <Group PointsToUnlock="0">
    <Event>
        <Frontend Name="Nova Split Time Trial" Description="" />
        <Awards BronzeValue="140" SilverValue="130" GoldValue="120" PlatinumValue="100" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Time Trial" ModernPhysics="False" SpeedClass="Toxic" FloorHugger="True">
            <Levels>
            <Level Name="Nova Split" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="True" Mirror="False" ExtraLaps="0" ForcePlayerShip="True" ForceAiShip="False" ForcedShip="Barracuda"/>
        <Ai Difficulty="Experienced" Count="7" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Arrivon XI Introduction Race" Description="" />
        <Awards BronzeValue="0" SilverValue="0" GoldValue="0" PlatinumValue="0" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Race" ModernPhysics="False" SpeedClass="Toxic">
            <Levels>
            <Level Name="Arrivon XI" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="True" Mirror="False" ExtraLaps="0"/>
        <Ai Difficulty="Experienced" Count="7" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Marina Rush Introduction Race" Description="" />
        <Awards BronzeValue="0" SilverValue="0" GoldValue="0" PlatinumValue="0" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Race" ModernPhysics="False" SpeedClass="Toxic">
            <Levels>
            <Level Name="Marina Rush" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="True" Mirror="False" ExtraLaps="0" ForcePlayerShip="False" ForceAiShip="False" ForcedShip="Gtek" />
        <Ai Difficulty="Novice" Count="7" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    </Group>
    <Group PointsToUnlock="6">
    <Event>
        <Frontend Name="Marina Rush Knockout" Description="" />
        <Awards BronzeValue="0" SilverValue="0" GoldValue="0" PlatinumValue="0" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Knockout" ModernPhysics="False" SpeedClass="Halberd">
            <Levels>
            <Level Name="Marina Rush" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="False" ForceAiShip="False" ForcedShip="Nexus" />
        <Ai Difficulty="Novice" Count="7" UseSpeedMult="False" SpeedMult="1.12" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Nova Split Survival" Description="" />
        <Awards BronzeValue="10" SilverValue="20" GoldValue="30" PlatinumValue="40" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Survival" ModernPhysics="False" SpeedClass="Zen">
            <Levels>
            <Level Name="Nova Split" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="False" ForceAiShip="False" ForcedShip="Gtek" />
        <Ai Difficulty="Novice" Count="0" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Hyperions Arrivon XI Trial" Description="" />
        <Awards BronzeValue="112.16" SilverValue="115.16" GoldValue="118.16" PlatinumValue="115.97" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Time Trial" ModernPhysics="False" SpeedClass="Halberd">
            <Levels>
            <Level Name="Arrivon XI" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="True" ForceAiShip="False" ForcedShip="Hyperion" />
        <Ai Difficulty="Novice" Count="0" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    </Group>
    <Group PointsToUnlock="12">
    <Event>
        <Frontend Name="Arrivon XI Survival" Description="" />
        <Awards BronzeValue="10" SilverValue="20" GoldValue="30" PlatinumValue="40" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Survival" ModernPhysics="False" SpeedClass="Zen">
            <Levels>
            <Level Name="Arrivon XI" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="False" ForceAiShip="False" ForcedShip="Gtek" />
        <Ai Difficulty="Novice" Count="0" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Omnicoms Nova Split Trial" Description="" />
        <Awards BronzeValue="114.08" SilverValue="117.08" GoldValue="120.08" PlatinumValue="117.03" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Time Trial" ModernPhysics="False" SpeedClass="Halberd">
            <Levels>
            <Level Name="Nova Split" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="True" ForceAiShip="False" ForcedShip="Omnicom" />
        <Ai Difficulty="Novice" Count="0" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    <Event>
        <Frontend Name="Marina Rush Survival" Description="" />
        <Awards BronzeValue="10" SilverValue="20" GoldValue="30" PlatinumValue="50" EasyScore="0" HardScore="0" />
        <EventSettings>
        <Mode Gamemode="Survival" ModernPhysics="False" SpeedClass="Zen">
            <Levels>
            <Level Name="Marina Rush" />
            </Levels>
        </Mode>
        <Modifiers Hardcore="False" Weapons="False" Mirror="False" ExtraLaps="0" ForcePlayerShip="False" ForceAiShip="False" ForcedShip="Gtek" />
        <Ai Difficulty="Novice" Count="0" UseSpeedMult="False" SpeedMult="0" />
        </EventSettings>
    </Event>
    </Group>