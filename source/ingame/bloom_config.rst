Bloom Config Tool
=================
.. note::
    To access the tool open the command console (Ctrl + Backspace) and then enter the command ``mod_bloomconfig``. This tool can also be used to export any bloom config from any track, including the internal tracks.

The bloom config tool allows you to generate a .bytes file that contains bloom configuration data for custom tracks. This editor exposes the majority of settings that are exposed in the offical development tools. 

The game uses two bloom post processing effects. The first is used for the light blooming itself and is Unity's own soft bloom. The second is used for lens effects and is Amplify's Bloom library.

Save/Close
----------
* **Save Config**: Save the bloom config to disk. The console will open to show you where the bloom was saved.
* **Close**: Closes the bloom config without saving anything. Your changes will stay active for the duration of the event.

Soft Bloom settings
-------------------
* **Threshold**: The minumum pixel intensity before bloom can kick in. Setting this close to 1 will allow you to only bloom out very bright objects that are almost white.
* **Soft Knee**: Introduces a soft blend to the threshold, allowing you to make darker pixels glow with less intensity.
* **Radius**: How far bright pixels get bloomed.
* **Intensity**: How intense the bloomed pixels are.
* **High Quality**: Whether a higher resolution screen reference should be used when bloomin the image.
* **Anti-Flicker**: Applies a blur to the bloom to stop small bright pixels from flicking bloom.

Advanced Bloom And Lens Settings
--------------------------------
For this section see `Amplify's documentation here <http://wiki.amplify.pt/index.php?title=Unity_Products:Amplify_Bloom/Manual>`_. The ingame editor has been setup to mirror their Unity editor inspector interface.

One thing to note is that the bloom component of Amplify isn't used and will be disabled if you disable lens effects from the options menu. When working on bloom you want to set the intensity value (the slider under the Precision option) to 0.