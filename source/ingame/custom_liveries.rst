.. _custom-liveries:

Custom Liveries
===============

.. note::
	This feature is only available in BallisticNG 1.3 and up.

BallisticNG supports adding addiitional liveries to any ship in the game using PNG texture files. You'll need a template image that contains a UV layout wire to get started.

Livery Templates
^^^^^^^^^^^^^^^^^
We have provided template PSDs for the internal ships to get you started. You can find these in the ``Modding -> Livery Templates`` folder where BNG is installed. These templates make use of Photoshops layer effects to provide non destructive and customizable panel lines, so while you can open these in any software that supports PSD keep in mind you will need Photoshop to get the intended effects.

For custom ships the ships author will need to provide the template image.

Adding a livery
^^^^^^^^^^^^^^^
All liveries go into the ``User -> Mods -> Liveries`` folder. If you're just updating to 1.3 then launch the game and it wil automatically create this folder for you.

To add a livery:

* Create a folder in the Liveries folder named after the ship you want to add a livery to. This is the name that you see on the menu and is case insensitive. So for instance a livery folder for Gtek will be ``User -> Mods -> Liveries -> Gtek``
* Place a PNG file in the new folder and follow the file naming rule below. PNG files are also detected recursively, allowing you to organise things further with sub-folders in the ships livery folder.

File Naming Rule
----------------
Custom liveries support replacing the diffuse, illum and reflection maps. The diffuse map is **required** and the livery will not be registered without it. Illum and reflection maps are however optional and will be ignored if not present, using the default illum and reflection maps for the ship in their place.

The hashes in the names below are placeholders, you replace them with the name of the livery. The name of the livery can include any character valid in a file path, including spaces and more underscores. The first underscore is just used to the split the prefix and display name.

* ``d_#`` is used for the diffuse map
* ``i_#`` is used for the illumination map
* ``r_#`` is used for the reflection map

Updating Liveries
-----------------
To update a livery simply update the texture file, you can do this while the game is running. If you're in a race you will need to restart it to see the changes. If you're on the main menu then you can simply switch to a different livery and then back again.

If you add or remove a texture you will need to refresh the liveries using the ``debug_refreshliveries`` console command. This will run through every ship, rebuild file paths references and then regenerate the liveries list.