.. toctree::
    :caption: In-Game

    installing_custom_content
    layout_creator
    custom_music
    custom_sounds
    custom_campaigns
    custom_liveries
    bloom_config
    flare_config
    virtual_palettes