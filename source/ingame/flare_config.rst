Flare Config Tool
=================
.. note::
    The access the tool open the command console (Ctrl + Backspace) and then enter the command ``mod_flareconfig``. This tool can also be used to export any flare config from a track, including the internal tracks.

Select Flare
------------
This list shows all of the flares in the scene. **Flare Config Flare** is an empty flare created by the tool that you can edit. Select this to begin editing, click the **Move custom flare to camera position** button and then move the camera back to see the flare.

When you are ready to save the flare, click the **Save Flare Config to JSON** button. This will export the flare to ``User -> Export -> Flare Configs -> FlareConfigs -> TrackName_flare.json``.

Edit Flare
----------
See ProFlares documentation `here <http://www.proflares.com/proflares-settings-guide>`_ for more information. The edit flare section has been setup to mirror their Unity editor inspector interface.