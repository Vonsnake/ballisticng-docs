.. _ingame-install-content:

Installing Custom Content
=========================

How you install custom content depends on whether you're manually installing the files or if you're downloading them off of the Steam Workshop. If you're installing off of the Steam Workshop then installation is done automatically for you and you can simply launch the game once the content is downloaded and it will be available.

Manual installation
-------------------
Manual installation allows you to access mods offline as they don't require Steam's APIs to fetch. Another advantage of manual installation is that you can organise your content so the game neatly categorizes everything for you.

All mods go into the User folder, which is found next to the game's executable.

Ships
^^^^^
**Path**: ``User -> Mods -> Ships``

Custom Ships use either the .ship, .shp or .csf format to indicate the V1, V2 and V3 ship formats. The ships folder is categorized so placing files in a sub folder correlates to a category in-game. Leaving ships files in the root of the folder will have them be moved to an Uncategorized folder when the game launches.

Ship categories are named after the folder the files are in.

Tracks
^^^^^^
**Path**: ``User -> Mods -> Tracks``

Custom tracks use either .trk or .wtrk to indicate raw track files and workshop track files. The tracks folder is categorized so placing files in a sub folder correlates to a category in-game. Leaving ship files in the root of the folder will have them be moved to an Uncategorized folder when the game launches.

Track categories are named after the folder the files are in.

| **Track Caches**
| When the game loads a track it must open up the track scene to load its menu data. For large scenes this can be a lengthy process so the game will cache the data for subsequent launches. If a track updates its frontend data you'll need to delete the cache file for the track to refresh it.

Track caches can be found in ``User/Caches/Tracks``

Campaigns
^^^^^^^^^
**Path**: ``User -> Mods -> Campaigns``

Campaigns use .XML for the event definitions and either .MP4, .WEBM or .PNG for the campaign graphic. The campaigns folder is searched recursively so entire folder hiearchies can be made to organise campaign content. If you're distributing a campaign it's recommended that you publish everything in a folder that people can easily drag and drop into the campaigns folder.

Ingame Music
^^^^^^^^^^^^
**Path**: ``User -> Mods -> Ingame Music``

Ingame music can be either .MP3, .WAV, .FLAC or .OGG. Placing music in sub-folders will create playlists for your music that you can select from the options menu. Leaving a song in the root of the directory will make that song part of the global playlist, which ignores all folders and plays everything.

Menu Music
^^^^^^^^^^
**Path**: ``User -> Mods -> Menu Music``

Menu Music needs to be in .WAV format and in a sub folder.

Menu music is layered and there is a strict file naming scheme that you need to use. Only Layer1 needs to be present, the extra layers only get used if they're there. The addiitional layers will be blended in as you progress through the menu.

* Layer1.wav - Base layer
* Layer2.wav - Additional Layer 1
* Layer3.wav - Additional Layer 2
* Layer4.wav - Additional Layer 3

Sounds
^^^^^^
**Path**: ``User -> Mods -> Sounds``

Custom sounds need to be in .WAV format and in a sub folder. The sub folder defines the sound pack which you select from the options menu. For file names, see the in-game soundpack helper menu.

Code Mods
^^^^^^^^^
**Path**: ``User -> Mods -> Code Mods``

Code mods are either raw .cs files or pre-compiled libraries and are placed in a sub folder. If providing a pre-compiled library, the file name **must** be the same name as the folder it's in. If letting the game compile an assembly from the raw .cs files, the resulting DLL will automatically take the folders name.

------------

External.txt files
-------------------
External.txt files allow you to have mods sit externally to an installation of BallisticNG. This is particuarlly useful if you want to run multiple versions of the games and have them all pull mods from the same folder, or link the game to custom music that sits externally to the game.

Setup
^^^^^
Every mod type requires it's own external.txt. 

| For instance an external.txt for ships would be ``User -> Mods -> Ships -> external.txt``
| For tracks it would be ``User -> Mods -> Ships -> external.txt``
| Custom music would be ``User -> Mods -> Ingame Music -> external.txt``
| and so on...

Each line of the external.txt file will be read as a directory, so paste each folder you want the game to see per line. You don't need any special characters, just the raw path. If you're on Wndows, the path you can copy out of the Explorer address bar is what you want.

Folder Interpretation
^^^^^
How each folder is interpreted changes a bit per mod type. There are two types of interpretations:

Direct
______
Folders of this type are treated as are. The game will blindly find all files in the directory tree. This is used for things such as custom music, where each folder is its own playlist.


Categories
__________
Folders of this type read the sub-directories as categories. For instance if you have custom ships in an external folder, you'd have a sub-directory of the external folder called ``Cool Ships`` and place any ships you want to show up in the menu under ``Cool Ships`` in here. Same behaviour as the internal folder, basically.

Here's a list of supported folders and their interpretation types:

.. csv-table::
	:file: tables/table_external_types.csv
	:header-rows: 1