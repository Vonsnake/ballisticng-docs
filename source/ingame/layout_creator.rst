Layout Creator
==============

This documentation only covers the new Layout Creator 2.0. There are currently no plans to provide documentation for the legacy layout creator as it's now an unsupported tool.

Introduction
------------
The Layout Creator is an in-game tool that lets you quickly build and playtest new track layouts in realtime. The editor uses splines to generate the track so all you need to do is place down nodes and move them about to get creating. This page covers everything you need to know from basic camera controls all the way up to exporting the track for use in the Unity Tools and modeling software.

Camera
------
The camera is controlled using the keyboard and mouse and uses a first person shooter control scheme.

| **FPS control**
| To take control of the camera hold down the right mouse button. With it held down move the mouse to look around and use WSAD to move around, Q and E to move the camera vertically and shift to speed up the camera speed. You can also use the mouse wheel to adjust the camera's speed.

| **Orbit control**
| Hold the middle mouse button down with nodes selected to orbit around them. W and S can be used to dolly in and out. This mode should be more comfortable for people who work in CAD software a lot.

Get to Grips
------------
Before you begin using the tools it's a good idea to understand what the tools are doing to build your track in the first place.

The layout creator uses splines to generate the track. A spline is a path that's built from nodes, which the path will travel through. You can create as many paths as you want, these are referred to as routes in the editor.

Routes by default will snap to each other so you can move the entrance or exit of a route near the side of another path and it will automatically create an entrance/exit junction for you! Routes can also be either open or closed. By default they're open. A closed route will connect the end back to the start.

Teminology
----------
There are several terms that you will come across when learning how to use the Layout Creator.

| **Node**
| A node is a point on the track that the route will travel through. These are represented in the editor as the spheres that you interact with.

| **Route**
| A route is a collection of nodes that build up a route on your track. Routes can be detached from each other or connected through the use of junctions.

| **Junction**
| A junction is a bit of track where two routes connect together. These are automatically generated for you when you move the entrance to the side of a piece of track that's not in the same route.

| **Active Route**
| The active route is the route that you are currently editing. When you select a node it changes the active route to the route of the node that you select.

| **Tangent**
| Tangents are points relative to the node that define how the track flows through that node. These can be adjusted to increase the tracks tension and angles of flow.


Reference Image Plane
---------------------
The reference image plane shows an image that you can load to display in the editor, which is useful if you've already got a layout that you've drew and want to trace over. The file path to the image is saved in your CTL file so make sure you don't move the file if you want it to be present again the next time to open the layout.

* To load an image navigate to ``File -> Open`` and then select a PNG image. To update the image, open it again.
* To remove the reference image navigate to ``Edit -> Clear Reference Image``

See the hotkey reference below or in-game for the reference image plane controls.

Play Mode Settings
------------------

Editing Modes
-------------
The layout creator has two editing modes:

| **Splines Mode**
| Splines mode is where you will spend most of your time. It's the main mode of the editor and is where you edit splines.

| **Physics Mod Zones Mode**
| Physiics Mod Zones mode allows you to configure physics mod zones so you can test out physics modifications as you develop your layout.

Physics Mod Zones created in this mode can be exported to XML to then be imported directly into the Unity Tools or other CTL files.

Save XML: ``File -> Export Mod Zones to XML``
Import XML: ``File -> Open -> Select XML File``

Helper Tools
------------
This section covers the more hidden features of the editor. For a reference on everything, see the hotkey reference below as it contains descriptions for everything.

| **Close Route**
| Press C to toggle whether a route is closed. When a close, the route will wrap back around on itself to close a circuit.

| **Straight Locking**
| Straight locking allows you to use a node as a line lock to aid in creating off-grid straights. To create a lock reference select a node and then press L and then move another node. Press L again to disengage the lock.

| **Section Links**
| This is a feature that doesn't really have any particular purpose but could be handy in some very specific scenarios if you want to close a track that uses multiple routes for the main circuit.

Select a node on one route and the node on another and then press V. The last segment on the first route you selected will now connect to the first segment on the second route that you selected. To remove the link, see the route tools window section below.

Node Tools Window
^^^^^^^^^^^^^^^^^
**Path**: ``Window -> Node Tools``

The node tools window provides you with several functions that arn't accessible through hotkeys.

| **Track Shape**
* **Reset Buttons**: Allows you to reset the entire shape or parts of the track profile shape for the selected nodes.
* **Mirror**: Allows you to mirror the track shape from one side to the other for the selected nodes.
* **Copy/Paste Shape**: Allows you to copy a track shape and then paste it onto other nodes.
* **Shape Interpolation**: Allows you to control the method of interpolation used for calculating the track shape between each node. There are a lot of different modes, `See Here <https://easings.net/>`_ for an interactive demo for some of them.

| **Precision Tools - Height**
* **Height To Zero**: Resets the selected track nodes height to zero.
* **Store Height**: Stores the height of the first node in the selection and stores it in the height field to the right.
* **Paste Height**: Copies the height from the hieght field and pastes it onto every selected node.
* **Avg Height**: Averages the height of every selected node and then applies it.

| **Precision Tools - Tilt**
* **To Zero**: Resets the selected track nodes tilt to zero.
* **To 180**: Sets the selected track nodes tilt to 180 degrees. Useful if you're designing upside down sections of track.
* **Tilt by 180**: Tilts the selected track nodes tilt by 180 degrees.
* **Apply Custom Tilt**: Copies the tilt value from the tilt field to the left and pastes it onto all selected nodes.
* **Tilt Buttons**: Provides a way to rotate nodes in 5, 45 and 90 degree increments.

| **Precision Tools - Tangents**
* **Reset Yaw**: Resets the yaw of the selected nodes tangents.
* **Reset Pitch**: Resets the pitch of the selected nodes tangents.
* **To Yaw / To Pitch**: Copies the value from the tangent rotation field to the left and pastes it onto the select nodes tangents.

Route Tools Window
^^^^^^^^^^^^^^^^^^
**Path** ``Window -> Route Tools``

The route tools window provides you several options to control how routes work and their ordering.

| **Scene Settings**
* **Junction Snapping Threshold**: Sets the minimum distance before track track segments snap together to make junctions.

| **Route Ordering**
| This section allows you to re-order or delete routes. There's nothing much else to it really as the route order only changes the order of how the TRM mesh is generated.

| **Route Settings**
* **Auto-Snap Entrance**: Whether the routes entrance should snap to nearby track segments.
* **Auti-Snap Exit**: Whether the routes exit should snap to nearby track segments.
* **Entrance Smoothing**: How many track segments away from a snapped junction at the entrance should be smoothened out.
* **Exit Smoothing**: How many track segments away from a snapped junction at the exit should be smoothened out.
* **Remove Link**: Removes a route link if there is any.

Hotkey Reference
----------------
These hotkeys are also available for viewing inside the editor itself. See ``Help -> Hotkey Reference``.

General Tools
^^^^^^^^^^^^^

============================ ================ ==================
Action                       Hotkey           Description
============================ ================ ==================
Toggle Grid Snap             G                | Toggles nodes snapping to the grid.
Decrease Grid Size           , (comma)        | Decreases the grids size.
Increase Grid Size           . (period)       | Increases the grids size.
Set/Remove Straight Lock     L                | Sets the selected node as the straight lock reference
                                              | or removes the reference if a straight lock is already 
                                              | defined.

Undo                         Ctrl + Z         | Undoes the last action.
Redo                         Ctrl + Y         | Redos the last undone action.

Toggle Play Mode             F2               | Toggles between editor and play mode.
============================ ================ ==================

Camera
^^^^^^

============================ ================ ==================
Action                       Hotkey           Description
============================ ================ ==================
Move Forward                 W                | Moves the camera forward.
Move Backwards               S                | Moves the camera backwards.
Move Left                    A                | Moves the camera left.
Move Right                   D                | Moves the camera right.
Move Down                    Q                | Moves the camera down.
Move Up                      E                | Moves the camera up.
Speed Up                     Left Shift       | Speeds up the cameras movement.
Change Speed                 Scroll Wheel     | Adjusts the cameras global movement speed.
Orbit Selection              Middle Mouse     | Orbits the camera around the selected nodes.
============================ ================ ==================

Reference Image Plane
^^^^^^^^^^^^^^^^^^^^^

============================ ================================ ==================
Action                       Hotkey                           Description
============================ ================================ ==================
Move on X/Z                  R + Space + Mouse                | Moves the reference image on the X and Z 
                                                              | axes.
Move on Y                    Left Shift + R + Space + Mouse   | Moves the reference image on the Y axes.
Scale                        Left Shift + R                   | Scales the reference image on both axes. 
Lock Transform               Double Tap R                     | Locks the reference image in place so you 
                                                              | can't accidently move it.
============================ ================================ ==================

Selection Tools
^^^^^^^^^^^^^^^

============================ ====================== ==================
Action                       Hotkey                 Description
============================ ====================== ==================
Select                       Left Click             | Selects the node that's under the cursor. Drag the 
                                                    | mouse with left click held to marquee select.
Add To Selection             Shift + Left Click     | Adds the node under the cursor to the current 
                                                    | selection. Also works when marquee selecting.
Remove From Selection        Ctrl + Shift Click     | Removes the node under the cursor from the 
                                                    | current selection. Also works when marquee 
                                                    | selecting.
Clear Selection              Backspace              | Clears the current selection. Marquee selecting 
                                                    | empty space will also clear the current selection.
============================ ====================== ==================

Node Tools
^^^^^^^^^^

============================ ===================== ==================
Action                       Hotkey                Description
============================ ===================== ==================
Add Node                     A                     | Adds a new node at the end of the active 
                                                   | route.
Insert Node                  S                     | Inserts a new node in the active route. 
                                                   | Uses the first node in the selection and 
                                                   | inserts the new node between it and the 
                                                   | next node in the route if there is one.
Delete Node                  D                     | Deletes the selected nodes. Deleting every 
                                                   | node in a route will delete the route. 
                                                   | Deleting a single node will select the last 
                                                   | node in the route the deleted node is a part 
                                                   | of.
Add Route At Mouse           Shift + A             | Creates a new route at the mouses position
                                                   | and rotates it to travel away from the
                                                   | camera.
Add Node In Place            Ctrl + A              | Adds a new node to the active route
                                                   | at the mouse cursors position.

Move on X/Z                  Space                 | Moves the selected nodes along the
                                                   | X and Z axes relative to the cameras
                                                   | rotation.
Move on Y                    Shift + Space         | Moves the selected nodes along the Y
                                                   | axis.
Move to cursor               Ctrl + Space          | Moves the selected node to the mouse
                                                   | cursors position.

Tilt                         T + Mouse             | Adjusts the tracks tilt through the
                                                   | selected nodes.
Tilt 90 Degrees              Shift + T             | Increases the selected nodes tilt by
                                                   | 90 degrees.
Reset Tilt                   Ctrl + T              | Resets the selected nodes tilt to zero.

Adjust Tangent Distance      F + Mouse             | Adjusts the selected nodes bezier
                                                   | tangent positions.
Adjust Tangent Yaw           Shift + F + Mouse     | Adjusts the selected nodes bezier
                                                   | tangent yaw.
Adjust Tangent Pitch         Ctrl + F + Mouse      | Adjusts the selected nodes bezier
                                                   | tangent pitch.

Toggle Left Wall             [                     | Toggles the left wall for the selected
                                                   | nodes.
Toggle Right Wall            ]                     | Toggles the right wall for the selected
                                                   | nodes.
Toggle Side Connection       P                     | Toggles side snapping for the selected
                                                   | nodes. This allows you to create routes
                                                   | that run alongside each other.
Toggle Maglock               M                     | Toggles the magnetic lock for the
                                                   | selected nodes.
Toggle No Tilt Lock          O                     | Disables 2159 tilt locking on the
                                                   | selected nodes.
Toggle Allow Out Of Bounds   / (foward slash)      | Disbles OOB detection and invisible
                                                   | walls for the selected nodes.
Toggle Jump                  J                     | Toggles jump mode for the selected nodes.
                                                   | This creates a break in the track.
Toggle Teleporter            ; (semi-colon)        | Toggles Teleporter mode for the selected
                                                   | nodes. Works the same as the Jump mode.
Flattern To FIrst Selection  H                     | Requires multiple nodes to be selected.
                                                   | Takes the height of the first node in
                                                   | the selection and copies it to all other
                                                   | nodes in the selection.
============================ ===================== ==================

Track Shape Tools
^^^^^^^^^^^^^^^^^

============================ ========================== ======================================================================
Action                       Hotkey                     Description
============================ ========================== ======================================================================
Modify Left Floor Width      Q + Mouse                  | Adjusts the horizontal offset of the 
                                                        | left floors outer vertex.
Modify Left Floor Height     Shift + Q + Mouse          | Adjusts the vertical offset of the 
                                                        | left floors outer vertex.
Modify Left Wall Width       Ctrl + Q + Mouse           | Adjusts the horizontal offset of the 
                                                        | left walls top vertex.
Modify Left Wall Height      Shift + Ctrl + Q + Mouse   | Adjusts the vertical offset of the 
                                                        | left walls top vertex.

Modify Right Floor Width     E + Mouse                  | Adjusts the horizontal offset of the 
                                                        | right floors outer vertex.
Modify Right Floor Height    Shift + E + Mouse          | Adjusts the vertical offset of the 
                                                        | right floors outer vertex.
Modify Right Wall Width      Ctrl + E + Mouse           | Adjusts the horizontal offset of the 
                                                        | right walls top vertex.
Modigy Right Wall Height     Shift + Ctrl + E + Mouse   | Adjusts the vertical offset of the 
                                                        | right walls top vertex.
============================ ========================== ======================================================================

Route Tools
^^^^^^^^^^^

============================ ================ ==================
Action                       Hotkey           Description
============================ ================ ==================
Toggle Entrance Snap         Shift + [        | Toggles the junction snapping function for the active routes 
                                              | entrance.
Toggle Exit Snap             Shift + ]        | Toggles the junction snapping function for the active routes 
                                              | exit.

Toggle Route Closed          C                | Toggles whether the active route is closed. A closed route
                                              | will loop back round to its first selection.
Set Section Link             V                | Lets you toggle a continue link to another route. 
                                              | Select the node in one route, another node in a different 
                                              | route and press V to have the first route connect its exit 
                                              | to the seconds start.
============================ ================ ==================

Physics Mod Zone Tool
^^^^^^^^^^^^^^^^^^^^^

============================ ================ ==================
Action                       Hotkey           Description
============================ ================ ==================
Add Zone                     A                | Adds a new zone to the layout.
Delete Selected Zone         D                | Deletes the selected zone from the layout.
Move Zone on X/Z             Space            | Moves the zone on the X and Z axes.
Move Zone on Y               Shift + Space    | Moves the zone on the Y axis.
Rotate Zone                  R + Mouse        | Rotates the zone along its yaw.
Scale On All Aes             S + Mouse        | Scales the zone all three axes.
Scale on X                   S + X + Mouse    | Scales the zone on its X axis.
Scale on Y                   S + Y + Mouse    | Scales the zone on its Y axis.
Scale on Z                   S + Z + Mouse    | Scales the zone on its Z axis.
============================ ================ ==================

CTL Programmer Reference
------------------------
Want to make your own tool that can import or export a CTL file? Here's the CTL specification!

CTL (Custom Track Layout) Specification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. note::
    * Strings have a leading byte that define how many bytes long the string is.
    * Booleans are written as bytes and are expressed as either 0 or 1.
    * Section link indicies are stored as you enumerate through the routes. Wait until you have loaded all routes before you assign them. If there isn't a link, this value will be stored as -1.

.. code-block:: csharp

    // code init (not for reading the file. this is what your code should be doing)
    int32[] sectionLinkIndicies

    // header
    int32 version

    // config
    bool autoJunctionThreshold

    if (version > 5)
        int32 hyperSpeedEnabled
        int32 speedClass
        int32 speedMode
        int32 physicsMode
        if (version > 7) bool meshFloorEnabled
        
        string referenceImagePath
        if (version > 6) bool referenceIsLocked

        float32 referenceXPos
        float32 referenceYPos
        float32 referenceZPos
        float32 referenceWidth

    // track configuration
    int32 routeCount
    for routeCount

        // route header
        bool connectEntrance
        bool connectExit

        int32 autoJunctionSmoothEntrance
        int32 autoJunctionSmoothExit

        int32 linkIndex
        linkIndex.add(continueIndex)

        // spline data
        bool splineClosed

        int controlPointCount

        for controlPointCount
            float32 posX
            float32 posY
            float32 posZ

            float32 rotX
            float32 rotY
            float32 rotZ
            float32 rotW

            bool sideConnect
            bool maglock

            if (version > 8) bool noTiltLock
            if (version > 9) bool allowOob
            if (version > 1) bool isJump

            bool leftWall
            bool rightWall
            
            if (version > 2) float32 tangentDistance
            if (version > 3) float32 tangentYaw
            if (version > 4) float32 tangentPitch

            float32 leftFloorExtentX
            float32 leftFloorExtentY
            float32 leftFloorExtentZ

            float32 leftWallExtentX
            float32 leftWallExtentY
            float32 leftWallExtentZ

            float32 rightFloorExtentX
            float32 rightFloorExtentY
            float32 rightFloorExtentZ

            float32 rightWallExtentX
            float32 rightWallExtentY
            float32 rightWallExtentZ


    




