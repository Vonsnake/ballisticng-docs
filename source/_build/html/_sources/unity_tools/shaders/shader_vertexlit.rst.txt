Vertex Lit Shader
=====================
**Shader Path**: ``BallisticNG -> VertexLit``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.