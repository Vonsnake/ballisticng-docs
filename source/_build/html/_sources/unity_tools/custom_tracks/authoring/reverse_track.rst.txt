Reverse Track Tools
===================
The reverse track tools are a set of tools that help you to export a reversed TRM and then quickly transfer forward track data into a reverse track scene. To get a reverse track setup, follow these steps:

* Open your track scene
* Navigate to ``BallisticNG -> Utilities -> Export Reversed TRM``
* Save the TRM somewhere in your Unity project. **DO NOT** have it in the same folder as the forward TRM as the prefab data will clash. Putting it in a sub-folder next to the original TRM is however fine.
* Save your track scene as a new copy with a different name, such as ``Track Name Reverse``
* Open your new track scene and navigate to ``BallisticNG -> Utilities -> Reverse TRM Transfer Tool``
* Assign your forward track scene to the Forward Scene field
* Setup the transfer settings. Auto Flip Tiles will flip all tile UVs so they match the forward versions direction. Transfer Track Atlas will transfer the forward tracks textures over

Your reverse track is now partially setup. Section data cannot be transfered so you will now need to go into the section editor to configure the track with routes and any flags you want active. The tool should have also transfered most of the textures correctly, but you may want to give the track a look over to manually fix anything that isn't correct.