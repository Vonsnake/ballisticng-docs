.. _unity-tools-scene-refs:

Scene References
================

The scene references object is a script that contains important configuration data for your track. This script is referenced for both ingame loading and cache file generation and as such is used to configure not only references and track environment settings, but also what's displayed on the menu.

To quickly access the scene references script, navigate to ``BallisticNG -> Track Configuration -> Scene Referecnes``. As it's a script attached to a gameobject, you will need to make sure you have an inspector tab open to see it.

Fog And Weather settings
************************
Fog Settings
^^^^^^^^^^^^
Fog is a post processing effect that's applied in-game only. BallisticNG uses linear fog so you manually define the start and end distances. If you want to rapidly tweak the values to preview them it's reccomended that you use the :ref:`unity-tools-fast-play` feature.
This is a legacy option and is no longer required as you can use Unity's fog settings in the lighting tab.

    ======================      ===========================================================================
    Setting                     Description
    ======================      ===========================================================================
    Use Fog                     | This setting controls whether fog will be enabled on this track or not. 
                                | Leave this disabled to have the game use the Unity scene fog settings.
    Fog Color                   The color that the fog will use.
    Fog Start/End Distance      | The start/end distances where the fog will be 
                                | fully transparent and fully opaque.
    ======================      ===========================================================================

Weather Settings
^^^^^^^^^^^^^^^^
The weather setting allows you to select a prefab weather controller to use instead of having to create your own with :ref:`unity-tools-weather-controller`. Leave this set to none if you want to create your own weather controller.

    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    None                        No weather effects will be used.
    Rain                        Standard rain with ambient rain sounds.
    Snow                        Standard snow with no ambient sounds.
    Thunder                     Standard rain with ambient rain sounds and ocassional thunder strikes.
    ======================      ============================================================================

Track Config
************
    **All Lights Are Baked**: Marks that all lights in the scene are baked and will disabled when the scene is loaded ingame. If you have a lot of lights this will help boost performance slightly.

    **Allow Mixed Pads**: Allows both tile and 3D pads to be used at the same time. With this disabled 3D pads will be removed unless you're playing in 2280.

    **Type**: Controls what ships are selectable on the menu and makes speed adjustments automatically.

    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    Normal                      Standard track.
    Drag                        Drag track that forces the drag speed multiplier.
    Drift                       Drift track. Now a redundant option and not needed.
    Precision                   The track will show up as a precision run.
    ======================      ============================================================================

    **Bloom**: Controls the bloom profile for the track. This is split into bloom type and custom bloom config options. Custom bloom config allows you to use the ingame bloom config tool to build a bloom settings file and assign it here. If you do this make sure you set bloom type to none, as having it set to anything other then none will override your custom config.

    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    Wet                         Suited for overcast/rainy tracks.
    Snowy                       Suited for bright tracks with lots of white.
    Dim                         Suited for dark tracks.
    Bright                      Suited for bright tracks.
    Neon                        Suited for dark tracks with lots of bright colors.
    ======================      ============================================================================

    **Physics Mode**: Sets which physics mode that will be used on this track. Modern will override Classic. Floor Hugger will override Modern if enabled as a cheat.

    **Point To Point Track**: Sets whether the track is a point to point track. When enabled the mid line will be used as the finishing line.

    **Disable Anti-Skip**: Globally disables the anti-skip system. On pre-1.0 tracks this is enabled by default and is disabled by default on new post-1.0 tracks.

    **Survival Ignores Track**: Sets whether the virtual environment will ignore the TRM track and not automatically configure materials for it.

    **Music Override**: Allows you to define an audio clip to replace ingame music with. The name of the file will be the name of the track displayed ingame. It's reccomended you use a wav and enable streaming in the import settings for a seamless loop and no fame interupts when the file is being fully loaded.

    **Blocked Weapons**: A list of weapon that will be blocked from rotation for this track. Names must be the API names, which you can find out by using the *give* console command ingame. The only offical track to make use of this option is Kuiper Overturn.

AI Settings
***********
    **Ai Look Ahead**: Controls how many sections ahead the AI look when steering and airbraking on a per-speed class basis. If you're using a standard TRM with the default 2 units per section then you shouldn't have to play with these as these options are primarily designed for custom tracks built with the TRM editor where the distances between sections might not be standard.

    **Ai Speed Multiplier**: Controls the global top speed of the AI for the track. This can be useful for making small adjustments if your layout is too easy or hard but it is strongly advised that you leave it and only use it if you absolutely have to.

Scene Data
**********
    **Hide Checkpoint Visuals**: If enabled this will disable the checkpoint laser meshes so you can create your own.

    **Skybox Material**: A reference to the Unity skybox material if you're not using a mesh skybox. For legacy reasions the game will always use a black background when the scene loads even if you set a skybox in Unity's lighting tab, so make sure you assign this on top of Unity's lighting tab skybox.

    **Welcome to Intercom**: A welcome to line to play when the track loads.

Menu Data
*********
    **Track Description**: The description that will be displayed on the menu.

    **Track Image**: The track image to display on the menu. This should be a 900x300 image with read/write enabled in the import settings.

Frontend Data
*************
    **Track Location**: The location of the track that will be displayed on the load screen.

    **Loading Screen Background**: The image to use on the loading screen. This can be any resolution you want but for reference, internal tracks use 1920x1080.

    **Frontend Layout Override**: An override to set the mesh that will show as the track layout on the menu.

Generic References
******************
    **Countdown Displays**: The countdown display objects in the scene. This is used to position, rotate and scale the actual countdown display that's spawned in-game.

    **Culled Objects**: A list of manually defined objects to cull if using the retro distance option. Use this for cases where you're not using a shader that supports BNGs retro settings.

Events
******
Events are calls to scripts that you can setup directly in the Unity editor. The use case for these are pretty generic so there's a wide range of things you can do with them.

    **On Countdown Three**: Triggered when the countdown hits three.

    **On Countdown Two**: Triggered when the countdown hits two.

    **On Countdown One**: Triggered when the countdown hits one.

    **On Countdown Go**: Triggered when the countdown hits go.

    **on Event Complete**: Triggered when the race has finished.