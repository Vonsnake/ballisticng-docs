Assets API Introduction
=========================

The assets API is a wrapper around Unity's asset bundle system to remove the complexities of building and then loading them. The API provides you with a way to pack Unity assets into an NGA file with names and then load that NGA file in-game by simply telling the API to load it.

To make use of this API you will need to use Unity. :ref:`unity-tools-install-update`.

Assets are stored inside of an Assets Container. For prefabs containing objects you'd like to reference, you can use :ref:`unity-tools-custom-prefab`. More information on both of these is available on the next page.

Usages
-------
The Assets API is vital for code mods that wish to load their own assets on the fly. The asset could be anything: a mesh, a texture, a sound, a font, an entire object hiearchy, etc.

Note that all assets for your mod should be packaged in the same file, because an NGA file contains multiple files and you load objects using their name.