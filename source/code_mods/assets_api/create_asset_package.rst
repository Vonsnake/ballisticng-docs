Create Asset Package
====================

Asset Packages are the NGA files that you export which contain your asset data. The asset package contains an asset container that stores references to the assets inside and then correlates them to a name that you define.

Assets Container
----------------

Asset Containers are Unity assets that store references to the assets you want to pack into an NGA file. To create a container:

* Right click somewhere in the Unity project tab
* Navigate to ``BallisticNG -> Create New Mod Assets Container``
* Rename the assets file it creates to something meaningful

Now select the file and in the inspector you will see an assets list and build button. Click the green **Add** button to add an asset to the list. From here you will then want to enter a name and assign an object to the object field. Note that this doesn't support assets from a scene and you need to assign assets from the project tab. Any kind of asset is supported (textures, meshes, sounds fonts, etc).

If you'd like to use game objects, drag the object out of the scene hiearchy tab into the project tab to prefab it. This prefab can then be assigned to your assests list. If you'd like to edit the prefab then double click it to enter the prefab edit mode. This will place the prefab into a temporary scene where you can edit it as if it was a game object in a real scene.

Custom Prefab
-------------
See: :ref:`unity-tools-custom-prefab`

Custom Prefabs allows you to use a Unity component to reference other components and game objects that are part of a prefab. This makes it very easy to fetch objects out of a prefab you've exported. This is very important for assets like custom weapons.

Make sure the Custom Prefab component is attached to the top level object in the prefab. This will allow you to use the assets API to directly fetch the object in a single line of code, instead of having to fetch its parent object and then fetch it manually.

Export NGA File
---------------
To export an NGA file, select your assets container and then click the **Build** button. This will ask you to tell it where to save the file to and then will compile the package for you.

If you already have a code mod setup, the best idea would be to export it directly to its folder.
