Code API
========

.. note::
	The Assets Api is under the namespace ``BallisticUnityTools.AssetApi``

ModAssets
---------
The ModAssets class is the bulk of the API and what you will use to do everything.

.. code-block:: text

	ModAssets Load(string filePath)
	-------------------------------
	Loads the assets of an NGA file.

	filePath        : The absolute or relative file path to the NGA file that you want to load.

	Usage Example
		ModAssets assets = ModAssets.Load("MyPackage.nga")

	void Purge(ModAssets assets, bool unloadAllLoadedAsets)
	-------------------------------------------------------
	Purges all the assets of a mod assets package from memory.

	assets					: The mod assets to Purge.
	unloadAllLoadedAssets	: Whether all loaded assets should be unloaded. If this is false, only assets that arn't being used will be unloaded.

.. code-block:: text

	string[] GetAssetNames()
	------------------------
	Returns the names of all assets in a loaded ModAssets file.

	object GetAsset(string name)
	----------------------------
	Gets an asset from a loaded ModAssets file. This will return it as an object, you will need to cast it to whatever data type it's supposed to be.

	name			: The name of the asset to load.
	
	T GetAsset<T>(string name)
	--------------------------
	Gets an asset from a loaded ModAssets file as the provided type of T.

	name			: The name of the asset to load.

	T GetComponent<T>(string name, bool createIntance)
	-------------------------------
	Gets an asset from a loaded ModAssets file, assumes it is a game object and then attempts to get a component of type T from it.

	name			: The name of the asset to load.
	createInstance	: Whether an instance of the object should be created when it's loaded, otherwise you will refering to the original object which only exists in memory and not as an actual object in the game.

CustomPrefab
------------
The CustomPrefab class allows you to return objects declare in custom prefabs. First off you will need to fetch a custom prefab from an NGA file, which you can do like this:

.. code-block:: csharp

	ModAssets assets = ModAssets.Load("MyPackage.nga")
	CustomPrefab prefab = assets.GetComponent<CustomPrefab>("MyPrefabName", true);

.. code-block:: text

	object GetObject(string n)
	--------------------------
	Returns an object from the prefab as a generic object. You will need to cast it to whatever data type it's supposed to be.

	n			: The name of the object to get from the prefab.

	T GetObject<T>(string n)
	------------------------
	Returns an object from the prefab as the provided type of T.

	n				: The name of the object to get from the prefab.