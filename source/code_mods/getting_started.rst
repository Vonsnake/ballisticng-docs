.. _code-mods-getting-started:

Getting Started
===============
.. note::
    See :ref:`ingame-install-content` for details on code mod pathing.

    Offical code mod examples are available here: https://github.com/bigsnake09/ExamplesNG

Library vs raw CS files
-----------------------
Code mods can be provided as either pre compiled libraries or by creating cs files in your code mods folder. The ingame C# compiler uses .NET 3.5 so it's reccomended you create pre compile libraries as you'll have access to the latest C# features with .Net 4.6.


Setup a development environment
-------------------------------

Install Visual Studio
^^^^^^^^^^^^^^^^^^^^^
* Download the Visual Studio Community Installer: https://visualstudio.microsoft.com/
* Run the installer and make sure you tick **.NET Desktop Development** and **Game development with Unity**
* Let Visual Studio install and then open it

Create Visual Studio Project
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
With Visual Studio installed and opened we can now create a project:

* Navigate to ``File -> New -> Project``
* On the left hand side make sure you have **Visual C#** selected
* In the middle make sure **Class Library (.NET Framework)** is selected 
* At the bottom make sure **Framework** is set to **.NET Framework 4.6**
* Set your projects location and name and then click OK

Link BallisticNG Dlls
^^^^^^^^^^^^^^^^^^^^^
In order to start programming for BallisticNG your project must link to the game's libraries so Visual Studio knows that your code is actually supposed to be referencing something.

On the right hand side of the screen you will see a panel called the **Solution Explorer**.

* In the solution explorer expand your project
* Right click the **References** option entry and then cllick **Add Reference**
* On the window that pops up click browse
* Navigate to your BallisticNG folder, the **BallisticNG_Data** folder and then the **Managed** folder
* Multi-select **Assembly-CSharp.dll**, **BallisticSource.dll** and **BallisticUnityTools.dll** and then open
* Click browse again
* From the same **Managed** folder, select every DLL that contains **UnityEngine** in the name and then open
* Make sure all items in the reference window are ticked and then click OK

That's our references in, but now we need to tell Visual Studio not to export them with our built code mod:

* Under the **Solution Explorer** you will see a properties tab
* In the **Solution Explorer** expand the **References** entry and then select every reference
* In the **Properties** panel set **Copy Local** to **False**

Visual Studio might freeze for a moment as it procceses all of the DLLs.

Build/Install Code Mod
^^^^^^^^^^^^^^^^^^^^^^^
To build your code mod all you need to do is right click the project from the **Solution Explorer** and then click **Build**. This will output a DLL to the ``bin/Debug`` where the project is located.

To install the code mod, create a new folder in your ``BallisticNG/User/Mods/Code Mods`` folder with the same name as the DLL file and then copy the DLL into that folder. To update your mod, just replace the DLL file in that folder (make sure the game isn't running when you do).
