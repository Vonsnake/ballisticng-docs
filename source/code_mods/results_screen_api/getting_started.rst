Results Screen Introduction
============================
The results screen API provides a way for code mods to generate their own result screens.

The API provides you with a generator class which you can use the build the interface and then update elements with new information later on if needed.

How it works
------------
The results screen UI is drawn using Unity's UI system. This means you could generate anything that Unity supports into the interface, but BNG provides you with helper methods to handle all of the elements you should need.

The interface is layed out using a cursor. The cursor defines the top left of every element and it starts at the top left of the results screen window. When you add in a new element it'll jump the cursor to the bottom right of the new element that was created.

Several methods are provided to control the cursor to enable more advanced UI layouts, which will be covered later on.