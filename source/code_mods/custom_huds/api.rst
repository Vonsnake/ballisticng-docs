Custom HUD API
==============

Using Custom Huds For Custom Gamemodes
---------------------------------------

You can use the custom HUD API to load in your HUDs standalone. The procedure is the same as if you were loading in vanilla huds but you instead call: 

.. code-block:: text

	ScriptableHud[] SceneHudManager.CreateNewHud<T>(AssetBundle bundle, string assetPath)
	--------------------------------------------------------------------------------------
	This will load a custom HUD and fully configure it ready to be used in a gamemode.

	T                : The component type to load. This needs to be your HUD class.
	bundle           : The loaded asset bundle to load the HUD prefab from
	assetPath        : The path to the asset This needs to be the full relative path in your Unity Project

	Usage Example
		ScriptableHud[] speedometerHuds = SceneHudManager.CreateNewHud<MySpeedometer>(MyHudMod.HudBundle, "assets/my huds/my example hud/speedometer.prefab");

Override API
------------

ScriptableHud
^^^^^^^^^^^^^
Scriptable Hud is what all of your HUD classes need to derive from. This class handles all of the internal behaviours for you so all you have to worry about is the how the specifics of your HUD elements work. 

**Variables**

.. csv-table::
	:file: tables/vars_scriptablehud.csv
	:header-rows: 1

**Methods**

You override these. Make sure you leave the base.MethodName call inside as these will be performing important tasks.
ScriptableHud is a monobehaviour so all of Unity's monobehaviour functions are available to you.

.. csv-table::
	:file: tables/methods_scriptablehud.csv
	:header-rows: 1

.. code-block:: text

	RectTransform GetById(string id)
	--------------------------------
	This will try to find a component by an ID and return the rect transform component. If it doesn't find anything it will return null.

	id        : The ID of the component to load

	Usage Example
		RectTransform scoreTransform = GetById("ScoreDisplay");

.. code-block:: text

	T GetById<T>(string id)
	-----------------------
	This will try to find a component by an ID and return a component of the specified type. This allows you to easily grab specific components without having to do any of the casting yourself. If it doesn't find anything it will return null.

	T        : The component type to load.
	id       : The ID of the component to load.

	Usage Example
		Text scoreDisplay = GetById<Text>("ScoreDisplay"); 

SceneHudManager
^^^^^^^^^^^^^^^
A scene hud manager is where you register your HUDs into the gamemode. You need to create a scene hud manager for each gamemode you want to override the HUD for. 

.. code-block:: text

	virtual void OnCreateHuds()
	---------------------------
	Called when your manager needs to create it's HUDs.

	Usage Example
		public override void OnCreateHuds()
		{
			RegisterHud<MySpeedometer>(MyHudMod.HudBundle, "assets/my huds/my example hud/speedometer.prefab");
			RegisterInternalHud(BallisticNG.RaceUi.InterfaceLoader.Huds.WrongWayDisplay);
		}

.. code-block:: text

	ScriptableHud[] RegisterHud<T>(AssetBundle assetBundle, string assetPath)
	---------------------------------------------------------
	Registers and loads a new HUD into the current gamemode. Returns the loaded huds per player.

	T              : The class type component to load
	assetBundle    : The loaded asset bundle to load the HUD prefab from
	assetPath      : The path to the asset This needs to be the full relative path in your Unity Project

	Usage Example
		RegisterHud<MySpeedometer>(MyHudMod.HudBundle, "assets/my huds/my example hud/speedometer.prefab");

.. code-block:: text

	void RegisterInternalHud(string name)
	-------------------------------------
	Registers and loads an internal HUD into the current gamemode. See the BallisticNG.RaceUI.InterfaceLoader.Huds class for the entire list of names.

	name        : The name of the HUD to load

	Usage example
		RegisterInternalHud(BallisticNG.RaceUi.InterfaceLoader.Huds.WrongWayDisplay);

CustomHudRegistry
^^^^^^^^^^^^^^^^^

The Custom Hud Registry is where you register your mod and your mods HUD content to the game.

First off make sure you have an ID for your mod. It's reccomended you store this in a constant so you can reference it everywhere and only have to edit the constant if you want to update the mod's ID everywhere. 

.. code-block:: text

	void RegsterMod(string mod)
	---------------------------
	Registers your mod into the UI. This will allow people to select it from the hud style option in the gameplay menu.

	mod        : The ID of your mod. This doubles up as the display name so make sure the ID is something people will recognise.

	Usage Example
		CustomHudRegistry.RegisterMod("MyHudMod");

.. code-block:: text

	void RegisterSceneManager(string gamemodeId, string mod, SceneHudManager manager)
	---------------------------------------------------------------------------------
	Registers a scene manager into the game. This is what will spawn your HUD when the game asks you to.

	gamemodeId        : The gamemode ID that the HUD manager will be responsible for.
	mod               : The ID of your mod. Needs to be the same as what you provide to RegisterMod
	manager           : An instance of your scene manager.

	Vanilla gamemode IDs are "Race", "Time Trial", "Speed Lap", "Survival", "Knockout", "Eliminator", "Upsurge, "Track Creator", "Precision" and "Practice".

	Usage Example
		CustomHudRegistry.RegisterMod("Race", "MyHudMod", new MyHudModRaceManager());

.. code-block:: text

	void RegisterWeaponSprite(string weaponId, string mod, Sprite sprite)
	---------------------------------------------------------------------
	Registers a weapon sprite override into the game.

	weaponId        : The weapon ID that the sprite will be assigned to. 
	mod             : The ID of your mod. Needs to be the same as what you provide to RegisterMod
	sprite          : The sprite itself.

	Vanilla weapon IDs are "rockets", "missile", "mines", "plasma", "energywall", "cannon", "shield", "autopilot", "emergencypack", "tremor", "turbo", "hunter" and "hellstorm"

	Usage Example
		string basePath = Path.Combine(ModLocation, "Icons");
		CustomHudRegistry.RegisterWeaponSprite("rockets", "MyHudMod", LoadSpriteFromDisk(Path.Combine(basePath, "Weapon_Rockets.png"));

.. code-block:: text

	Sprite LoadSpriteFromDisk(string filePath)
	------------------------------------------
	Loads a jpg or png from disk and loads it into memory as a sprite.

	filePath        : The absolute file path to the image you want to load.

	Usage Example
		string path = Path.Combine(ModLocation, "MySpriteImage.png");
		Sprite mySprite = CustomHudRegistry.LoadSpriteFromDisk(path);

.. code-block:: text

	Sprite GetWeaponSprite(string weaponId, bool fallbackToIngame)
	--------------------------------------------------------------
	Returns a loaded weapon sprite for the active HUD mod. Will return null if othing is found.

	weaponId                : The weapon ID to get the sprite for. See above for a list of vanilla IDs
	fallbackToIngame        : Whether to fallback to the game's vanilla sprites if a sprite isn't found

	Usage Example
		Sprite weaponSprite = CustomHudRegistry.GetWeaponSprite("rockets", true);

.. code-block:: text

	Sprite GetIngameIcon(string weaponId)
	-------------------------------------
	Returns an ingame weapon sprite. This is also called by GetWeaponSprite if no sprite is found while fallbackToIngame is set to true. Will return null if nothing is found.

	weaponId        : The weapon Id to get the sprite for. See above for a list of vanilla IDs

	Usage Example
		Sprite weaponSprite = CustomHudRegistry.GetIngameIcon("rocets");