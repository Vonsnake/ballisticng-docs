BallisticNG Code Documentation
==============================

An outline of BallisticNG's code base is available for viewing. This includes summary comments and will be kept up to date with the latest major releases.

This includes everything from inside of the Unity project but excludes some external libraries like the Unity Tools. Editor tools are included in this which are obviously not included with the built game, so do keep that in mind.

https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/namespaces.html