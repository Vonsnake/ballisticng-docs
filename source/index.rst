.. BallisticNG documentation master file, created by
   sphinx-quickstart on Wed Jun 17 16:10:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BallisticNG's documentation!
=======================================
Welcome to the offical modding documentation for BallisticNG, the ultimate AG modding sandbox! This documentation is currently a work in progress so there's a lot missing right now, but the goal is for this to eventually cover everything around the game's modding in detail.


Useful Links
------------
* `Neognosis Workflow Tools (Download And Documentation) <https://github.com/bigsnake09/Neognosis-Workflow-Tools>`_

.. Unity Tools Section

.. toctree::
   :hidden:
   :maxdepth: 100
   :caption: Unity Tools

   unity_tools/index

.. toctree::
   :hidden:
   :maxdepth: 100
   :caption: In-game

   ingame/index
   
.. toctree::
   :hidden:
   :maxdepth: 100
   :caption: Code Mods
   
   code_mods/index


