What's New?
===========

1.3.3
^^^^^

**Unity Tools**

* Flare occluders no longer have a warning about no mesh colliders. The warning is now for having a mesh collider. (the game has automatically handled this since 1.1).
* Fixed AnimationTrigger not working when `Override Play State` is disabled.
* Fixed array access errors in color and float scroller animation script editors which made them unusable.
* Added ability to change 3d pad offset in the scene view when placing them by holding CTRL + Shift and click dragging the mouse.
* Added generic triggers. See:
  
  * :ref:`unity-tools-trigger-generic-custom`
  * :ref:`unity-tools-trigger-generic-delayed`
  * :ref:`unity-tools-trigger-generic-objectevents`
  * :ref:`unity-tools-trigger-generic-particlesstop`
  * :ref:`unity-tools-trigger-generic-print`
  * :ref:`unity-tools-trigger-generic-repeater`

1.3.1
^^^^^

**Unity Tools**

* Added ``Survival Sky Mesh`` component. (:ref:`unity-tools-survival-toggles`)
* Added Harpstone 23 Tile Set textures

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Tiles -> Harpstone``

1.3
^^^^

**Game**

* Added support for custom liveries (:ref:`custom-liveries`)
* Added support for custom virtual/survival palettes (:ref:`virtual-palettes`)
* Added in-game campaign editor
* Added external folders for mod loading (:ref:`ingame-install-content`)

**Layout Creator**

* Now uses Control instead of Command on Mac.
* Added ship speed option
* Added tangent rotation snapping. This defaults to 15 degrees and is enabled alongside grid snapping. Tangent rotation snapping is relative to the track direction.
* Added option to set the track refresh rate. The default refresh rate has been increased from 15fps to 60fps
* Added orthographic camera modes. See the hotkey reference for more information
* Added node scaling tool, bound to the S key. This will scale nodes away/towards from the average position of all selected nodes.
* Added node orbit tool, bound to the R key. This will orbit nodes around the average position of all selected nodes.
* Added route options to offset the reference end points for connections. These default to 4 on new tracks and will load as 0 on tracks saved before this version
* Added a new junction connection smoothing mode. This will be enabled by default for any new route created and can be toggled in the route tools window.
* Added options to copy shape data when appending a new node and interpolate when inserting. Both are enabled by default
* Moved Insert Segment hotkey to I key
* Moved reference image hotkey to W key
* Grid snapping now defaults to 2 units.
* Grid and rotation snapping can now be set using the new input fields at the top of the editor.
* Grid snapping now supports decimal values
* Section generation, connection generation and connection processing are now separated into three different passes to prevent geometry processing conflicts
* Implemented section data pooling to improve track rebuilding performance 
* Improved handling of section transforms near the end of a closed route
* The editor will no longer respond to play mode toggle inputs until the ship has been spawned
* Reworked the node move tool to use plane ray intersections so movement is based on how the mouse cursor moves in the 3d space
* Using the move to cursor tool now supports multiple nodes. Grid snapping is now also only applied to the average position of the moved nodes, and straight locking is no longer applied.
* The camera now starts with a much faster movement speed
* Fixed an issue where routes with a single node would create connections to sections near the world origin
* Fixed input fields not being deselected when ending an edit
* Fixed a slow memory leak caused by materials not being purged from memory after exiting play mode


**Unity Tools**

* (Ships) Added support for animated characters on ships. (:ref:`unity-tools-charactersetup`)
* (Ships) Added options to control whether ship airbrakes are drawn when in the cockpit and external camera modes. (:ref:`unity-tools-airbrakessetup`)
* (Ships) Added numerous ship stats (:ref:`unity-tools-ship-physics-stats`)
* (Ships) Added an import interface when loading ship stats from an XML
* (Ship) Reworked the scale reference system to work dynamically off of asset files provided in the project. 
* (Tracks) Added `Small Vehicles` track type mode
* (Tracks) Added sunshaft options 
* (Tracks) Added :ref:`unity-tools-fpt` component
* (Tracks) Added :ref:`unity-tools-srl` component
* (Tracks) Added :ref:`unity-tools-dustcontroller`, :ref:`unity-tools-dustzone`, :ref:`unity-tools-dustsurface` and associated assets.
* (Tracks) Added instanced lighting support to VLM 2.0 (:ref:`unity-tools-vlm`)
* (Tracks) Added :ref:`unity-tools-turbotrigger` component
* (Tracks) Added additional 3D pad variants (glow and projector)
* (Tracks) Added :ref:`unity-tools-shader-trackpadanimated` shader
* (Tracks) Added ``billboards_3`` texture atlas and material
* (Tracks) Added template light bridge additive material (``BallisticNGTools -> BallisticNG Assets/Reverse Bridge/Materials/ReverseLightBridgeAtlas```)
* (Tracks) Added additional options for camera introduction paths (:ref:`unity-tools-trackintropans`)
* (Tracks) Added built in fog support to the terrain shaders
* (Tracks) Added :ref:`unity-tools-shader-terrain-advanced-max` shader
* (Tracks) Added :ref:`unity-tools-holofield`
* (Tracks) Added various small vehicle assets ``BallisticNGTools -> BallisticNG Assets -> Small Vehicles``
* (Tracks) Added lots of a new tile textures from Maceno Island and the small vehicle tracks
* (Tracks) Added new features to the :ref:`unity-tools-water-shader`
* (Tracks) Improved :ref:`unity-tools-scene-refs` inspector GUI
* (Tracks) Improved the :ref:`unity-tools-3d-pad-editor`
* (Tracks) Fixed track tiles being forgotten on MacOS due to file pathing issues
* (Tracks) Fixed survival scripts having no affect on Billboard Sprite objects
* (Tracks) Fixed TRM import creating multiple folders if the TRM file is reimported

1.2.5
^^^^^
* NgAudio exposed to Lua scripting (:ref:`unity-tools-lua-api`)
* Model B stats XML updated to reflect changes in-game
* Reflection mask texture for reverse light bridges added
* The source .blend file for the new reverse light bridges is now included in the game's modding folder

1.2.4
^^^^^
* Added Model C Track Lua Script
* Added :ref:`unity-tools-survival-colorizer`
* Added illumination tint property to :ref:`unity-tools-standard-shader`
* Added support for locking liveries behind campaigns (see :ref:`custom-ships-liveries`)
* billboards_2.png updated with Protonic advert texture
* Optimized the Ui rendering for the standard shader
* Rewrote the reverse track creation tools into a single streamlined tool (:ref:`custom-tracks-reverse-track`)
* Source code documentation now includes the BallistcModding, BalllisticSource, BallisticUI and BallisticUnityTools libraries
* Fixed the lightmapper not recieving shadows from objects if they're not using the default layer

1.2.3
^^^^^
* Added :ref:`unity-tools-custom-ships` documentation
* Added :ref:`unity-tools-custom-prefab`
* Added :ref:`codemods-asset-api` documentation
* Finished Shaders documentation
* Started documentation for :ref:`codemods-custom-huds`
* Fixed the textures panel on the track editor overlay extending until it reaches the vertical height of the screen
* The vertex lightmapper is now 32-bit and supports baking light information into tangents
* Added :ref:`unity-tools-water-assets`
* Added :ref:`unity-tools-shader-terrain-advanced` and :ref:`unity-tools-shader-terrain-advanced-plus`
* Added wobble scale option to hologram shaders
* Added Neognosis Unity Tools backend. Right now there is only one tool, which is :ref:`neo-tools-surface-placer`
* Added ``BallisticNG -> Build Active Track Scene`` menu item
* Added ``Vertex Color Blend Material.blend`` file to the game's modding folder
* Implemented Lua Scripting for custom tracks. See :ref:`unity-tools-luaintro` for more information.
* Added :ref:`unity-tools-luarunnner` for running Lua scripts
* Added Lua Enter/Exit call triggers to :ref:`unity-tools-trigger-empty` and :ref:`unity-tools-projectile-trigger-zone`

1.2.2
^^^^^
This releases fast play is incompatible with previous game builds. You **must** be using the 1.2.2 Unity Tools to use fast play with BallisticNG 1.2.2 and up.

* Fixed viewport lockup when setting a teleporter zones target section index below zero
* Fixed the track editor window not scaling to fit the scene view
* The tile flag modes (wet, shadow, swooshable) are now paint modes instead of click toggle (:ref:`unity-tools-track-editor`)
* Added Ai Always Wall Avoid section flag (:ref:`unity-tools-track-editor`)
* Added Override Ai Target Next section next reference (:ref:`unity-tools-track-editor`)
* Added :ref:`unity-tools-oatz`
* Added :ref:`unity-tools-fv`
* Added Local To Track direction option to push zones (:ref:`unity-tools-push-zone`)
* Added Utah Dam Tile Set textures

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Tiles -> Utah Dam``

* Added Virtual 2 tile set textures

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Tiles -> Virtual 2``

* Added Ai Difficulty option to :ref:`unity-tools-fast-play`
* Reworked ship editor prefabs to read from XMLs stored in the Unity project directory

1.2.1
^^^^^
* Added precision track option. See type option under track config in :ref:`unity-tools-scene-refs`
* Added :ref:`unity-tools-hrtf`
* Added :ref:`unity-tools-author-times`

1.2
^^^

**VLM**

* Rewritten from scratch and made open source. See :ref:`unity-tools-vlm`

**Fast Play**

* Updated ship list to reflect 1.2's ship roster

**Track Tools**

* Added :ref:`unity-tools-ref-probe-gen`
* Added pick tool to the atlas editor. Hold down control and left click a tile to piick a texture and make it the active painting texture
* Added place object tool
* Added No Ai Fly Backwards, For Teleporter, No Maglock Sounds, Ai Ignore Junction, Lock Until Close w/ customizable distance and No Hover Position Correction section flags
* Added blocked weapon list option to scene references
* Added all lights are baked option to scene references
* Added frontend layout override option to scene references
* Added allow mixed pads option option to scene references
* Added custom music override option to scene references
* Added :ref:`unity-tools-racing-line-editor-handle-mode`
* Removed the unused weapon/spawn replace tile tags from the atlas editor

**Ship Tools**

* Removed a duplicate of the engine fires option from the :ref:`unity-tools-ship-editor`
* Implemented flare rotates with ship option for ship engines
* Implemented per livery option to override the material that will be used
* Implemented :ref:`unity-tools-ship-editor-user-data`

**Assets**

* Added physics mod zone XML files for Port Ares / Kuiper Overturn presets. These can also be imported into the layout creator.

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Physics Zones``

* Added the new thruster trail meshes

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Ships -> Ship Trails``

* Added new engine sounds for the Model B, Model C, Caliburn and Orbitronix Ships

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Ships -> Default Engine Sounds``

* Added Outer Reaches and Metal Frame tile textures and registered them into the tile database tool

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Tiles``

* Added survival TRM atlas textures. These can be used in conjunction with the **Survival Track Surface** script to extend the survival floor visuals wherever you like.

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Tiles -> Survival -> Combined``

* Added weather example assets

    * Found in ``BallisticNGTools -> BallisticNG Assets -> Weather Example``

* Updated the shader advert texture atlases

**Shaders**

* All shaders have been updated to support retro effets inside of the editor. Go to ``Edit -> Preferences -> BallisticNG`` to toggle them
* Added :ref:`unity-tools-standard-shader`
* Added affine mapping options to all shaders
* Added :ref:`unity-tools-ship-irid-shader`
* Added :ref:`unity-tools-hologram-shader`
* Updated :ref:`unity-tools-terrain-shader` with a UV scale distance blending option
* Updated :ref:`unity-tools-cloud-shader` with a write to depth buffer fix
* Updated :ref:`unity-tools-water-shader` with fog support


**Components**

* Added :ref:`unity-tools-sprite`
* Added :ref:`unity-tools-vacuum-zone`
* Added :ref:`unity-tools-teleporter-zone`
* Added :ref:`unity-tools-projectile-trigger-zone`
* Added :ref:`unity-tools-push-zone`
* Added :ref:`unity-tools-nfgz`
* Added :ref:`unity-tools-damage-zone`
* Added :ref:`unity-tools-weather-controller`
* Added :ref:`unity-tools-tp`
* Added use depth buffer option to lens flare
* The Ballistc Physics Toggler script now uses 3 checkboxes to set whether an object is enabled per physics mode instead of only allowing it to be activated for one physics mode.

    * This hides the original script. The original will continue to be used until you manually remove it and replace it with the new script, which shares the same add component path.
* Track pads now have exposed events that trigger when they are activated and deactivated. An option to customize the illumination intensity material property name has also been added.
* Collider add component path has been renamed to Ballistic Collider
