.. _unity-tools-luaintro:

Getting Started
===============

.. note::
    Although per frame functions are provided you should refrain from using them unless you really need them.
    **Too much code running per frame can cause garbage collection which will cause periodic stutter. Some code being ran is fine but do prefer built in Unity Tools components when they're available.**

.. note::
    Lua scripts are sandboxed and do not have access to anything outside of the game. Everything in Lua is manually exposed so there may be things missing. If there's something you'd like added to the Lua API, let Vonsnake know on the BallisticNG discord.

Please also see :ref:`unity-tools-lua-performance`.

As of BallisticNG 1.2.3 it is now possible to write Lua scripts to create custom behaviours for your tracks. These allow code to be run per frame, on global race events and by being triggered by :ref:`unity-tools-trigger-empty` and :ref:`unity-tools-projectile-trigger-zone` components.

Learn By Example
----------------
See the Lua Scripts folder in your Unity Project's Assets folder for a hands on look at some pre-made scripts to showcase some stuff you can do.

Create a Lua script
--------------------
* In Unity's project view right click and navigate to ``Create -> Lua Script``. If you right clicked a folder the script will be added to that folder, otherwise the script is created in the folder you are currently looking at.
* Rename the new script file to something meaningful, you are going to need to reference it in a moment.

Create Lua Runner
--------------------
All Lua scripts will be ran from a Lua Runner, which is a script component you attach to a gameobject.

To create a runner:

* Create a new gameobject in your track scene ``GameObject -> Create Empty``
* In the inspector for the new gameobject, click **Add Component** and then attach the :ref:`unity-tools-luarunnner` script. See the link for the add component path.
* Either drag drop your Lua script file from the project view onto the **Lua Script** option or click the circle icon to the right of it and then browse for the file.

Your Lua script will now run with the track. Build the track (:ref:`unity-tools-fast-play` reccomended) and have fun!

Debugging Lua Scripts
--------------------
If there are any issues with your Lua Scripts, you will either know straight away as the game will break or any errors will be output to the game's debug console, accessible via Ctrl + Backspace.

Any print() calls from your Lua code will also be logged in the debug console.

.. note::
    The debug console can become slow with large amounts of logs and it will also impact the game's performance outside of viewing as the UI rebuilds itself per log. Before uploading your track to the public, make sure you remove any print calls and fix any issues that might be throwing errors!

So what exactly can I do with Lua?
----------------------------------
While Lua scripting isn't as open as Code Mods, there's still plenty you can do with scripting. Here's a couple of ideas:

* Create a custom countdown screen using prcoedual animation
* Create custom objects that give ships weapons (random or one of your choice). You might also want to hook into the game's API to determine specific situations to give a weapon in
* Play a looped sound and change its properties, like pitch, in realtime
* Detect when the player has started a new lap to make changes to the track in some way
* Detect when Gtek spawns and eliminate them instantly because they were going to win anyway

