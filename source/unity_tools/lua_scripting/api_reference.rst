.. _unity-tools-lua-api:

API Reference
=============
.. note::
    Although per frame functions are provided you should refrain from using them unless you really need them.
    **Too much code running per frame can cause garbage collection which will cause periodic stutter. Some code being ran is fine but do prefer built in Unity Tools components when they're available.**

Reference Variables
-------------------
Reference variables are included in every script to access basic data about the script.

    **gameObject**: The game object that is holding the script.

    **transform**: A reference to the transform of the game object that is holding the script.

    **scriptName**: The file name of the script that's being run.

Static Calls
-------------
These allow you to direct access various parts of the Unity and BallisticNG code base. You access them by using the names as are. So for instance if I want to access Unity's Mathf Lerp function, I will call ``Mathf.Lerp()``

Mathf
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Mathf.html>`_

Physics
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Physics.html>`_

RenderTexture
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/RenderTexture.html>`_

RenderSettings
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/RenderSettings.html>`_

Object
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Object.html>`_

GameObject
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/GameObject.html>`_

Color
^^^^^^
`Exposed Unity API. Click here for offical documentation. <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Color.html>`_

Color32
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Color32.html>`_

Camera
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Camera.html>`_

Vector2
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector2.html>`_

Vector2Int
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector2Int.html>`_

Vector3
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector3.html>`_

Vector3Int
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector3Int.html>`_

Vector4
^^^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector4.html>`_

Quaternion
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Quaternion.html>`_

Matrix4x4
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Matrix4x4.html>`_

Texture2D
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Texture2D.html>`_

Sprite
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Sprite.html>`_

Material
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Material.html>`_

Time
^^^^^^
`Exposed Unity API. Click here for offical documentation <https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Time.html>`_

TrackData
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_bn_g_1_1_track_data_1_1_track_data.html>`_

Use TrackData.Instance to access the current running instance of the track data object, which is where you can access everything.

RaceManager
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_race_manager.html>`_

Use RaceManager.Instance to access the current running instance of the race manager object, which is where you can access everything.

You can also use RaceManager.CurrentGamemode to get access to the loaded gamemode. The returned gamemode will always be the parent class, so if you want to access variables for a specific gamemode you will need to use reflection (see API below) to access those.

Ships
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/struct_game_data_1_1_ships.html>`_

Race
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/struct_game_data_1_1_race.html>`_

NgAudio
^^^^^^^
`Exposed BallisticNG API Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_audio_helpers.html>`_

To get an audio clip you will need to use the GetAudioClip method that takes the internal and custom sound strings manually. The static string arrays have two entries (1st is internal, 2nd is custom). 

Layers
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/struct_game_data_1_1_layers.html>`_

LayersIds
^^^^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/struct_game_data_1_1_layer_i_ds.html>`_

PickupRegistry
^^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/struct_ballistic_n_g_1_1_pickups_1_1_pickup_registry.html>`_

NgMenuContentManager
^^^^^^
`Exposed BallisticNG API. Click here for offical documentation <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_source_1_1_interface_1_1_menu_u_i_1_1_managers_1_1_ng_menu_content_manager.html>`_

Use _ContentManager.Instance to access the current running instance of the content manager, which is where you can access everything.

Api
^^^^^^
Dedicated API class for working in LUA.

GamePaused()
...........
Returns if the game is paused or not.

GetType(obj)
.........
Allows you to return the type of an object. This is only for when you're working with non Python types. You probably won't need this.

GetInstanceField(obj, fieldName)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. note::
    This uses reflection and is slow. Use it sparingly.

Attempts to find a field on an object instance and returns its value. This can be used to return values from inherited data types that arn't exposed to the Lua API but have parents that are, such as gamemodes.

    **obj**: The object to search in.

    **fieldName**: The name of the field to search for.

GetField(obj, fieldName)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. note::
    This uses reflection and is slow. Use it sparingly.

Attempts to find a static field on an object and returns its value.

    **obj**: The object to search in.

    **fieldName**: The name of the field to search for.

GetInstanceProperty(obj, propName)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. note::
    This uses reflection and is slow. Use it sparingly.

Attempts to find a property on an object instance and returns its value. This can be used to return values from inherited data types that arn't exposed to the Lua API but have parents that are, such as gamemodes.

    **obj**: The object to search in.

    **propName**: The name of the property to search for.

GetProperty(obj, propName)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. note::
    This uses reflection and is slow. Use it sparingly.

Attempts to find a static property on an object and returns its value.

    **obj**: The object to search in.

    **propName**: The name of the property to search for.

AddComponent(gameObject, type)
..............................
Adds a component to a game object.

    **gameObject**: The game object to add the component to.

    **type**: The name of the component to add to the object. Valid types are MeshFilter, MeshRenderer, Light, Camera, LineRenderer, TrailRenderer, ParticleSystem, Rigidbody,  BoxCollider, SphereCollider, CapsuleCollider, MeshCollider and TrackPad

GetComponent(gameObject, type)
..............................
Gets a component attached a game object.

    **gameObject**: The game object to get the component from.

    **type**: The name of the component to get from the object. Valid types are MeshFilter, MeshRenderer, Light, Camera, LineRenderer, TrailRenderer, ParticleSystem, Rigidbody, BoxCollider, SphereCollider, CapsuleCollider, MeshCollider and TrackPad

Easing
^^^^^^^
Provides easing functions for interpolation. There are a lot so this is only an outline. All of these take a from, to and T parameter, for instance ``Lerp(from, to, t)``

* Lerp
* QuadIn
* QuadOut
* QuadInOut
* QuadOutIn
* CubicIn
* CubicOut
* CubicInOut
* CubicOutIn
* QuartIn
* QuatOut
* QuatInOut
* QuatOutIn
* QuintIn
* QuintOut
* QuintInOut
* QuintOutIn
* SineIn
* SineOut
* SineInOut
* SineOutIn
* ExpoIn
* ExpoOut
* ExpotInOut
* ExpoOutInt
* CircIn
* CircOut
* CircInOut
* CircOutIn
* ElasticIn
* ElasticOut
* ElasticInOut
* ElasticOutIn
* BackIn
* BackOut
* BackInOut
* BackOutIn
* BounceIn
* BounceOut
* BounceInOut
* BounceOutIn