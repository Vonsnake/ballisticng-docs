.. _unity-tools-lua-functions:

Functions Reference
===================
.. note::
    Although per frame functions are provided you should refrain from using them unless you really need them.
    **Too much code running per frame can cause garbage collection which will cause periodic stutter. Some code being ran is fine but do prefer built in Unity Tools components when they're available.**


Global Scope
------------
Any code written in the global scope (outside of functions) will be ran, so you can use the top of the script to initialize data.

Awake()
------
Called once on track scene load. This is called before anything else an before any useful race data will be available. Use this for initialization if you want to keep it outside of the global script scope.

Start()
-------
Called once on track scene load, after Awake.

Update(unscaledDeltaTime, deltaTime)
------------------------------------
Called once per rendered frame. Use this for animations.

    **unscaledDeltaTime**: How much time there was between the last frame and the current one.

    **deltaTime**: How much time there was between the last frame and the current one, scaled by the game's timescale. This will be zero when the game is paused.

FixedUpdate(deltaTime)
----------------------
Called once per physics tick, which is 200 times a second. **Only use this if you absoluetely must, such as doing raycasts. This is a very heavy function to run under Lua.**

    **deltaTime**: The tick rate of the physics. If you've animating by time, multiply your values by this to make your animation framerate independant.

OnShipEnter(ship)
-----------------
Called when a :ref:`unity-tools-trigger-empty` is triggered by a ship entering it.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information

OnShipExit(ship)
-----------------
Called when a :ref:`unity-tools-trigger-empty` is triggered by a ship leaving it.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information

OnBehaviourEnter(behaviour)
---------------------------
Called when a non ship entity triggers the script. Vanilla game case is with :ref:`unity-tools-projectile-trigger-zone`

    **Behaviour**: The behaviour interface that triggered the call.

OnBehaviourExit(behaviour)
--------------------------
Called when a non ship entity triggers the script. Vanilla game case is with :ref:`unity-tools-projectile-trigger-zone`

    **Behaviour**: The behaviour interface that triggered the call.

OnCountdownStart()
------------------
Called when the race countdown has started.

OnCountdownStageTriggered(stage)
--------------------------------
Called when a new stage of the race countdown has started.

    **Stage**: The new stage the countdown is at. (3, 2, 1)

OnCountdownEnd()
----------------
Called when the race countdown has finished and the race has begun.

OnAllShipsSpawned()
-------------------
Called once every ship has been spawned in.

OnEventComplete()
-----------------
Called when the event has been completed by the player.

OnShipSpawned(ship)
-------------------
Called when a new ship has just spawnked.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information

OnShipExploded(ship)
--------------------
Called when a ship is eliminated from the race.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information

OnShipHitSpeedPad(ship, pad)
----------------------------
Called when a ship hits a 3D speed pad.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information
    **Pad**: The track pad object that the ship hit.

OnShipHitWeaponPad(ship, pad)
------------------------------
Called when a ship hits a 3D weapon pad.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information
    **Pad**: The track pad object taht the ship hit.

OnShipHitSpeedTile(ship, tile)
------------------------------
Called when a ship hits a speed track tile.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information
    **Tile**: The track tile that the ship hit.

OnShipHitWeaponTile(ship, pad)
------------------------------
Called when a ship hits a weapon track tile.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information
    **Pad**: The weapon pad container for the tile the ship hit.

OnShipFinished(ship)
--------------------
Called when a ship finishes the race.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information  

OnShipTeleported(oldPos, oldRot, ship)
--------------------------------------
Called when a ship has just teleported.

    **OldPos**: The old position of the ship.
    **OldRot**: The old quaternion rotation of the ship.
    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information

OnShipLapUpdate(ship)
--------------------
Called when a ship has started a new lap.

    **Ship**: The ship that triggered this call. See `Ship Refs <https://vonsnake.gitlab.io/BallisticNG-ModdingReferences/class_ship_refs.html>`_ for more information