User Variables
==============
.. note::
    Although per frame functions are provided you should refrain from using them unless you really need them.
    **Too much code running per frame can cause garbage collection which will cause periodic stutter. Some code being ran is fine but do prefer built in Unity Tools components when they're available.**
    
User Variables allow you to create data that you can feed your scripts from the Unity scene. This allows you to pump in references to objects, components and other useful data that you might need.

.. note::
    User variables can also be useful for prefabing your lua runner objects for reuse with different settings across your tracks.

Create a variable
------------------
To create a variable, click the green **Add** button in the :ref:`unity-tools-luarunner` script. You can add as many as you like, and of course use the buttons in the header of each variable entry to move and delete variables.

     **Name**: This is the name of your variable both for the UI and the script. For example if you set this name to **mySetting**, you will also access this variable as **mySetting** in the script itself.

     **Type**: This is the type of data that your variable is. There are a lot of types to choose from but most of time you are likely only going to need Integer, Float, Boolean and Transform.

     **Value**: The value for your variable. If this is a number, toggle or name you enter it here. If you're referencing an object, you will need to drag the object or choose from the object picker to assign it.

Declare User Variables
----------------------
You can declare user variables in the script file itself, allowing you to click the **Create Variables From Script** button to automatically populate the variables list with everything the script will expect.

To do this you need to use the var comment. Syntax is as follows:

``-- var name type``

Replace **name** with the name of your variable and **type** with the data type. Data types are the ones you see in the type list without the spacing that Unity adds to them.