.. _unity-tools-install-update:

Install / Update
================
.. note::
    If you're updating a pre 1.2 project, follow the Scripting Runtime Version install step below before updating the project.

.. note::
    When moving between Unity packages and versions, it's reccomended that you make a backup of your Unity project, local and/or remote source control with git would be ideal. If something goes wrong you can restore your backup, or rollback to your last commit with git.

    If something goes wrong, **do not** save anything as this will wipe any saved data that can be restored. Try restarting Unity to see if it fixes your problem. If not, you'll want to rollback to a backup.
    
The Unity Tools, as the name implies, are a set of tools that extend Unity to create content for BallisticNG.

------------

Upgrading to 1.3
*******
As of BallisticNG 1.3 the game now runs on Unity 2020.3.5. There's been structural changes to the Unity Tools so to upgrade existing projects please follow the instructions below carefully.

**It's very important that you do not save anything in Unity until you have finished the upgrade procedure. Saving while the project is in a mid-upgrade state will break your assets. Remember to make a backup.**

* Download and install Unity 2020.3.5. The links for this are below. **Do not launch your Unity project(s)**.
* Navigate to your Unity projects folder using your operating systems file explorer and delete the following folders:
 * ``Assets -> BallisticNGTools -> Plugins``
 * ``Assets -> BallisticNGTools -> Scripts``
 * ``Assets -> BallisticNGTools -> BallisticNG Assets -> Ships -> Scale References`` 
 * ``Assets -> ShipStats``
* Launch your Unity Project(s) with **Unity 2018.3.8**.
* Import the latest Unity Tools package. You will see errors in the console. Ignore them and close Unity
* Open your Unity Project(s) with the newly installed **Unity 2020.3.5**
* Let it open and reimport everything

Fix Broken Tracks (1.3 upgrade)
****************
Upon upgrading a Unity 2018.3.8 project to Unity 2020.3.5, there are two things that will happen:

* Track lighting **will** be broke
* TRM data **may** be corrupt

Both of these issues are caused by changes with how Unity handles meshes, but are both easily fixable.

Lighting Corruptions
--------------------
Rebake the lighting for your track.

TRM Corruptions
-----------------
TRM data corruption in testing was random, it may not even happen at all. However, if you do find yourself with a broken TRM mesh, do the following:

* Open your Unity project
* In the project view, navigate to the folder where the corrupted TRM is located
* Delete the TRM folder next to the TRM file
* Right click the TRM file in Unity's **Project panel** and click **Reimport**. You want the file that has the **paper icon**, not the generated prefab or folder.
* Setup the TRM in a new scene (don't save the scene)
* Bake the lighting for the scene
* Re-open the scene where the TRM is broken. It should now be fixed.

------------

Unity Setup
***********

.. note::
    If you're running on Mac or Linux then make sure you install the **Windows** build component. This is required for Unity to compile DirectX shaders into your tracks for Windows users.

* Download and Install Unity Hub from here: https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe
* Once installed, add Unity 2020.3.5 to the hub by `clicking here <unityhub://2020.3.5f1/8095aa901b9b>`_.

If the above fails to work, you can find resources for manually installing Unity 2020.3.5 `here <https://unity3d.com/unity/whats-new/2020.3.5>`_.

2020.3.5 is **required** for compatability with the latest versions of the BallisticNG Unity Tools. Do not install any other version.

Install
*******
* Create a new Unity 2020.3.5f1 project
* Once created navigate to ``File -> Build Settings -> Player Settings``, scroll down to the **Scripting Runtime Version** and make sure it's set to **.Net 4.x**
* Navigate to ``Assets -> Import Package -> Custom Package`` and open the **BallisticUnityTools.unitypackage** file shipped with the game. You can find this file in the Modding folder where the game is installed
* Import everything

Update
******
* Open your Unity project and create a new scene to ensure nothing is open
* Navigate to ``Assets -> Import Package -> Custom Package`` and open the **BallisticUnityTools.unitypackage** file shipped with the game. You can find this file in the Modding folder where the game is installed
* Import everything
* Restart Unity, do not save scenes if prompted to

------------

Legacy: Broken TRM Assets
****************
In older versions of the Unity Tools there was a bug where TRM meshes might be reimported and reconstructed from scratch when importing new versions which broke references in track scenes. This is fixed in recent versions, but in case this happens and you find your tracks are broken after an update then follow these steps:

* Load the track scene that has been broken
* Run ``BallisticNG -> Utilities -> 5.6 to 2017 Scene Updater``
* Save a copy of the scene
* Create a new scene
* Load the new copy of your scene
* Save over your original scene and delete the copy you just made