.. _neo-tools-surface-placer:

Neo Surface Placer
==================
**Tool Menu Path**: ``Neognosis -> Surface Placer``

The Surface Placer is a tool that allows you to duplicate objects by dropping them relative to the surface under your mouse. Select an object in your scene and then navigate to ``Neognosis - Surface Placer`` to start duplicating it.

Inspector
-----------
The inspector will show you settings for the tool and also the inspector for the components on object you're duplicating, so for instance on a light it'll let you configure the settings of the light before placing each instance.

Tool Settings
^^^^^^^^^^^^^

    **Confirm Objects**: Confirms your duplicate objects, keeps them in the scene and then closes the tool.

    **Cancel Objects**: Cancels your duplicate objects, removes them from the scene and then closes the tool.

    **Normal Offset**: How far away from the surface your mouse is over the object will be placed at. You can also adjust this by holding shift and moving your mouse horizontally.

    **Rotate To Normal**: Whether the object should also be rotated to align with the surface.

    **Rotate Axis**: The axis that the object will aligned along.

    **Rotation Offset**: How much to offset the rotation once it has been rotated to the hovered surface.