.. _unity-tools-shaders:

Shaders
=======

.. toctree::
    shader_standard

    shader_additive
    shader_additive_vertex_alpha
    shader_building_cupemap
    shader_building_reflection_probe
    shader_clouds
    shader_customship
    shader_customshipcull
    shader_display
    shader_display_doublesided
    shader_holofield
    shader_hologram_opaque
    shader_hologram_transparent
    shader_laser
    shader_shipirid
    shader_ships_playerflare
    shader_skydome
    shader_skydomenofog
    shader_skystrip
    shader_skystripnofog

    shader_survival
    shader_survival_doublesided
    shader_survival_environment_detail
    shader_survival_skybox
    shader_survival_transparent

    shader_terrain
    shader_terrain_advanced
    shader_terrain_advanced_plus
    shader_trackpad
    shader_trackpadanimated
    shader_vertexlit
    shader_vertexlit_doublesided
    shader_vertexlit_noclip
    shader_vertexlit_cutout
    shader_vertexlit_cutout_doublesided
    shader_vertexlit_transparent
    shader_vertexlit_Transparent_doublesided
    shader_water
