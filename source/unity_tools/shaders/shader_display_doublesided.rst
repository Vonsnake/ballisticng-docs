.. _unity-tools-shader-display-doublesided:

Display (Double Sided) Shader
=====================
**Shader Path**: ``BallisticNG -> Display (Double Sided)``

The display shader is used for creating digital advert displays. This is the double sided version of :ref:`unity-tools-shader-display-singlesided`

Properties
----------

.. csv-table::
	:file: tables/props_display.csv
	:header-rows: 1