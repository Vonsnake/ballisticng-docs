.. _unity-tools-hologram-shader:

Hologram (Opaque) Shader
=====================
**Shader Path**: ``BallisticNG -> Hologram (Opaque)``

Opaque version of :ref:`unity-tools-hologram-transparent`.

The hologram shader simulates a slightly unstable light projection with wobbling and unstable color flickering. It also makes use of the scanline overlay also seen in the digital shaders.

Properties
----------

.. csv-table::
	:file: tables/props_hologram.csv
	:header-rows: 1