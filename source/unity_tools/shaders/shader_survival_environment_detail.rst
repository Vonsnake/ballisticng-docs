Survival Environment Detail Shader
=====================
**Shader Path**: ``BallisticNG -> Survival Environment Detail``

Intended to be used with Survival Environment Details, documented in :ref:`unity-tools-survival-toggles`

The Survival Environment Detail is a special use case shader that allows you to add overlay effects on top of existing geometry in survival mode, such as fully lit building windows.

The shader pulls polygons forward in the depth buffer so they always render on top of what's behind them.

Ideally you'll want the alpha texture and illumination texture to be the same.

Properties
-----------

.. csv-table::
	:file: tables/props_survivalwindows.csv
	:header-rows: 1