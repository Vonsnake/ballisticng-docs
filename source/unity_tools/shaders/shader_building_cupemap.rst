Building (Cubemap) Shader
=====================
**Shader Path**: ``BallisticNG -> Building (Cubemap)``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The building shader is an opaque shader that uses a cubemap for window reflections.

Properties
----------

.. csv-table::
    :file: tables/props_buildingcubemap.csv
    :header-rows: 1