Track Pad Shader
=====================
**Shader Path**: ``BallisticNG -> Track Pad``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

Properties
----------

.. csv-table::
    :file: tables/props_trackpad.csv
    :header-rows: 1