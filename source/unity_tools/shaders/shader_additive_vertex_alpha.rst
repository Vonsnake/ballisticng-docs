Additive (Vertex Alpha) Shader
=====================
**Shader Path**: ``BallisticNG -> Additive (Vertex Alpha)``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The additive (vertex alpha) shader is a transparent shader that blends the object by adding the pixels color instead of overlaying it with a specified alpha. This can be useful for a magnitude of effects that rely on lights.

This version of the shader takes color values from vertices to use as a base color.

Properties
----------

.. csv-table::
    :file: tables/props_additive.csv
    :header-rows: 1