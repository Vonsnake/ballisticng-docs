.. _unity-tools-shader-terrain-advanced-max:

Terrain Advanced Max Shader
=====================
.. note::
    For this shader to work you must bake lighting information into mesh tangents. To do this, attach the **Lightmapping Options** script to any objects using a material with this shader and tick the **Encode In Tangents** option.

**Shader Path**: ``BallisticNG -> Terrain -> Terrain Advanced Max``

The terrain advanced max shader is a complicated material that allows you to blend multiple texture rmaps using vertex colors. It also provides you with additional options for perlin noise based color tinting and UV scaling.

On top of this, which is the base feature set for :ref:`unity-tools-shader-terrain-plus`, this shader also uses tri-planar mapping to unwrap and apply your textures at runtime, removing the need to UV the mesh correctly to get good results.

Blender Material Helper
-----------------------
If you're using Blender we've provided ``Vertex Color Blend Material.blend`` in the game's modding folder. Append this to your blender scene, import the material and then use blenders shader editor to assign textures to red, green and blue inputs. Now use the material preview render mode in the scene view and you can preview the textures for your terrain as you paint the colors on!

Properties
----------

.. csv-table::
    :file: tables/props_terrainadvancedmax.csv
    :header-rows: 1