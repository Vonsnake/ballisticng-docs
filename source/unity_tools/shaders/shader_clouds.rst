.. _unity-tools-cloud-shader:

Clouds Shader
=====================
**Shader Path**: ``BallisticNG -> Clouds``

The cloud shader uses two noise maps that destructively interfere with each other to create the illusion of clouds.

Properties
----------

.. csv-table::
    :file: tables/props_clouds.csv
    :header-rows: 1