Player Flare Shader
=====================
**Shader Path**: ``BallisticNG -> Ships -> Player Flare``

The player flare shader is an edit of Unity's additive particle shader that forces it to render on top of everything else.