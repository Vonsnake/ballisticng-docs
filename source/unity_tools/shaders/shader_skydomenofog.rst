Skydome (No Fog) Shader
=====================
**Shader Path**: ``BallisticNG -> SkyDomeNoFog``

The sky dome shader is a background opaque shader that can be used for manually created sky backgrounds, ignoring fog. If you're using a sphere mesh and a panorama projected texture it's however reccomended you use Unity's ``Skybox -> Panoramic`` shader and assign the material to your track scenes skybox material.

Properties
----------

.. csv-table::
	:file: tables/props_sky.csv
	:header-rows: 1