.. _unity-tools-ship-irid-shader:

Ship Iridescent Shader
=====================
**Shader Path**: ``BallisticNG -> Ship Iridescent``

The ship iridescent shader is a shader for ships that lets you achieve a variety of two tone effects using rim lighting and tinting driven by sine waves.

Properties
----------

.. csv-table::
	:file: tables/props_irid.csv
	:header-rows: 1