Building (Reflection Probe) Shader
=====================
**Shader Path**: ``BallisticNG -> Building (Reflection Probe)``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The building shader is an opaque shader that uses the nearest reflection probe, or the probe defined in the mesh renderers anchor transform, for window reflections.

Properties
----------

.. csv-table::
    :file: tables/props_buildingrefprob.csv
    :header-rows: 1