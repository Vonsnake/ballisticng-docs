.. _unity-tools-standard-shader:

Standard Shader
=====================
| **Shader Path**: ``Standard (BallisticNG)``
| **Non Batched Shader Path**: ``Standard (BallisticNG - No Batching)``

The standard shader is a multi purpose ubershader that is designed to handle the majority of cases for scenery materials and a few types of effects. This replaces many of the old vertex lit shaders and implements the UV scroller at the GPU level so they're all consolidated into a single place.

The shader comes in two flavours: batched and non batched. The batched version allows objects using the shader to be grouped up before being rendering to the CPU, improving performance if you have lots of objects using it. Non batched does not allow objects to be grouped up and is intended to be used for instanced effects that rely on the objects position for color fading.

Properties
----------

**General Settings**

    **Render Mode**: The mode to use when rendering the object.

        * **Opaque**: For objects with no transparent parts. Can write into the depth buffer.

        * **Cutout**: For binary alpha using pixel clipping. Can write into the depth buffer.

        * **Transparent**: For objects with variable amounts of transparency. Can't write into the depth buffer, a notice will be displayed advising you to turn off Z Write below.

        * **Additive**: Transparent object that adds the pixel color to the screen instead of overlaying it. Can't write into the depth buffer, a notice will be displayed advising you to turn off Z Write below.

        * **Multiply**: Transparent object that multiplies the pixel color to the screen instead of overlaying it. Can't write into the depth buffer, a notice will be displayed advising you to turn off Z Write below.

    **Face Culling**: The type of face cullling to use.
    
        * **Off**: Both sides of polygons will be rendered
        * **Front**: Front faces of polygons will be culled
        * **Back**: Back faces of polygons will be culled

    **Ignore Projectors**: Due to a bug in Unity this option does not work and will likely be removed in a future update.

**Depth Settings**

    **Z Write**: Whether the material will write to the depth buffer, enabling effects such as fog.

    **Z Test**: The depth comparison logic to use when deciding if polygons are visible for drawing.

    **Fade To Color**: Whether the material should fade out to a color over a distance. Make Min larger than Max to have the object fade to a color when the camera is close.

        * **Fade Distance Min**: The minimum distance before the fading begins.

        * **Fade Distance Max**: The maximum distance where the object becomes the faded color.

        * **Fade Color**: The color to fade to.

        * **Fade Origin (Vertex <> Origin)**: How the fade distance is calculated. Vertex fades the object at each vertex. Origin uses the objects pivot. If using Origin blending use the batching version of the shader instead.

    **Use Custom Render Queue**: Whether to use a custom render queue value or let the shader control it. The render queue controls at which point the object is drawn, which can be useful for resolving transparent objects in the background rendering over transparent objects in the foreground.

        * **Custom Render Queue Index**: The render queue index to use when overriding the shader.

**Draw Settings**

    **Vertex Lighting Blend**: The intensity of lighting on the material.

    **Allow Distance Clip**: Whether the retro limited distance option will affect this material.

    **Allow Vertex Wobble**: Whether the retro vertices option will affect this material.

    **Allow Affine Mapping**: Whether the retro affine mapping option will affect this material.

        **Blend**: The affine mapping blend. With large polygons that will take up large amounts of the screen, you'll want to turn this down.
    
**Texture Settings**

    **Texture Offset**: The offset of all textures on the material.

    **Texture Tiling**: The tiling of all textures on the material.

    **Screen Space UVs**: Whether the mesh's UVs should be ignored for the UVs to be calculated based on the screen instead. This is designed for use with portal rendering but may have other uses.

    **Diffuse Tint**: Color multiplier for the diffuse map.

    **Illumination Tint**: Color multiplier for the illumination map.

    **Diffuse Map**: The diffuse texture to use. Defaults to white if no texture is defined.

    **Illumination Map**: The illumination map to use. Defaults to black if no texture is defined.

    **Illumination Intensity**: The intensity of the illumination map.

    **Reflection Mode**: The reflection mode to use. Cube will take a cube map texture and use that. Probe will use the nearest reflection probe (or the probe defined in the mesh renderer's anchor transform field).

        **World Space Reflection**: Calculates reflections in world space instead of local to the object.

        **Reflection Cube (in cube mode)**: The cubemap to use when in cube reflection mode.

        **Use Diffuse Alpha As Mask**: Whether the diffuse's alpha channel will be used as a greyscale mask for reflections.

        **Reflection Color Mask**: A color multiplier for the reflection.

        **Reflection Tint**: A final color multipler for the reflection.

**UV Animation Mode**: The animation mode to use. These are the same as the modes in :ref:`unity-tools-uv-scroller`.

    **Scroll Speed (Scroll Mode)**: The speed of the scroll when in scroll mode.

    **Jump Amount (Jump Mode)**: How much to move the UVs by on each jump.

    **Time Between Jump (Jump Mode)**: How much time to wait between UV jumps.
