Additive Shader
=====================
**Shader Path**: ``BallisticNG -> Additive``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The additive shader is a transparent shader that blends the object by adding the pixels color instead of overlaying it with a specified alpha. This can be useful for a magnitude of effects that rely on lights.

Properties
----------

.. csv-table::
    :file: tables/props_additive.csv
    :header-rows: 1