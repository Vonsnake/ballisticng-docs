.. _unity-tools-customshipcull-shader:

Custom Ship (Cull) Shader
=====================
**Shader Path**: ``BallisticNG -> CustomShipCull``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The custom ship shader is a legacy shader that can be used for custom ships. This is the single sided version of :ref:`unity-tools-customship-shader`

Properties
----------

.. csv-table::
    :file: tables/props_customship.csv
    :header-rows: 1