.. _unity-tools-shaders-survival_opaque:

Survival Shader
===============
**Shader Path**: ``BallisticNG -> Survival``

The shader used for the survival environment. This is the opaque single sided version of :ref:`unity-tools-shaders-survival_transparent` and :ref:`unity-tools-shaders-survival_doublesided`

Properties
----------

.. csv-table::
	:file: tables/props_survival.csv
	:header-rows: 1