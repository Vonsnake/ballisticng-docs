.. _unity-tools-water-shader:

Water Shader
=====================
**Shader Path**: ``BallisticNG -> Water``

The water shader is an advanced shader that simulates water, using a reflection probe source as a reflection source.

Properties
----------

.. csv-table::
	:file: tables/props_water.csv
	:header-rows: 1