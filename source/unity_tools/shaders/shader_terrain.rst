.. _unity-tools-terrain-shader:

Terrain Shader
=====================
**Shader Path**: ``BallisticNG -> Terrain -> Terrain (4 Blend)``

The terrain shader is an advanced shader that uses multiple textures and mesh normals to automatically blend between different textures.

It also provides additional options to add color variation and scale UVs by distance to add more random looking collors and reduce noise when looking at the terrain from a distance.

Properties
----------

.. csv-table::
	:file: tables/props_terrain4blend.csv
	:header-rows: 1