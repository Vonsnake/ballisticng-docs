.. _unity-tools-shaders-survival_transparent:

Survival Transparent Shader
=====================
**Shader Path**: ``BallisticNG -> Survival Transparent``

The shader used for the survival environment. This is the transparent single sided version of :ref:`unity-tools-shaders-survival_opaque` and :ref:`unity-tools-shaders-survival_doublesided`

Properties
----------

.. csv-table::
	:file: tables/props_survival.csv
	:header-rows: 1