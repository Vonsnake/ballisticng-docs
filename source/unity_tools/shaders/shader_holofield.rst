.. _unity-tools-holofield:

Holofield shader
=====================
**Shader Path**: ``BallisticNG -> Holofield``

.. note::
	See the ``Assets -> BallisticNGTools -> BallisticNG Assets -> Small Vehicles -> Walls -> Bike Holowall.mat`` asset for an example of this shader.

The holofield shader is holographic shader that provides numerous hologram effects

Properties
----------

.. csv-table::
	:file: tables/props_holofield.csv
	:header-rows: 1