.. _unity-tools-shaders-survival_doublesided:

Survival (Double Sided) Shader
=====================
**Shader Path**: ``BallisticNG -> Survival (Double Sided)``

The shader used for the survival environment. This is the opaque double sided version of :ref:`unity-tools-shaders-survival_doublesided` and :ref:`unity-tools-shaders-survival_opaque`

Properties
----------

.. csv-table::
	:file: tables/props_survival.csv
	:header-rows: 1