.. _unity-tools-shader-trackpadanimated:

Track Pad Animated Shader
=====================
**Shader Path**: ``BallisticNG -> Track Pad (Animated)``

.. note::
    This shader is an updated version of the track pad shader with illum animation features.

Properties
----------

.. csv-table::
    :file: tables/props_trackpad_animated.csv
    :header-rows: 1