Laser Shader
=====================
**Shader Path**: ``BallisticNG -> Laser``

.. note::
    This shader is redundant and has been replaced by the :ref:`unity-tools-standard-shader`, which can handle what this shader was designed to do.

The laser shader is an edit of Unity's additive particle shader that adds a fixed scroller to the UVs.