Survival Skybox Shader
=====================
**Shader Path**: ``BallisticNG -> Survival Skybox``

The survival skybox shader is a gradient sky with a scanline effect. It's designed for use as a unity skybox material, meaning it needs to be assigned to your track from Unity's lighting tab.

Properties
----------

.. csv-table::
	:file: tables/props_survivalsky.csv
	:header-rows: 1
