.. _unity-tools-hologram-shader-transparent:

Hologram (Transparent) Shader
=====================
**Shader Path**: ``BallisticNG -> Hologram (Transparent)``

Transparent version of :ref:`unity-tools-hologram`.

The hologram shader simulates a slightly unstable light projection with wobbling and unstable color flickering. It also makes use of the scanline overlay also seen in the digital shaders.

Properties
----------

.. csv-table::
	:file: tables/props_hologram.csv
	:header-rows: 1