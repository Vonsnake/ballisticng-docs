.. _unity-tools-shader-display-singlesided:

Display Shader
=====================
**Shader Path**: ``BallisticNG -> Display``

The display shader is used for creating digital advert displays. This is the single sided version of :ref:`unity-tools-shader-display-doublesided`

Properties
----------

.. csv-table::
	:file: tables/props_display.csv
	:header-rows: 1