A crash course on Unity
=======================
Unity is a big, complicated engine. It's actually quite easy to understand but it may be a bit overwelming at first.

More advanced topics and going into further depth is outside the scope of documenting BallisticNG tools so if you'd like to learn more beyond this page, you can get started with the offical Unity documentation here:

https://docs.unity3d.com/Manual/UnityOverview.html

The Interface / Camera Controls
-------------------------------
Before you get started you'll want to read Unity's documentation on their UI. It's a pretty short read:

https://docs.unity3d.com/Manual/UsingTheEditor.html

One thing you will need to dive a bit deeper to understand though is viewport controls, so if you want the TL;DR for those here you go:

* FPS camera movement is done by holding down the right mouse button and using WSAD. Q and E will let you ascend and descent and holding shift will speed the camera up. While holding right mouse you can also use the scroll wheel to step forward and backwards
* CAD orbit movement is done by holding down alt and using left mouse for rotate and right mouse for zooming. Hold down the middle mouse button at any time to pan (you don't need to be holding alt)
* Press F with an object selected to focus on it

.. note::
    For some reason Unity thought it would be cool to link the near/far clipping planes to the CAD orbit zoom, which also updates if you press F on an object. If you find that objects suddenly start clipping in the distance or when close to the camera, either keep zooming in with the obit camera until it becomes a lot less responsive or just create a new empty game object and focus on it.

Understanding assets
-----------------------
Assets are files that you import into your Unity project to use, be it textures, models, sounds, etc. You can place assets wherever you like as long as they're inside the assets folder that was created inside your Unity project's folder. You can import stuff into Unity by either manually placing the files into the assets folder or by just dragging the file in the project panel. Unity assets are their own complex directory of file references, so you can properly organise everything with sub directories to keep everything clean.

Unity supports a lot of different file formats. The reccomended however is always FBX for meshes, PNG or TGA for textures and WAV for sounds.

Understanding objects
---------------------
In Unity everything that you're playing is inside a scene. A scene is a collection of objects and settings that make up a play area and it's where you'll be doing everything. Every track in BallisticNG is a scene with it's own scenery and settings that define what that track is and how it plays.

A thing in a scene is called a game object. By itself a game object is an invisible point in space - a point that has a rotation and scale more precisely. We can make these more complex by attaching components to them. Components are scripts that define some kind of behaviour, for instance a Mesh Filter will give an object some geometry and then a Mesh Renderer will take the geometry from the Mesh Filter and then draw it. **Everything** in your scene will be built from game objects, they are the fundemental building blocks for anything and everything.

A very useful thing with game objects is that they can parented to other game objects, allowing you to build hiearchies of objects so moving one object will move all of the children. Or even more simply, you can use them to just organise your scene.

Understanding prefabs
-----------------------
Unity allows you to prefab objects, which is to say take a hiearchy of gameobjects out of a scene and then place it as a generic asset inside of your project, allowing it to be re-used across multiple scenes or be used outside of scenes entirely.

You can create a prefab by dragging a gameobject out of a scene into the project view tab. Double clicking on a prefab will open the prefab editor, which puts you into a temporary scene where you can adjust the configuration of everything in the prefab.

If you make an edit to a prefab in a scene and want it to be reflected across all instances of it in your project, you can apply the local scene changes back to the prefab asset using the prefab controls at the top right of the inspector while it's selected in the scene. You can also create nested prefabs, which is where a prefab is linked to another prefab and will copy it configuration but then apply its own configuration on top. You can create these by dragging a prefab out of a scene into the project view tab.

Understanding materials
-----------------------
Mesh Renderers were mentioned above, which are now very important for this next part.

Whenever we draw something in Unity we use a shader. A shader is a bit of code that runs on your graphics card to tell it how it's going to draw something. To get from scenery to the code that's going to draw it we use a material. A material exposes settings from shaders so we can tell it how we want it to draw something, such as textures and various other settings that let us control how something looks.

There are many different ways that things can be drawn so there are naturally a lot of shaders at your disposal when setting up materials. To learn more about what BallisticNG adds for you to use, see :ref:`unity-tools-shaders`.

We attach materials to mesh renderers which then instructs that mesh renderer how we want it to draw the geometry.

Some extra tips
---------------
Unity has it's fair share of useful stuff that's not too well known, so here's a couple of things to make your life easier:

* Press F2 to quickly rename whatever you have selected
* Press Ctrl + D with assets selected to duplicate them. This is especially useful for materials if you want to quickly make copies
* Press Ctrl + Shift + F with a game object selected to move and allign it with the scene view camera. This is extremely useful if you want to quickly place lights by flying to where you want them instead of having to fight with the transform gizmos
* Holding alt while revealing or collapsing object hiearchies in the hiearchy panel will reveal/collapse all child objects too
