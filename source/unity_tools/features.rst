What's possible?
=====================
The entirety of Unity is at your disposal so there really is not much that you can't do. The only thing you can't do is use your own scripts. Unity does not compile scripts into asset bundles and there is no way to unload code once it's loaded. You can use anything that's built into Unity or anything the BallisticNG tools add however.

Outside of that, anything that's standard in Unity or part of BallisticNG can be used. So in short yes, you can in fact build a mountain of rigidbody barrels and fly into them to stress test the physics engine ;)