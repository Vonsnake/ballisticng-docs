Behaviours - Physics Toggler
============================
**Add Component Path**: ``BallisticNG -> Behaviour -> Physics Toggler``

The physics toggler object allows you to control which physics mode the object will be enabled in.

Properties
----------

    **Enable in 2159**: Whether this object will be enabled in 2159.

    **Enable in 2280**: Whether this object will be enabled in 2280.
    
    **Enable in Floor Hugger:** Whether this object will be enabled in Floor Hugger.