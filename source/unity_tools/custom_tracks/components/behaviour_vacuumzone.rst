.. _unity-tools-vacuum-zone:

Behaviours - Vacuum Zone
========================
**Add Component Path**: ``BallisticNG -> Behaviour -> Vacuum Zone``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Vacuum Zones allow you to define a space as a vacuum, which will muffle the game's audio and remove the ships wingtip vortices.

Properties
----------

    **Inverted**: Whether the trigger is inside out where entering the vacuum zone means there is oxygen and leaving the vacuum means there is no oxygen.