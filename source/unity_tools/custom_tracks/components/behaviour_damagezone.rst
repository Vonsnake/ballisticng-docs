.. _unity-tools-damage-zone:

Behaviours - Damage Zone
========================
**Add Component Path**: ``BallisticNG -> Behaviour -> Damage Zone``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Damage Zones allow you to damage or heal ships instantly or at set intervals of time.

Properties
----------

    **Enter Damage**: The damage profile to use when a ship enters the damage zone.

    **Exit Damage**: The damage profile to use while a ship stays in the damage zone.

Damage Profile Settings
-----------------------

    **Enabled**: Whether the damage profile is enabled.

    **Damage Amount**: How much damage will be dealt to the ship. Set to a negative value to heal the ship.
    
    **Damage Interval**: How often the ship will be damaged in seconds. For the Enter Damage property this will be ignored.

    **Use Instant Kill**: Instantly kill the ship instead of applying damage.