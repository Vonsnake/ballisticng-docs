.. _unity-tools-trigger-generic-print:

Generic Triggers - Print
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Print Trigger``

A trigger that logs text to the Unity debug output. Use the logger from the F9 debug menu to see logs in-game.

To fire this trigger, call the `Trigger()` method.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired.

	**Text**: The text to print to the console.

	**Print Enabled**: Whether the text will be printed to the console. Disable if you'd just like to run the trigger (useful for debugging and later disabling the output).

Callable Methods
-----------------

	**Trigger()**: Fires the trigger.

	**SetText()**: Sets the text that will be displayed on the next trigger fire.
