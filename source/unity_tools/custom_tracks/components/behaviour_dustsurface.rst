.. _unity-tools-dustsurface:

Behaviours - Dust Surface
=============================
**Add Component Path**: ``BallisticNG -> Behaviour -> Dust Surface``

.. note::
	The dust surface is part of a multi-component setup and won't do anything unless there is a :ref:`unity-tools-dustcontroller` with particle sets configured.

A dust surface is a surface tag component that informs the game when a ship has started hovering over a particular surface.

For a dust surface to work it must be attached to an object with a collider that a ship can hover over (the TRM floor or a scenery floor).

If setting up a scene with multiple surfaces it is **strongly** reccomended that you disable the TRM floors mesh collider and setup multiple scenery floor colliders which correlate to each surface type the ship can hover over.

Dust surfaces also have a more utilitarian use outside of just particles and sounds. A particle set can be configured to invoke enter/exit events on any kind of trigger, which means a surface can suddenly become a giant trigger with a complex shape for all sorts of behaviours!

Properties
^^^^^^^^^^

	**Particle Set**: The name of the particle set that this surface will trigger. This should be lowercase and match the name of a particle set in the :ref:`unity-tools-dustcontroller`.