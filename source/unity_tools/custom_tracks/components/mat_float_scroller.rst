Materials - Float Scroller
==========================
**Add Component Path**: ``BallisticNG -> Animation -> Materials -> Float Scroller``

.. note::
    This script currently has a bug that prevents the animation curve showing up when you first create a scroller. To work around this, deselect and then reselect the object.

.. note::
    This script is previewable in Unity's play mode.

The float scroller allows you to manipulate a float property in a material by scrolling through an animation curve that you created.

Setup a scroller
----------------
* Click the add scroller button
* Enter the name of the target float property you want to scroll. This will default to the default name of illumination intensity properties
* Set the speed of the scroller using the **Scroll Amount** property
* Use the animation curve editor to set the curve to scroll through using **Value Curve**

Properties
----------

    **Target Float**: The name of the float property to manipulate. If you want to find out the name of the property for this:

        * Click the small options button at the top right of the inspector
        * Select debug mode
        * In the material open the *Saved Properties* list and then open the *Floats* list. All of the float properties in the material are listed here.
        * Go back to normal mode from the inspector options

    **Scroll Amount**: How fast to scroll through the animation curve

    **Value Curve**: The animation curve to scroll through.

Float scrollers support an infinite number of scrollers per script.