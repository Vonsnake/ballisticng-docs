.. _unity-tools-sprite:

Rendering - Billboard Sprite
============================
**Add Component Path**: ``BallisticNG -> Rendering -> Billboard Sprite``

.. note::
    Do not scale the transform of the sprite. Use the script to set the size instead.

The billboard sprite script creates a traditional 3D sprite that renders an image that faces the camera at all times. Because this takes advantage of Unity the sprite however renders a material instead of just an image, so you get plenty of flexibility.

The sprite itself is a quad mesh that is generated behind the scenes. You cannot attach any scripts to this object to animate the quad in any way. If you'd like to UV scroll the sprite, you'll need to use the BallisticNG standard shader.

Properties
----------

    **Size**: The size of the sprite in X and Y.

    **Offset**: The offset of the sprite from its origin.

    **Center Sprite**: Whether to center the sprite so the middle of the quad is at the origin of the object instead of the bottom edge being at the origin.

    **Full Orient**: Whether the sprite should fully orient the camera on all axes, instead of just the yaw.

    **Use View Direction**: Orients the sprite with the cameras view direction instead of looking at the cameras position.

    **Billboard Material**: The material that the sprite will use.