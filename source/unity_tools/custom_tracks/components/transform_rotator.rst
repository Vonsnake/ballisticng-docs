Animation - Transform Rotator
=============================
**Add Component Path**: ``BallisticNG -> Animation -> Transformation -> Rotator``

The transform rotator allows you to rotate an object around its own axis or around another objects axis.

Properties
----------
    **Rotation Speed**: The speed per axis to rotate the object.

    **Rotate Around**: The object to rotate this object around. Leave as none to have the object rotate around its own axis.