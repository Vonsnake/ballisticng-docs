.. _unity-tools-dustcontroller:

Behaviours - Dust Controller
=============================
**Add Component Path**: ``BallisticNG -> Behaviour -> Dust Controller``

.. note::
	The dust controller is part of a multi-component setup and won't do anything unless there are :ref:`unity-tools-dustzone` and / or :ref:`unity-tools-dustsurface` components referencing the particle sets configured in the controller.

.. note::
	All of the internal assets for dust zones are provided. You can find them in the ``Assets -> BallisticNGTools -> BallisticNG Assets -> Dust Zones`` folder.


The dust controller is the brain of BallisticNG's dust system. It defines particle sets which are then referenced by zones and surfaces to spawn particles, play audio and remotely trigger any kind of trigger component available, such as push zones.

**This should be attached to it's own game object.**

Particle Sets
-------------

Note that particle systems and audio clips are not required for the particle set to work. If they are absent they will simply not be used. This means, if you want to, you can just hook triggers and to take advantage of dust surfaces to activate particular behaviours based on thet surface the ship is currently hovering over.

	**Name**: The name of the particle set that zones and surfaces will reference. This should be lowercase.

	**Effects In Survival Environment**: Whether this particle set will be used in the Survival environment.

	**Particles**: The optional particle system to use. The particles are orientated Y up, so make sure your systems are orientated with this in mind.

	**Clip**: The optional sound to play.

	**Volume**: The volume of the sound being played.

	**Pitch**: The pitch of the sound being played.

	**Triggers**: A list of all triggers that should be fed enter/exit events as the ship enters or leaves the particle set.