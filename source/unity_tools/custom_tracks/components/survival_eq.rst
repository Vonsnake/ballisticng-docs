Survival - EQ Visuals Board
===========================
**Add Component Path**: ``BallisticNG -> Rendering -> Survival EQ Visualizer``

.. note::
    EQ Visualizers require normalized UVs to display properly. A helper texture is provided to help you correctly UV meshes for these.

    ``BallisticNGTools -> Shared Assets -> SurvivalVisHelperTexture.png``,

The Survival EQ Visualizer allows you to create music visualizers that show up in survival mode. Typically these will be used to replace advertisment billboards.

The script needs to be attached to an object with a mesh renderer. The material on the mesh renderer will be automatically replaced with one generated in-game that contains the output of the rendererd visualizer.

Properties
----------

    **Type**: The type of bands to use.

        * **Bands**: Normal bands with nothing special.

        * **Bands Sliced**: Each band will be sliced several times vertically.