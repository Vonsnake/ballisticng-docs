Animation - Transform Oscillator
================================
**Add Component Path**: ``BallisticNG -> Animation -> Transformation -> Oscillator``

The transform oscillator allows you to move a transform from its origin along a direction in it's local space and back at a set speed.

Properties
-----------

    **Direction / Distance**: The direction and distance to move the object from its origin point. For instance setting Y to 5 will set the end point of the animation to 5 units in the objects Y axis.

    **Speed**: How fast the object will oscillate.