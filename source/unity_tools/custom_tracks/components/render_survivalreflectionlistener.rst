.. _unity-tools-srl:

Rendering - Survival Reflection Listener
=============================
**Add Component Path**: ``BallisticNG -> Rendering -> Survival Reflection Listener``

The survival reflection listener component can be attached to reflection probes that are used for objects in Survival. When this component is attached the game will be informed to re-render the reflection cubemap everytime the virtual environment colors change.