.. _unity-tools-trigger-generic-objectevents:

Generic Triggers - Object Events
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Object Events Trigger``

A trigger with many events that listen for Unity MonoBehaviour calls.

This trigger is automated, calling `Trigger()` will call the regular Trigger Event instantly.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired from Trigger().

	**On Start Event**: What to do when the object has been enabled for the first time.

	**On Enabled Event**: What to do when the object has been enabled (set active).

	**On Disabled Event**: What to do when the object has been disabled (set inactive).

	**On Destroyed Event**: What to do right before the object is destroyed.

	**On Become Visible Event**: What to do when a mesh renderer attached to the same object has become visible to a camera. This will be called when inside of the camera's view frustrum or if it has become visible through baked occlusion culling.

	**On Become Invisible Event**: What to do when a mesh renderer attached to the same object has become invisible to a camera. This will be called when outside of the camera's view frustum or if it has become invisible through baked occlusion culling.

	**On Will Render Object Event**: What to do when a mesh renderer attached to the object will be rendered in the current frame.

Callable Methods
-----------------

	**Trigger()**: Fires the trigger.