Triggers - Custom Lap Tracking
==============================
**Add Component Path**: ``BallisticNG -> Triggers -> Tracking Trigger``

Tracking triggers allow you to manually set the transform of the start and mid line triggers. The cyan box in the editor shows the area of the trigger. You can scale the area by scaling the object.

Properties
----------

    **Trigger Type**: Which trigger this object will transform.