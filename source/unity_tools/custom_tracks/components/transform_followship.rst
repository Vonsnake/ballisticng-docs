.. _unity-tools-follow-ship:

Transform - Follow Ship
=======================
**Add Component Path**: ``BallisticNG -> Animation -> Transformation -> Follow Ship``

.. note::
    This script requires that the object has a mesh filter and mesh renderer as it's updated when rendered so support multiple cameras. If you're using this on a light flare, attach the light flare to default unity cube object, remove the box collider andthen use a transparent material on the mesh renderer.

The follow ship script allows you to setup an object that will follow the camera with a set amount of blending and a world offset. This allows you to create background objects that appear to be at an infinite distance.

Properties
----------

    **Follow X/Y/Z**: How much the object will follow the camera on each world axis.

    **Follow Offset**: How offset the object will be from the camera.


Scene Gizmo
------------
When you have an object with this script attached you will see a blue sphere. This blue sphere scales itself to the mesh attached to the object and will then give you a preview of the follow behaviour so you can configure it how you want it without having to jump into the game to see it.