.. _unity-tools-teleporter-zone:

Triggers - Teleporter Zone
==========================
**Add Componenet Path**: ``BallisticNG -> Triggers -> Teleport Zone``

.. note::
    If you're looking to create a visible portal, see :ref:`unity-tools-tp`. Teleporter zones and renderers/cameras rely on each other.

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Teleporter zones allow you to teleport any ship that enters them to a different location.

Properties
^^^^^^^^^^

    **Section Target Index**: The section that the player will be teleported to. Check the scene view for the wire sphere and line pointing out and above it, this shows you where the target section is.

    **Snap Ship Rotation**: If enabled then the ship will inherit the destinations rotation, otherwise the ships relative rotation will be kept through the teleport.

    **Reset Velocity**: Whether the ships velocity should be reset after being teleporter.

    **Keep Inside Bounds**: If using a section as a target, this will make sure that the players ship are always inside of the tracks bounds when they are teleported.

    **Teleporter Transform Override**: Setting this allows you to use a transform instead of a section. This is an advanced feature and you should read through the placement guiode in :ref:`unity-tools-tp` first if you are not yet familiar with how teleportation works in BallisticNG.