.. _unity-tools-water-assets:

Water Assets
============
.. note::
    These should be used in conjunction with with the :ref:`unity-tools-water-detail-switcher`

**Basic Water Path**: ``Assets -> BallisticNG -> Water -> Water``

**Advanced Water Path**: ``Assets -> BallisticNG -> Water -> Water4``

The water assets are a slightly modified version of the stock Unity water assets that used to come with older versions of the engine. These are what we use when you enable the **Fancy Oceans** option in-game.

Inside each directory for the 2 different water types is a prefab folder, which contains pre setup assets that you can drag and drop into your scenes and configure.

Water4 is split into an advanced and simple prefab. These are basically just quality settings.

We reccomend that you create your own material to assign to the water. The default one's are good, but there is a **lot** to customize in the materal.

If you're looking to create water in lline with the vanilla game, you should also remove the **Mod Specular Lighting** script from the Water4 object. You'll need to full unpack the prefab before you do this though. You can do this by right clicking the water object in the hiearchy tab and then using the **Unpack Prefab Completely**.