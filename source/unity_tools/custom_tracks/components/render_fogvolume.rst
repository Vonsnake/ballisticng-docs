.. _unity-tools-fv:

Rendering - Fog Volume
======================
**Add Component Path**: ``BallisticNG -> Rendering -> Fog Volume``

.. note::
    A collider is not required for this component and you will be prompted to remove it if there is one.

.. note::
    If using Unity's lighting tab to set the fog instead of the legacy method of using the scene references object, you must be using the linear fog mode for fog volumes to work.

Fog volumes allow you to control the scenes fog when you're using fog. If fog isn't enabled in the scene, fog volumes will do nothing.

Fog will be changed when the camera is inside of the fog volume, will transition betwen fog volumes and when not in any volume will transition back to the global fog settings.

When selected you can use the dot handles on the volume to adjust the dimensions of the fog volume.

Properties
^^^^^^^^^^

    **Mode**: Whether to use a box or sphere for the volume. Box mode supports transform scaling. The sphere does not and you should reset the objects scale to 1 else the sphere radius preview will be out of sync.

    **Extents/Radius**: The extents (box) or radius (sphere) of the volume.

    **Fog Color**: The color of the fog when inside of the zone. Alpha channel does nothiing.

    **Start Distance**: How far away from the camera the fog starts inside of the zone.

    **End Distance**: How far away from the camera the fog ends inside of the zone.

    **In Time**: How fast the fog settings will transition to this volumes settings.

    **Out Time**: How fast the fog settings will transition to the global fog settings when leaving this volume.