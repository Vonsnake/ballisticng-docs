Triggers - Transform Interpolator
=================================
**Add Component Path**: ``BallisticNG -> Triggers -> Transform Interpolator``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

The transform interpolator script allows you to trigger an object to interpolate between an A and B transform if there are any ships inside of the zone.

Trigger Properties
------------------

    **Trigger Enter Event**: Triggered when a ship has entered the trigger zone and no other ships are in the zone..

    **Trigger Exit Event**: Triggered when all ship have left the trigger zone.

Properties
----------

    **A**: The A transform, where the target transform will be when no ships are in the zone.

    **B**: The B transform, where the target transform will be when there is a ship in the zone.

    **Target**: The target transform that will be interpolated between A and B.

    **Interpolation Time**: How long it will take for the target to lerp between A and B.

Setup Guide
-----------

* Duplicate your target object and remove all attached components on the duplicated object. Rename the object to something you will recognise as the A transform.

* Duplicate your target object, move the duplicated object to where you want the target object to move to and then remove all attached components on the duplicated object. Rename the object to something you will recognoise as the B transform.

* In the transform interpolator script set A to your A transform, B to your B transform and target to the original object that you were just duplicating.