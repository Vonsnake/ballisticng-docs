.. _unity-tools-uv-scroller:

Materials - Uv Scroller
====================
**Add Component Path**: ``BallisticNG -> Animation -> Materials -> UV Scroller``

.. note::
    If you're using BallisticNG's standard shader then it's reccomended you use the scroll animation tools implemented in that. It has the same features as this script but will run on the GPU, not manipulate the material and can be previewed in the scene view.

.. note::
    This script is previewable in Unity's play mode.

The UV scroller allows you to manipulate the UVs of a texture in a material by smoothly scrolling or stepping them.

Setup a scroller
----------------
* Click the add scroller button
* Enter the name of the the target texture name you want to scroll. This willl default to _MainTex and will very likely be what you want to leave it on, as this controls the UVs for everything in all shaders
* Set the speed of scroller in both axes using **Scroll Amount**

Properties
----------

    **Texture Name**: The name of the texture property to manipulate the UVs of. If you want to find out the name of a property for this:

        * Click the small options button at the top right of the inspector
        * Select debug mode
        * In the material open the *Saved Properties* list and then open the *Tex Envs* list. All of the texture properties in the material are listed here.
        * Go back to normal mode from the inspector options

    **Use Scale Time**: If enabled then the step scroller will use the physics delta time instead of the frame delta time to update. This makes the scroll speed framerate independant and means it will pause when the game pauses.

    **Use Step Scrolling**: If enabled then the scroller will use stepping instead of a smooth scrolling, using the **step interval** setting to determine at which intervals of time in seconds the UVs will be moved.

    **Scroll Amount**: How fast the UVs will scroll. If you're in step scrolling, this is the distance they wiill jump.

UV scrollers support an infinite number of scrollers per script.