.. _unity-tools-trackintropans:

Animation - Track Intro Pans
============================
**Add Component Path**: ``BallisticNG -> Pre-Race -> Intro Overview``

Track intro pans allow you to setup the pre-race panning camera that you see on the internal tracks. Each pan of the camera uses a spline to move the camera while a look at position is used to orientate it.

Setup
-----

* Create a new gameobject and attach the Intro Overview script
* Click add path, this will create a new spline and path reference that you can edit
* Enter a name for your path
* Edit the spline to create the path you want the camera to follow
* Create a new empty game object, position it where you want the camera to look when following the path and then assign it to the **Lookat Start** field.
* If you want the camera to shift its focus to a different point by the end of the animation, create another object and assign it to the **OPTIONAL Lookat End** field
* If you want fine control over the cameras up vector through its animation, create a gameobject and rotate it so its y axis is pointing along the up vector you desire and then assign it to the **OPTIONAL Lookat Up**
* Set the speed you want the camera to travel down the spline at
* Repeat from 2nd step for each path that you want


