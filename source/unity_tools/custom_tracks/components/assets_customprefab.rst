.. _unity-tools-custom-prefab:

Assets - Custom Prefab
=======================
**Add Component Path**: ``BallisticNG -> Assets ->  Custom Prefab``

.. note::
	These arn't for use in custom tracks. Please see :ref:`codemods-assets-api` for more information on thiis.

The custom prefab script allows you to create a prefab of objects that you can reference for code mods to read. These arn't for use in custom tracks and should be setup as Unity prefab assets for exporting to an NGA file.
