.. _unity-tools-luarunnner:

Behaviours - Lua Runner
======================
**Add Component Path**: ``BallisticNG -> Behaviour -> Lua Runner``

Lua Runners allow you to run custom Lua code to control objects and respond to ship events. See the Lua :ref:`unity-tools-luaintro` guide for more information.

Properties
----------

    **Lua Script**: The lua script file to run with the object.

    **Update Tick Rate**: The frequency that the update method will be called at if there is one.

    **Create Variables From Script**: Loads variable definitions from the assigned lua script and populates the variables list with them.

    **Variables**: The variables that will be fed to the lua script.