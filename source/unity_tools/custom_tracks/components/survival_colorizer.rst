.. _unity-tools-survival-colorizer:

Survival - Colorizer
=====================
**Add Component Path**: ``BallisticNG -> Rendering -> Survival Colorizer``

The Survival Colorizer script allows you to setup any mesh that's also setup with a Survival Ignore script to respond in some way to the survival color pallete.

You can create multiple targets per script.

Properties
----------

	**Target Material**: The material that the target will be adjusting.

	**Color Type**: What part of the survival color pallete to pull the color from.
	
	**Target Color Properties**: A list of color properties to ajust on the target material. (_Color, _IllumTint, etc).