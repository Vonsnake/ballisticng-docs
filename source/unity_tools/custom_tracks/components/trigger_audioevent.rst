Triggers - Audio Event
======================
**Add Component Path**: ``BallisticNG -> Triggers -> Audio Trigger``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Audio event triggers allows you to trigger an audio source when a ship enters and leaves the trigger zone.

Note that you can also trigger audio sources to play without this script as they contain a play method you can hook, however this trigger script gives you a few more options relevant to BallisticNG.

Trigger Properties
------------------

    **Trigger Enter Event**: Triggered when a ship has entered the trigger zone.

    **Trigger Exit Event**: Triggered when a ship has left the trigger zone.

Properties
----------

    **Target Source**: The target audio source that will be played.

    **Override Play State**: Whether trigger the audio source to play will override the play state. If turned off, trigger the audio source while the audio source is still playing a sound will not play a sound.

    **Player Only**: Whether only player ships can trigger the sound to play.

    **Repeat Delay**: A minumum time that must pass before the sound can be played again.