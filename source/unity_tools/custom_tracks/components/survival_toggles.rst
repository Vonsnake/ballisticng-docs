.. _unity-tools-survival-toggles:

Survival - Rendering Toggles
============================

Survival Disable
----------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Disable``

Attach to an object you want to be disabled when the virtual environment is being used.

Survival Enable
---------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Enable``

Attach to an object you only want to be enabled when the virtual environment is being used.

Survival Environment Detail
---------------------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Environment Detail``

Attach to objects that will be environment details. This needs to be used in conjunction with the Survival Environment Detail shader.

Because of the nature of the shader, you can disable the mesh renderer on the object you attach this script to and it will automatically re-enable it when starting the track with the virtual environment enabled.

Survival Ignore
---------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Ignore``

Attach to an object you want the virtual environment to ignore and do nothing with.

Survival Track Surface
-----------------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Track Surface``

Attach to an object that you want to extend the TRM track. A helper texture is provided with the tools to help you UV any meshes you want to do this.

``BallisticNGTools -> BallisticNG Assets -> Tiles -> Survival -> Combined``

Survival Sky Mesh
-----------------
**Add Component Path**: ``BallisticNG -> Survival -> Survival Sky Mesh``

Attach to an object you want to use the virtual environment sky material. This is intended to be used with transparent materials that draw over opaque geometry to clip out everything but the sky behind it, where Unity's sky background will also be cut out.
