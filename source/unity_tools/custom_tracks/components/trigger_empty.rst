.. _unity-tools-trigger-empty:

Triggers - Custom Trigger
================
**Add Component Path**: ``BallisticNG -> Triggers -> Custom Trigger``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

The custom trigger script is a basic trigger script that allows you to send events to other objects when a ship has entered or left the zone.

Trigger Properties
------------------

    **On Enter Lua Trigger**: Calls the OnShipEnter method on the lua script object set when a ship has entered the trigger zone.

    **On Exit Lua Trigger**: Calls the OnShipExit method on the lua script object set when a ship has left the trigger zone.

    **Trigger Enter Event**: Triggered when a ship has entered the trigger zone.

    **Trigger Exit Event**: Triggered when a ship has left the trigger zone.