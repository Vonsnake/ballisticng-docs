.. _unity-tools-oatz:

Behaviours - Override Ai Target Zone
====================================
**Add Component Path**: ``BallisticNG -> Behaviour -> Override Ai Target Zone``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

The override AI target zone allows you to instruct the AI which section to aim for when inside of the zone.

You can also use the AI Next feature available to sections directly, but this provides you with more fine control if you need it.

Properties
^^^^^^^^^^

    **Target Section**: The section that the AI will aim for. Check the scene view for the wire sphere and line pointing out and above it, this shows you where the target section is.