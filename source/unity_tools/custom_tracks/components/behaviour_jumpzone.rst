Behaviours - Jump Zone
======================
**Add Component Path**: ``BallisticNG -> Triggers -> Jumpzone``

.. note::
    This script does not need a collider. It will generate a box collider when the track starts.

.. note::
    For purposes behind jump forces, jump zones are a depreciated feature left in for their more explicit purpose. For a more generic and feature rich version, see :ref:`unity-tools-push-zone`

Jump Zones allow you to push ships around and affect their acceleration. These were originally implemented for use in large jumps to help ships make it across, though they can also be used for a magnitude of other purposes.

To set the direction of a jump zone, use the Pitch and Yaw offset handles in the scene view.

To scale a jump zone, you must use the small dot handles in the center of each face on the area cube.

Properties
----------

    **Toxic**: The jump profile to use in toxic class.

    **Apex**: The jump profile to use in apex class.

    **Halberd**: The jump profile to use in halberd class.

    **Spectre**: The jump profile to use in spectre class.

    **Zen**: The jump profile to use in zen class.

Jump Profile Properties
-----------------------

    **Push Speed**: The force to apply to the ship in the set direction.

    **Acceleration Multiplier**: How much to multiply the ships engine acceleration by. Leave at zero to have no acceleration multiplication.