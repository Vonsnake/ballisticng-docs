.. _unity-tools-hrtf:

Configuration - No HRTF
=====================================
**Add Component Path**: ``BallisticNG -> Configuration -> No HRTF``

.. note::
    This script is attached to an object that has an audio source component.

HRTF (`Head Related Transfer Function <https://en.wikipedia.org/wiki/Head-related_transfer_function>`_) is an audio processing effect that BallisticNG uses when the Spatial Audio option from the audio settings menu is enabled. It simulates how sound reaches our ears to create a more realistic sounding stereo field.

The No HRTF script allows you to disable HRTF processing on any audio source that the script is attached to. This is important for stereo ambient sounds that should make up a soundscape when you're inside of the minimum distance.

You shouldn't use this script for sounds that come from a point in space such as crowds chearing, light buzzing, etc.