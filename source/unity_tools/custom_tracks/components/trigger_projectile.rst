.. _unity-tools-projectile-trigger-zone:

Triggers - Projectile
=====================
**Add Component Path**: ``BallisticNG -> Triggers -> Projectile Trigger``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

The projectile trigger script is a basic trigger script that allows you to send events to other objects when a projectile has entered and left the zone.

Trigger Properties
------------------

    **On Enter Lua Trigger**: Calls the OnBehaviourEnter method on the lua script object set when a projectile has entered the trigger zone.

    **On Exit Lua Trigger**: Calls the OnBehaviourExit method on the lua script object set when a projectile has left the trigger zone.

    **Trigger Enter Event**: Triggered when a projectile has entered the trigger zone.

    **Trigger Exit Event**: Triggered when a projectile has left the trigger zone.