.. _unity-tools-water-detail-switcher:

Rendering - Water Detail Switcher
=================================
.. note::
    This is for use with :ref:`unity-tools-water-assets`

**Add Component Path**: ``BallisticNG -> Rendering -> Water Detail Switcher``

The water detail switcher script allows you to reference two types of objects that will show up depending on the in-game fancy oceans option. Internally this is used to toggle between a lower quality and higher quality water material.

Properties
----------

    **High Quality Water**: The object that will be used when fancy oceans is enabled.
    
    **Low Quality Water**: The object that will be used when fancy oceans is disabled.