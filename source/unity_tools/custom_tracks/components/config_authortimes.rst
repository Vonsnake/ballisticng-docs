.. _unity-tools-author-times:

Configuration - Track Author Times
========================
**Add Component Path**: ``BallisticNG -> Configuration -> Track Author Times``

.. note::
    Times entered in here need to be the raw floating point value. Once you have set a time in-game you can get this by opening the command console (ctrl + backspace) and using the ``info_generatetimereport`` command. This will copy a report to your clipboard. Paste it somewhere such as notepad and you'll find the raw seconds float in the total time at the bottom of the report.

Track Author Times allows you to store award times into the track. This is used for precision runs and will be automatically detected by the mode. You don't need to assign any references to this script.

If bronze is set to zero or a time for an award is less then the tier above it, precision runs will enter debug mode where there will be no timer.

Properties
----------

    **Bronze Time**: The time needed to obtain a bronze award.

    **Silver Time**: The time needed to obtain a silver award.

    **Gold Time**: The time needed to obtain a gold award.

    **Platinum Time**: The time needed to obtain a platinum award.