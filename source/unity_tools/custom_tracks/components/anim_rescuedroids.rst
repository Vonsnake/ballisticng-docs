Animation - Start Grid Rescue Droids
====================================
**Add Component Path**: ``BallisticNG -> Pre-Race -> Rescue Droids``

The start grid rescue droids are the small floating machines you see locking the ship in place at the start of the race. They are not required.

The rescue droids work by linking a track tile to a spline which defines their path.

Setup
-----

* Attach the script to a new game object
* Click add path button
* On the new entry click **Select Tile** and click the first spawn tile on the track
* Click the **Auto-Config Spline** button. This will create a new spline that you can go and edit
* Enter a speed, or leave it at the default, to set how fast the rescue droid will follow the path
* Repeat from 2nd step for the other spawn tiles