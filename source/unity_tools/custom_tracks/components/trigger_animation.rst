Triggers - Animation Trigger
=============================

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Animation Triggers allow you to trigger Animator components when a ship enters the trigger area.

Trigger Properties
------------------

    **Trigger Enter Event**: Triggered when a ship has entered the trigger zone.

    **Trigger Exit Event**: Triggered when a ship has left the trigger zone.

Properties
----------

    **Target Animator**: The animator to trigger the playback on when a ship has triggered the zone.

    **Override Play State**: Whether the animation should be played back from the start instantly as soon as the zone is triggered.

    **Player Only**: Whether only player ships can trigger the zone.