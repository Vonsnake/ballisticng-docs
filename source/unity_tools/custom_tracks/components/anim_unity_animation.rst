Animation - Unity Animation
==========================
.. note::
    You can preview your animations in Unity's game view.

Unity has it's own animation system that you can use to animate your scenery inside of your CAD and then import the key frames into the engine.

Unity's current animation system is designed around character animations with a state system more then it is quick and simple key framed animations, so make sure you use the legacy animation system.

Setup Legacy Animation
----------------------

* Click on your scenery asset in Unity's project panel
* In the inspector go to the Rig tab
* Change the **Animation Type** to **Legacy**
* Head over to the animation tab and configure the animation however you want it to play
* Click **Apply** at the bottom of the import settings
