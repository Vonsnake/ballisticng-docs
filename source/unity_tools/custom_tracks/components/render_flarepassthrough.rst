.. _unity-tools-fpt:

Rendering - Flare Passthrough
=============================
**Add Component Path**: ``BallisticNG -> Rendering -> Flare Passthrough``

A flare passthrough can be attached to objects that have colliders which you do not wish to occlude lens flares. This can be useful for assets such as holographic walls, where you want a ship to collide with it but for lens flares to pass through it normally.