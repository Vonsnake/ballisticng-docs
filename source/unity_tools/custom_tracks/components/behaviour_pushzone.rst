.. _unity-tools-push-zone:

Behaviours - Push Zone
======================
**Add Component Path**: ``BallisticNG -> Behaviour -> Push Zone``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

Push Zones provide you with several ways of pushing a ship around.

The cyan line in the scene view shows you the direction and force of the push.

Properties
----------

**Target Ships**

    **Affects AI**: Whether the push zone can affect AI.
    
    **Affects Players**: Whether the push zone can affect players.

**Direction Settings**

    **Push Space**: The space that the force is relative to.

        * **Local To Zone**: The push force will be local to the zones rotation.

        * **Local to Ship**: The push force will be local to the ship inside of the zone.

        * **World**: The push force will be in world space.

        * **Local To Track**: The push force will be local to the current track section.

    **Direction**: The direction vector of the force.


**Force Settings**
    
    **Push Force**: The force of the push.

    **Push Force Mode**: The Unity force mode to use on the ship. Force/Acceleration apply a gradual force. Impact/Velocity Change instantly set the ships velocity.

    **Use Force Gain**: Whether the force will be applied over time.

    **Force Gain**: How long it will take the push force to reach it's maximum value.

    **Use Force Falloff**: Whether the force will let off over time.

    **Force Falloff**: How long it will take the push force to stop.

**Velocity Settings**

    **Manipulate Ship Velocity**: Whether the push zone will also multiply the ships velocity.

    **Velocity Multiplier**: The velocity multiplier on each axis.