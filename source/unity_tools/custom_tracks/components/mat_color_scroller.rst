Materials - Color Scroller
=======================
**Add Component Path**: ``BallisticNG -> Animation -> Materials -> Color Scroller``

.. note::
    This script currently has a bug that prevents the gradient showing up when you first create a scroller. To work around this, deselect and then reselect the object.

.. note::
    This script is previewable in Unity's play mode.

The color scroller allows you to manipulate a color property in a material by scrolling through a gradient that you created.

Setup a scroller
----------------
* Click the add scroller button
* Enter the name of the target color property you want to scroll. This will default to _Color and will very likely be what you want to leave it on
* Set the speed of the scroller using the **Scroll Amount** property
* Use the gradient editor to set the gradient to scroll though using **Color Gradient**

Properties
----------

    **Target Color**: The name of the color property to manipulate. If you want to find out the name of a property for this:

        * Click the small options button at the top right of the inspector
        * Select debug mode
        * In the material open the *Saved Properties* list and then open the *Colors* list. All of the color properties in the material are listed here.
        * Go back to normal mode from the inspector options
    
    **Scroll Amount**: How fast to scroll through the color gradient

    **Color Gradient**: The gradient of colors to scroll through.

Color scrollers support an infinite number of scrollers per script.

