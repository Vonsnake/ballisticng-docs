.. _unity-tools-trigger-generic-delayed:

Generic Triggers - Delayed
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Delayed Trigger``

A trigger that waits for the set number of seconds before fireing.

To fire this trigger, call the `Trigger()` method.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired.

	**Time**: The time in seconds to wait before fireing the trigger after `Trigger()` has been called.

	**Phase**: The decimal percentage of Time + Random to start the delay countdown at. 0 means the entire time will need to elapse, 1 means it will fire instantly.

	**Random Min**: The minimum value to add / subtract from Time at random.

	**Random Max**: The maximum value to add / subtract from Time at random.

	**Reroll Random On Trigger**: Whether the random time value will be re-rolled on each trigger. When left off, the random time value will only be set on the first `Trigger()` call.

Callable Methods
-----------------

	**Trigger()**: Fires the trigger.

	**SetTime()**: Sets the Time propety.

	**SetPhase()**: Sets the phase property.

	**SetRandomMin()**: Sets the random min property.

	**SetRandomMax()**: Sets the random max property.
