Behaviours - Physics Mod Zone
=============================
| **Add Component Path**: ``BallisticNG -> Behaviour -> Physics Mod Zone``
| **Import From XML**: ``BallisticNG -> Track Configuration -> Import Physics Mod Zones From XML``

.. note::
    This script does not need a collider. It will generate a box collider when the track starts.

Physics Mod Zones provide you with various controls to manipulate ship physics while they are inside of its area.

These can be configured inside of the track creator 2.0 and exported to XML, which can then be imported into the tools. This allows you to design layouts with physics modifications in mind.

Physics Mod Zones do not additively stack. If you have a mod zone inside of another mod zone, the inner mod zone will overwrite the outer mod zone until the ship leaves the inner mod zone.

Properties
----------

**General**

    **Gravity Multiplier**: Multiplies the gravity force by this value.

    **Track Allignment Multiplier**: Multplies how fast the ship orientates to the track.

    **Grip Multiplier**: Multiplies the ships grip stat.

    **Drag Multiplier**: Multiplies how much drag force is applied to the ship. Note that decreasing this will also increase the ships top speed.

**Zero-G**

    **Enabled**: Whether Zero-G physics mode is enabled. If this is enabled the gravity multiplier is automatically set to zero.

    **Allignment Multiplier**: How much to increase the ships track orientation speed by.

    **Pitch Multiplier**: Multiplies the rotation cause by the players pitch inputs.

    **Pitch Force**: How much vertical force in response to the players pitch input should be applied.

    **Pitch Speed**: How quickly the vertical force is applied to reach the target pitch force.

**Spring**

    **Enabled**: Whether a spring force to keep the ship alligned with the tracks height (for use with Zero-G) should be used.

    **Force**: The maximum amount of force that the spring can exert on the ship.

    **Min Threshold**: The minimum absolute distance the ship must be from the track for the spring to start exerting force.

    **Max Threshold**: The maximum absolute distance the ship must be from the track for the spring to reach its maximum force.

    **Damping**: How much the spring should be dampened.

    **Alignment Multiplier**: How much to increase the ships track orientation speed by as it approaches the maximum distance threshold.

    **Speed Multiplier**: How much to increase the springs maximum force as the ships speed increases.