.. _unity-tools-anim-spline:

Animation - Splines
===================
**Add Component Path**: ``BallisticNG -> Animation -> Spline``

Splines are paths that objects can travel along.

Scene Gizmos
------------
To manipulate the spline you use the sphere handles in the scene to move the node points and node handles. Selecting a node will show the Unity transform gizmo to allow you to move it.

Inspector Interface
-------------------
The inspector interface is where you add new points and define their mode.

    **Loop**: Whether the spline loops or not. If you disable this after enabling it, the final node will be detached and left inside the start node for you to move.

    **Add Node**: Adds a new node to the spline. Selecting a spline before the last spline will insert a node between the selected node and the next node.
 
**With a node selected**:

    **Node Position**: The position of the selected node.

    **Node Handles Mode**: The mode that will be used for positioning the node handles.

        * **Free**: Both handles do not interact with each other.
        * **Aligned**: Both handles share a rotation but the distance is separate.
        * **Mirrored**: Both handles are mirrored fully
    
    **Delete Note**: Deletes the node.