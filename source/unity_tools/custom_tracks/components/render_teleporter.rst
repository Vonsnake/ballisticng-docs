.. _unity-tools-tp:

Rendering - Portals/Teleporter
==============================
.. note::
    Portals require screen space UVs to work. Before anything else, create a material using the BallisticNG standard shader for each portal mesh and tick the **Screen Space UVs** option.

.. note::
    The placement of these components, as well as the :ref:`unity-tools-teleporter-zone`, are very important for the rendering of the portal. See the section below about orientations and offsets for more information.

Portal / Teleporter is an extension of :ref:`unity-tools-render-targets`, with additional calculations required to simulate a portal that you can look through.

Teleporter Renderer
-------------------
**Add Component Path**: ``BallisticNG -> Rendering -> Teleporter Renderer``

The Teleporter Renderer takes the output of a teleporter camera and assigns it to the specified textures in a material.

Properties
^^^^^^^^^^

    **Texture Map Targets**: The list of texture names to assign the camera output to in the attached material. This defaults to _Illum which should be what you want out of the box.

    **Target Teleporter Camera**: The target teleporter camera to take the texture output from.

    **Oblique Clipping Blend**: In order to properly render the portal exit the game must clip anything that's behind the portal in case the camera is far back enough to see it. This is done using what's called an oblique projection and this option controls the near clipping plane blend of it. Leaving this at the default should usually be fine but if you find the track is obviously clipping when you're near the portal, try tweaking this.

    **Max Render Distance**: How far away the player needs to be before the portal stops rendering. Leave at -1 for the portal to always render as long as the Teleporter Renderer is visible. If you set a max render distance for this you might want to use the Fade To Color option in the BallisticNG Standard Shader.


Teleporter Camera
-----------------
**Add Component Path**: ``BallisticNG -> Rendering -> Teleporter Camera``

.. note::
    The Teleporter Camera requires an anchor transform to function, as it uses this to calculate the relative player offset before rendering the portal.

    When you first attach the script to an object click the **Create Anchor Transform** button. This will automatically create the anchor object and attach the teleporter camera object to it for you.


The Teleporter Camera takes the output of a generated camera for a teleporter renderer to use. The placement of this camera is very important.

Properties
^^^^^^^^^^

    **Camera Anchor**: The anchor transform that will be used as a reference for the camera position before rendering. See the note above as there is a tool built in that will configure this for you.

Placement Guide
---------------
Refer to the sections place object reference feature in :ref:`unity-tools-track-editor` first.

When placing your portal objects it's very important that everything lines up relatively to each other on both ends, otherwise you will create a visual and positional disconnect. If everything is configured correctly the portal will look and feel seamless.

Portal Mesh
^^^^^^^^^^^
Your portal mesh can be whatever you want it to be, the shape and size doesn't matter. What **does** matter however is the pivot and where it's placed in the world.

    **Portal Entrance Mesh**: Forward axis should be facing through the object so the pivots Z axis is facing the same direction as the backface.

    **Portal Exit Mesh**: Forward axis should be facing in reverse, so the pivots z axis is facing the same direction as the face if you turn around.

The pivot on both meshes should be in the same place.

Use the section place object tool to automatically snap your portal meshes to the track. The section for the entrance can be whatever section you want to be where the portal starts. The exit needs to be the same section that you assigned in your Teleporter Zone.

Camera Anchor Position
^^^^^^^^^^^^^^^
The camera anchor needs to be placed exactly at the section you assigned in your Teleporter Zone. Use the section place object tool to automatically snap the anchor transform to the section.

.. note::
    Make sure you are snapping the anchor and not the camera itself.

Teleporter Zone
^^^^^^^^^^^^^^^^
Your teleporter zone should be setup to be in the same place as your portal entrance mesh, but pushed behind it instead. To do this, follow these steps:

    * If you haven't yet, create a new game object and attach a box collider, tick **Is Trigger**, attach the teleporter zone script and then set the exit section.
    * In the hiearchy view drag the teleporter zone object on top of your portal entrance mesh object to parent it to it.
    * Set the teleporter zone position and rotation to ``0, 0, 0``
    * Configure the box collider size so it covers the area you want to trigger the teleporter, making sure the Z axis faces through the portal.
    * Set the Z center to the Z size divide by two.
    * In the hiearchy view drag the teleporter zone object out of the portal mesh object to detatch it from it

You should now find that the teleporter zone is behind the portal and the pivot is perfectly in sync with the portal entrance.


