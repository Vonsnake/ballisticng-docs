.. _unity-tools-turbotrigger:

Behaviours - Turbo Trigger
============================
**Add Component Path**: ``BallisticNG -> Behaviour -> Turbo Trigger``

The turbo trigger component will activate a turbo when a ship enters it.

Properties
^^^^^^^^^^

	**Play Sound**: Whether the turbo pickup sound will be played.

	**Time Mult**: A multiplier for how long the turbo will be active for. Note that setting this to zero will still give you a turbo, as there's a hard coded deacceleration cooldown.