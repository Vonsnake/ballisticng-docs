Rendering - Light Flares
========================

Ballistic Flare
---------------
**Add Component Path**: ``BallisticNG -> Rendering -> Ballistic Flare``

The ballistic flare allows you to render lens flare using either two presets provided by the game or by providing your own custom flare asset, which can be made using the in-game flare editor. The light flare is rendered in 3D so to keep it at a static distance, see the :ref:`unity-tools-follow-ship` script.

    **Flare Type**: The type of flare prefab to use if not using a custom flare configuration.

    **Custom Flare (Override)**: The custom flare asset to use if not using an in-game prefab.

    **Use Depth Buffer Culling**: The flare will be culled using the depth buffer instead of a raycast.
        
        * This only works on Windows when using DX11 and can quickly become GPU intensive. Use this on as little flares as possible, preferably one.


Flare Occluder
--------------
**Add Component Path**: ``BallisticNG -> Rendering -> Flare Occluder``

Any object with a mesh collider that's not in the Ignore Raycast will block light flares. However having a mesh collider means that the mesh will always be collible, which in a lot of cases you don't want.

The flare occluder will tag out the option on track launch and apply a special layer that nothing but the light flare culling check will collide with. **These will also occlude weather**.

Attach a mesh collider and then the flare occluder script to an object and that's it!

.. note::
    You do not need to click the **Create Backface Collider** button. This was a legacy option that is left in for backwards compatability and is not required as all newer builds of the game support backface raycast queries.