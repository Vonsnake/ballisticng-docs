.. _unity-tools-render-targets:

Rendering - Render Targets
==========================

Render targets allow you to render the scene from a specific viewpoint or from the players viewpoint and then transfer it into a texture to display elsewhere in the world.


Render Target Display
---------------------
**Add Component Path**: ``BallisticNG -> Rendering -> Render Target Display``

The render target display script takes the output of a render target camera script and assigns the texture to a material attached to the same object.

Properties
^^^^^^^^^^

    **Texture Names**: The list of textures in the attached material to assign the render target camera output to. This defaults to _Illum which is the reccomended texture map, however you can change or add more to suite your needs.

    **Target Camera**: The target render camera to grab the image data from.

Render Target Camera
--------------------
**Add Component Path**: ``BallisticNG -> Rendering -> Render Target Camera``

The render target camera script generates a camera and is then used by a render target display to copy its output image to a mesh and material somwhere in the world.

Properties
^^^^^^^^^^

    **Framerate**: The framerate of the camera. It's **strongly** reccomended that you leave this at 24fps for performance reasons.

    **Field Of View**: The field of view of the capture camera.

    **Use Main Camera**: Whether to use the players camera instead. This will output what the player is seeing in-game.
