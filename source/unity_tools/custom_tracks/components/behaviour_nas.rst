Behaviours - No Anti-Skip Volume
==============================

**Add Component Path**: ``BallisticNG -> Behaviour -> No Anti-Skip Volume``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

No Anti-Skip Volumes allow you to disable the anti skip while a ship is inside of its area.
