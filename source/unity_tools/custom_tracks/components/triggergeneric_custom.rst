.. _unity-tools-trigger-generic-custom:

Generic Triggers - Custom
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Custom Generic Trigger``

An empty trigger script that can be used as a method for other triggers to call.

To fire this trigger, call the `Trigger()` method.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired.

Callable Methods
-----------------

	**Trigger()** - Fires the trigger.