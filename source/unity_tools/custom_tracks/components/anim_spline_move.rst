Animation - Spline Mover
========================
**Add Component Path**: ``BallisticNG -> Animation -> Procedual -> Spline Move``

.. note::
    This script is previewable in Unity's play mode.

The spline mover allows you to move an object along a spline.

Properties
----------

    **Target Spline**: The target spline to move the object along. See :ref:`unity-tools-anim-spline`

    **Target Gameobject**: The target object to move along the spline.

    **Duration**: How long it will take the object to travel around the spline in seconds

    **Start Position**: Where along the spline the object will Start

    **Animation Type**: The type of animation to use.

        * Once will only play the animation once
        * Loop will play the the animation again once it has finished
        * Pingpong will reverse the animation once it reaches the end and then reverse it again when it reaches the start

    **Play Automatically**: Whether the animation should start when the track loads.

Script Trigger Methods
----------------------

    **StartAnimation()**: Starts playback of the animation.
