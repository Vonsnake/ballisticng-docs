.. _unity-tools-nfgz:

Behaviours - No Forced Grounding Zone
=====================================
**Add Component Path**: ``BallisticNG -> Behaviour -> No Forced Grounding Zone``

.. note::
    This script requires a collider component, make sure you have one attached and have **Is Trigger** set to true. If you are unsure what shape you should use, stick to a **Box Collider**.

When a ship reaches a certain height above the track its gravity is increased to force it back to the ground faster. This zone allows you to disable that behaviour whenever a ship is inside of it.

Trigger Properties
------------------

    **Trigger Enter Event**: Triggered when a ship has entered the trigger zone.

    **Trigger Exit Event**: Triggered when a ship has left the trigger zone.