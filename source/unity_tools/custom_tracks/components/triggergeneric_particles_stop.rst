.. _unity-tools-trigger-generic-particlesstop:

Generic Triggers - Particles Stop
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Particles Stop Trigger``

A trigger that fires when on an object with a particle system with its stop action set to callback.

This is an automated trigger, but call also be fired with the `Trigger()` method.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired.

Callable Methods
-----------------

	**Trigger()**: Fires the trigger.
