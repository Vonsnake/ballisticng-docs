.. _unity-tools-weather-controller:

Rendering - Weather Controller
===============================
**Add Component Path**: ``BallisticNG -> Rendering -> Weather Controller``

.. note::
    You can only have one weather controller per track.

Weather controllers allow you to create your own weather effects for your tracks.

For an example, see ``BallisticNGTools -> BallisticNG Assets -> Weather Example``

Properties
----------

    **Grid Snap Size**: The size of the grid that the particles system position will snap to.

    **Height Above Player**: How high above the player the particlle system will sit at.

    **Thunder Enabled**: Whether thunder strikes will be used.

    **No Engine Reflections**: Whether the weather should make all tiles with a direct line of sight to the sky wet or not.

    **No Geometry Occlusion**: Whether the weather will be occluded by geometry or not.

    **No Sound**: Whether the sound should be enabled or not.

    **Custom Sound**: A custom sound to use, instead of the game's default rain loop.

    **Vortex Particle**: A particle system that will throw particles at the players camera when they're flying fast enough.

    **Weather Particles**: The list of weather particle system profiles to use.

Weather Particle Profile
------------------------

    **System**: The particle system.
    
    **Renderer**: The particle systems renderer.