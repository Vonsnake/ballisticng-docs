Behaviours - Respawn Zone
=======================
**Add Component Path**: ``BallisticNG -> Triggers -> Respawn Trigger``

Respawn zones allow you to instantly respawn any ship that enteres them. The area is shown as a red box in the editor. You can scale them by scaling the object.