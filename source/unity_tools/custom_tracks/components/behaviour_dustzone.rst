.. _unity-tools-dustzone:

Behaviours - Dust Zone
=============================
**Add Component Path**: ``BallisticNG -> Behaviour -> Dust Zone``

.. note::
	The dust zone is part of a multi-component setup and won't do anything unless there is a :ref:`unity-tools-dustcontroller` with particle sets configured.

A dust zone is a trigger which informs the game that the ship has started hovering over a particular surface. Unlike the :ref:`unity-tools-dustsurface` this is a catch all and as long as the ship is in the zone, it doesn't matter what's below the ship as everything will be treated as the same surface type.

Properties
^^^^^^^^^^

	**Particle Set**: The name of the particle set that this zone will trigger. This should be lowercase and match the name of a particle set in the :ref:`unity-tools-dustcontroller`.