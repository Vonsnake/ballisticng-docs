Behaviours - BallisticNG Collider
===============================
**Add Component Path**: ``BallisticNG -> Behaviours -> BallisticNG Collider``

.. note::
    Do not attach a Unity collider to the object you attach this too. The game will automatically create a mesh collider when it loads the track.

.. note::
    **DO NOT** use the track floor and track wall modes. These are preserved for the TRM floor and TRM wall. Use the Scenery Floor and Scenery Wall modes instead.

The BallisticNG collider script allows you to setup scenery objects that will be treated as the track floor and track wall.