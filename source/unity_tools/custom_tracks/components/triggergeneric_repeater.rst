.. _unity-tools-trigger-generic-repeater:

Generic Triggers - Repeater
=====================
**Add Component Path**: ``BallisticNG -> Triggers Generic -> Repeater Trigger``

A trigger that repeat fires after the set number of seconds.

This trigger is automated. Calling `Trigger()` will restart / reset the repeater.

Trigger Properties
------------------

    **On Lua Trigger**: Calls the OnTriggered method on the lua script object when triggered.

    **Trigger Event**: What to do when the trigger is fired.

	**Time**: The time in seconds to wait between each fire cycle.

	**Phase**: The decimal percentage of Time + Random to start the first cycle after Trigger() or Autostart at. 0 means the entire time will need to elapse, 1 means it will fire instantly.

	**Random Min**: The minimum value to add / subtract from Time at random.

	**Random Max**: The maximum value to add / subtract from Time at random.

	**Autostart**: Whether the repeater automatically starts its timer. Setting to false means you will need to call `Trigger()` to start it.

	**Reroll Random On Cycle**: Whether the random time value will be re-rolled on each repeat cycle.

	**Reroll Random On Trigger**: Whether the random time value will be re-rolled when `Trigger()` is called.

Callable Methods
-----------------

	**Trigger()**: Restarts / Resets the repeater.

	**Stop()**: Stops the repeater.

	**SetTime()**: Sets the Time propety.

	**SetPhase()**: Sets the phase property.

	**SetRandomMin()**: Sets the random min property.

	**SetRandomMax()**: Sets the random max property.
