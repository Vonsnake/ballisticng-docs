Scene Structure
===============

After you've setup a track scene you will notice in the **hierarchy view** that some objects have been created. These objects are created to give you a guide for structuring your scene, but you are not required to follow this structure and can do whatever you like.

At the top level will be two objects called **[ Scene Configuration ]** and **[ Track Configuration ]**. 

Scene Configuration
*******************
Scene configuration is where any objects that you add via Unity should be stored. The follow objects are children of this to further help you organise:

=============   ========================================================
Object Name     Description
=============   ========================================================
|- Lights       Used for storing your scene lights.
|- Audio        Used for storing your scenes sound sources.
|- Particles    Used for storing your scenes particles.
|- Managers     Contains the scene's technical manager scripts.
|- Objects      Contains several sub objects for various object types.
=============   ========================================================

For the managers, see: :ref:`unity-tools-scene-refs`.

Track Configuration
*******************
Track configuration contains your TRM mesh. The purpose of this object is for you to store any imported assets in here, such as your tracks scenery.