Texture Configuration
=====================
To make your texture assets fit with BallisticNG there are a few import settings you will want to change. You might also need to set a texture as read/write enabled depending on the textures use. **More on read/write below**.

You can access the import settings for an asset by selecting the asset (or assets) in the project view and then looking in the inspector.

Global Settings
**************
These should be your default import settings.

* Turn off **Generate Mip Maps**
* Set Filter Mode to **Point (no filter)**
* If you want to retain the full detail of your image, set Compression to None

Track Tiles and Frontend Images
*******************************
These settings **need** to be used for track tile textures and images for the frontend. You shouldn't use these for anything else as read/write creates a second copy of the texture in RAM, which bloats the memory requirements for your tracks.

* Turn off **Generate Mip Maps**
* **(Track Tiles Only)** Set Filter Mode to **Point (no filter)**
* Turn on **read/Write Enabled**