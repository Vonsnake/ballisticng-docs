Import TRM / Scene Setup
==========

What's a TRM?
*************
* The file that the layout creator exports
* Stands for Track Render Mesh and is used as the default track mesh, used for generating your tracks data and can be easily UV'd using the track editor atlas painter tools
* Once used to generate your track data is not required and can be disabled if you'd like to use custom meshes for visuals and collisions. It however needs to be enabled for the tremors raising floor effect.

Importing
*********
Drop your TRM directly into Unity's project view or copy it somewhere in your Unity project's asset folder *(sub-directories can be used)*. When Unity detects the file it will generate some assets next to the TRM file:

* A folder called TRM that contains a floor mesh asset, wall mesh asset and a default material. You might want to change the material to suite your needs, but you will never need to touch the floor mesh and floor wall assets
* A prefab file that contains an object hiearchy containing your floor and wall meshes

Create a track scene
********************
    **Method 1 - Create Scene From TRM**:

        Right click the generated prefab and then navigate to ``BallisticNG -> Create Scene From TRM``. This will create a new scene, generate track data, create a skybox and pre-bake some lighting for you.

        It's reccomended that you save your scene straight away.

    **Method 2 (LEGACY) - Track Wizard**:

        This is a legacy method and should not be preferred.

        * Navigate to ``File -> New Scene``. In the new scene, drag your TRM prefab file into the hiearchy view, this will place it into the scenes origin. If you drag it into the scene view, make sure you zero out the position of the prefab
        * Navigate to ``BallisticNG -> Track Configuration -> Track Wizard``. In the window that opens validate the settings you want and make sure the track floor and wall object references are set, if not make sure you assign them manually
        * Click run and your scene will be setup for you to begin editing

