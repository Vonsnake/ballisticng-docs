.. _unity-tools-fast-play:

Fast Play
=========
**Open Path**: ``BallisticNG -> Fast Play``

Fast Play is a feature that allows you to build your current open track to a tempory location in your Unity project and then have BallisticNG launched and directly load your track.

Before you use the tool you first need to set your BallisticNG install location. Click the **Set Game Location** button and navigate to your BallisticNG install path.

Now select the gamemode, ship and speed class you'd like to play with and click **Build And Launch**. The game will launch and open your track.
