TRM Editor
==========
**Open Path**: ``BallisticNG -> Utilities -> TRM Editor``

.. note::
    The TRM editor is a game object with a custom editor. If it looses focus and you need to get back into it, select the **TRM Editor** object in the hiearchy panel.

The TRM editor is a tool that allows you to trace a TRM mesh over anything in Unity.

Before you begin tracing:

* Select all meshes that you want to trace over
* Attach a Mesh Collider in the inspector

With the TRM editor open you will now see a red dot over any vertices you hover over.

How it works
------------
The TRM editor requires you to trace out each section in order to generate the final TRM mesh. Several tools are implemented to aid you in this. The interface explains what you need to do so this documentation will cover everything in a more generic fashion instead of walking you through the steps you need to take.

Sections Mode
-------------
Sections mode is where you draw out sections for your Trm mesh. There are 2 modes to aid you in this.

Click Mode
^^^^^^^^^^
.. note::
    If the track has no wall at the section you're creating, click the floor again for the wall vert and this will register that side of the section as having no wall.

Click mode allows you to define the section manually, vertex by vertex. You create the section in order of ``Middle Floor - Left Floor - Left Wall - Right Floor - Right Wall``.


Drag Mode
^^^^^^^^^
.. note::
    Drag mode does not support no walls at this time. If the section you are creating doesn't have walls you will want to switch to click mode.

Drag mode allows you to create a new section in a single mouse drag. Click the the left wall vertex and then drag your mouse over the vertices to make up the section. When you release the mouse button the section will be created.

General Settings
^^^^^^^^^^^^^^^^
    
    **Don't connect next section**: Create a break in the track when you have defined the next section. Use this to create jumps and start new routes

    **Auto centre middle vert**: Automatically centers the middle floor vertex to the center of the left and right floor once the section has been defined


Nexts Mode
----------
The nexts mode allows you to update the next order of the generated sections. A yellow sphere will show the selected section and a blue sphere will show the highlighted section under your mouse.

    **Z + Click**: Set the clicked section as the next section to the selected sectionn

    **X + Click**: Clears the next section on the clicked section

Vertices
--------
Vertices mode allows you to move vertices of the sections you've created. Select a vertex and then use the Unity gizmo to move it. Hold down V and use the middle square gizmo to vertex snap the vertex to other vertices in the scene

Save and Export
------
To save your trace all you need to do is save the Unity Scene.

To export a TRM, click the **Export Mesh To TRM** button at the top of the interface.