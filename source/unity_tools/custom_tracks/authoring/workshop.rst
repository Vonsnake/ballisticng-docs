Upload to Steam Workshop
========================
.. note::
    To use these tools you will need to be running Steam with an account that has the game.

.. note::
    Workshop images need to be a JPG file under 1mb in size. A resolution of 1024x1024 is reccomended.

Tools are provided in Unity to upload your tracks to the Steam workshop. For the workshop a different file format that contains workshop data is used, called WTRK.

Generate WTRK
--------------
* Navigate to ``BallisticNG -> Track Workshop Manager``
* Use the **Set TRK Folder Location** button to point the manager to your custom tracks Folder
* Use the **Set WTRK Folder Location** button to point the manager to another folder where your build WTRK files will be stored. You should make this a separate folder to your TRK file location
* Select your track from the TRK files list and then click the **Convert To WTRK** button to the right
* You should now be automatically taken to the WTRK file in the WTRK files list. If not, click the **WTRK Files** button at the top of the UI and then select your WTRK file.

Setup Workshop Item
-------------------
With a WTRK file selected you will now need to setup the workshop item data. To the right hand side of the interface you will see tools for this.

* Enter a name in the **Title** field.
* Enter a description in the **Description** field.
* Add some tags that describe your track using the **Add Tags** dropdown
* Set the visibility of your workshop item using the **Visibiility** dropdown
* Click the **Update Preview Image** and select a preview image. See the note above for specifications on this image
* Click the **Save File Button**
* Click the **Upload To Workshop** button

Update Workshop Item
---------------------
If you'd like to update your track on the workshop, navigate back to the WTRK file and then use the **Update TRK** button to replace the TRK in the WTRK file. Now save the file and click **Upload To Workshop**.