.. _custom-tracks-reverse-track:

Create A Reverse Track
===================
**Open Path**: ``BallisticNG -> Create Reverse Track``

.. note::
	**DO NOT** save the reverse TRM in the same folder as the forward TRM. This will create a conflict where data such as lighting will attempted to be shared across them.

The Create Reverse Track tool allows you to very quickly setup a reverse track by reversing the TRM mesh, exporting it and then automatically creating a new scene with all of the references setup.

To use the tool:

* Make sure you are in the scene of the track you want to reverse
* Open the Create Reverse Track tool
* Give it a location to save the TRM file. This **needs** to be inside of your Unity project assets directory somewhere. Please see the note above regarding saving TRMs in the same folder (save it in a different folder).
* Give it a location to save the reverse scene file. The same folder rule does not apply to this file, you can save it next to your other scene file if you wish.
* Let the tool run, a few popup windows will show to let you know what the tool is doing.
* The reverse scene will open.

With the reverse scene created you can now begin setting it up. The tool will do it's best to transfer the tile data over. Floor tiles should be 1:1, but you may find a few wall tiles are incorrect, which you can easily fix. You will also now need to setup the sections like you did with the forward version as section data cannot be automatically translated over to the reverse track.