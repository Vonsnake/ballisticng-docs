Building a TRK
==============
Building your track is a quick process, to build it either:

Make sure the track you want to build is open and then navigate to ``BallisticNG -> Build Active Track Scene`` and then select the folder you want to save your track scene to.

**OR**

* Right click your tracks scene file
* Navigate to ``BallisticNG -> Build Track``
* Select the folder you want the track to be build to (you'll want this to be the BNG **User/Mods/Tracks** folder)

Once the build completes you can now start the game to test it!

.. note::
    You can rebuild the track while the game is running. Build the track and restart the event if you're currently playing the track.

    If you update your tracks frontend data you will need to clear the tracks cache. You can do this by deleting the .tch file in **User/Cache/Tracks**. After deleting this you will need to restart the game if it is running.

