.. _unity-tools-scene-refs:

Scene References
================

The scene references object is a script that contains important configuration data for your track. This script is referenced for both ingame loading and cache file generation and as such is used to configure not only references and track environment settings, but also what's displayed on the menu.

To quickly access the scene references script, navigate to ``BallisticNG -> Track Configuration -> Scene Referecnes``. As it's a script attached to a gameobject, you will need to make sure you have an inspector tab open to see it.

Track Setup
^^^^^^^^^^^

Frontend Settings
*****************
    **Preview Image**: The image that will be used on the menu. This should be a 900x300 image with read/write enabled in the import settings. Also make sure you set ``Non-Power of 2`` to None, otherwise your image will look blurry.
    
    **Track Description**: The description text that will be used on the menu.

    **Track Location**: The tracks location which will be displayed on the loading screen.

    **Loading Screen Background**: The background image that will be used on the loading screen.

    **Menu Layout Mesh Override**: A custom layout mesh to use in place of the tracks track floor. Leave as ``None`` if you do not wish to have a custom mesh.

Object Settings
***************
    **Countdown Displays**: A list of objects that represent where the countdown displays will be spawned.

    **Track Data**: Exposed reference to the track data object. You don't need to worry about this, unless it's empty.

Gameplay Settings
*****************
    **Track Mode**: The tracks gameplay mode. 

    
    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    Normal                      Standard track.
    Drag                        Drag track that forces the drag speed multiplier.
    Drift                       Drift track. Now a redundant option and not needed.
    Precision                   The track will show up as a precision run.
    Small Vehicles              The small vehicle speed reduction will be applied.
    ======================      ============================================================================

    **Physics Mode**: The physics mode to use on the track. Modern will override Classic. Floor Hugger will override Modern if enabled as a cheat.

    **Allow Mixed Pads**: Allows both tile and 3D pads to be used at the same time. With this disabled 3D pads will be removed unless you're playing in 2280.

    **Point To Point Track**: Sets whether the track is a point to point track. When enabled the mid line will be used as the finishing line.

    **Disable Anti-Skip**: Globally disables the anti-skip system. On pre-1.0 tracks this is enabled by default and is disabled by default on new post-1.0 tracks.

    **Survival Anti-Cheat Extra Spins**: Sets how many additional spins can be performed in Survival mode before the anti-cheat kicks in on this track.

    **Blocked Weapons List**: A list of weapon that will be blocked from rotation for this track. Names must be the API names, which you can find out by using the *give* console command ingame. The only offical track to make use of this option is Kuiper Overturn.

Ai Settings
***********
    **Ai Speed Multiplier**: Controls the global top speed of the AI for the track. This can be useful for making small adjustments if your layout is too easy or hard but it is strongly advised that you leave it and only use it if you absolutely have to.

    **Ai Look Ahead**: **This option is no longer used**. Controls how many sections ahead the AI look when steering and airbraking on a per-speed class basis. If you're using a standard TRM with the default 2 units per section then you shouldn't have to play with these as these options are primarily designed for custom tracks built with the TRM editor where the distances between sections might not be standard.

Audio Settings
**************
    **Music Override**: Allows you to define an audio clip to replace ingame music with. The name of the file will be the name of the track displayed ingame. It's reccomended you use a wav and enable streaming in the import settings for a seamless loop and no fame interupts when the file is being fully loaded.

    **Welcome to Intercom**: A welcome to line to play when the track loads.

Rendering Settings
******************
    **All Lights Are Baked**: Marks that all lights in the scene are baked and will disabled when the scene is loaded ingame. If you have a lot of lights this will help boost performance on slower computers.

    **Track Uses Depth Buffer Effects**: Lets the game know that the track is using effects that require the depth buffer. If you're using the water shader with depth buffer based effects enabled, tick this.

    **Survival Ignores TRM Floor**: Sets whether the virtual environment will ignore the TRM floor and not automatically configure materials for it.

    **Hide Vanilla Checkpoint**: If enabled this will disable the checkpoint laser meshes so you can create your own.

    **Skybox Material**: A reference to the Unity skybox material if you're not using a mesh skybox. For legacy reasions the game will always use a black background when the scene loads even if you set a skybox in Unity's lighting tab, so make sure you assign this on top of Unity's lighting tab skybox.

    **Manually Distance Culled Objects**: A list of manually defined objects to cull if using the retro distance option. Use this for cases where you're not using a shader that supports BNGs retro settings.

    ======================      ===========================================================================
    Setting                     Description
    ======================      ===========================================================================
    Preview Image                | This setting controls whether fog will be enabled on this track or not. 
                                | Leave this disabled to have the game use the Unity scene fog settings.
    Fog Color                   The color that the fog will use.
    Fog Start/End Distance      | The start/end distances where the fog will be 
                                | fully transparent and fully opaque.
    ======================      ===========================================================================


Effects And Post Processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^
    **Weather Prefab**: The weather prefab to use for the track. Leave as Clear if you want to create your own weather with a :ref:`unity-tools-weather-controller`.

    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    None                        No weather effects will be used.
    Rain                        Standard rain with ambient rain sounds.
    Snow                        Standard snow with no ambient sounds.
    Thunder                     Standard rain with ambient rain sounds and ocassional thunder strikes.
    ======================      ============================================================================

Fog Settings
************
Fog settings is a depreciated feature and you should use Unity's built in settings instead.


    **Fog Enabled**: Whether fog is enabled. If you set up fog in Unity's lighting window then disable this.

    **Fog Color**: The color of the fog.

    **Fog Start Distance**: The distance from the camera where the fog starts.

    **Fog End Distance**: The distance from the camera where the fog ends.

Bloom Settings
**************
    **Bloom Config**: The config data exported from the in-game bloom config tool to use on the track. If left empty then the Bloom Prefab option is used instead.

    **Bloom Prefab**: If no Bloom Config is set then this sets which pre-made bloom prefab will be used on the track.

    ======================      ============================================================================
    Setting                     Description
    ======================      ============================================================================
    Wet                         Suited for overcast/rainy tracks.
    Snowy                       Suited for bright tracks with lots of white.
    Dim                         Suited for dark tracks.
    Bright                      Suited for bright tracks.
    Neon                        Suited for dark tracks with lots of bright colors.
    ======================      ============================================================================


Sunshaft Settings
*****************

    **Sunshafts Enabled**: Whether sunshafts are enabled on the track.

    **Rely On Z Buffer**: Where the path tracing for the sun rays will use the depth buffer to figure out occlusion. This should be left on.

    **Resolution**: The quality of the sun shafts.

    **Blend Mode**: How the sun shafts are blended onto the screen (like Photoshop blend modes).

    **Shafts Caster**: Where the shafts are being casted from. If you're setting up a typical sun then make sure your casting object is very far away from the scene in the direction of your sun.

    **Threshold Color**: A threshold that determines the minimum color that is required for sun shafts to start being drawn.

    **Shafts Color**: The color of the sun shafts. This is basically the color of the fog causing the shafts to be casted.

    **Distance Falloff**: A scaler value for how long the sun shafts are.

    **Blur Size**: How much the sun shafts get blurred on the screen.

    **Blur Iterations**: How many times the sun shafts are blurred.

    **Intensity**: How bright the sun shafts are.

Event Hooks
^^^^^^^^^^^^^^^^^^^^^^^^^^^
    
Events are calls to scripts that you can setup directly in the Unity editor. The use case for these are pretty generic so there's a wide range of things you can do with them.

Countdown Events
*****************

    **On Countdown Three**: Triggered when the countdown hits three.

    **On Countdown Two**: Triggered when the countdown hits two.

    **On Countdown One**: Triggered when the countdown hits one.

    **On Countdown Go**: Triggered when the countdown hits go.


Race State Events
*****************

    **On Event Complete**: Triggered when the race has finished.