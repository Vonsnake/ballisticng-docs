.. _unity-tools-vlm:

Lighting
========
.. note::
    The lightmapper documented here is open source inside the Unity Tools and can be used for your own projects if you want. If you'd like a more generic version that isn't specialized for BallisticNG, you can grab it here:
    https://github.com/bigsnake09/Vertex-Lightmapper

.. note::
    The lightmapper does not handle instanced lightmaps by default. See the Lightmapper Options section below to see how to use  instanced lightmaps.
    
BallisticNG uses a custom written lightmapper for baking light information into mesh vertices. This page covers the new open source VLM 2.0. If you're still using the old version then it's highly reccomended that you upgrade, although an option to continue using it is present in the new bake window. How to upgrade to the new lightmapper is mentioned in the Bake Window section below.

Configuring Lights
------------------
**Ambient Lighting**

    Ambient lighting is a base color that everything will be tinted with. You can set the ambient lighting using the bake window (documented below) or by using Unity's lighting tab, which you can access through ``Window -> Rendering -> Lighting``

    VLM supports all three ambient light modes that Unity provides. The simplest mode is the color mode, where you can select a single solid color to use.

**Lights**

    * VLM uses Unity's lights to bake lighting
    * The supported types are point, spot and directional
    * The color and intensity fields can be used to control the color and brightness of the light
    * Setting the shadow type to soft or hard will bake shadows. Set it to off to not bake any shadows for the light. The shadow intensity option also works

**Baking**

    Baking is done through the bake window. See below for more details.

Exposing Objects
----------------
Objects you want to lightmap must be exposed to the lightmapper first. This is done the same way you prepare objects for Unity's built in lightmapper, by setting them to lightmap static.

.. note::
    Tools to make this process easier are provided. See the Bake Window section below for more details.

Lightmapper Options
-------------------
**Add Component Path**: ``BallisticNG -> Rendering -> Lightmapping Options``

The lightmapping option script allows you to configure specific baking properties for the object you attach them to. Please note that there is also a legacy version of this script that is used for the legacy lightmapper, suffixed with (legacy). If you're using the new lightmapper, make sure you don't attach that one by accident if you search for the component.

    **Ignore Lightmapper**: Even if exposed to the lightmapper, this object will be ignored and will not have lighting baked for it. Useful if you want to generically flag out all objects for baking and then pick out a few without removing their lightmap static flag.

    **Cast Shadows**: Whether this object will cast shadows.

    **Recieve Shadows**: Whether this object will recieve shadows.
    
    **World Up Shadow**: Whether shadows will be checked for by casting directly up instead of in the direction of the light.

    **Encode Into Tangents**: The light information will be baked into the vertex tangent data instead of the vertex color. This is used with the advanced terrain shaders.

    **Use Vertex Stream**: Enables instancing using Unity's additional vertex stream feature. Ticking this allows you to re-use the asset multiple times in the same scene and have each object recieve it's own lightmap data.

Light Sponges
-------------
| **Add Component Path**: ``BallisticNG -> Rendering -> Light Sponge``
| **Menu Item Path**: ``GameObject -> Light -> Light Sponge``

Light sponges allow you to absorb ambient and directional light for scenarios like tunnels where you'll probably want there to be no sky light. They will not absorb point and spot light, allowing you to use them in conjunction with what would be man made light sources in your scene.

How the light sponge absorbs light is calculated as an attenuation, meaning they're strongest at the center and fade out over their area of effect.

    **Intensity**: How much light to absorb.

    **Ignore Reverse Normals**: Whether the light sponge will ignore all geometry facing away from it. This is useful if you have a tunnel that's close to the surface and you want to use a bigger sponge without it affecting the scenery outside.

    **Shape**: The shape of the light sponge.

    **Shere Radius**: When in sphere mode, this is the radius of the sphere.

    **Box Bounds**: When in box mode, this is the size of the area that will be absorbed.



GI Areas
--------
| **Add Component Path**: ``BallisticNG -> Rendering -> Global Illumination Area``
| **Menu Item Path**: ``GameObject -> Light -> GI Area``

Global Illumination Areas allow you to generate a single light bounce from a directional light. This uses voxels to determine the origin and orientation of bounces and as such can very quickly become memory intensive and CPU heavy.

Depending on the bounce light settings in the bake window, GI light can bounce out of the GI area to affect scenery outside of them. It's reccomended you keep the default scale of 100, 100, 100 and place these about sparringly in key locations as to not dramatically increase your bake times.

    **Light Source**: The directional light that will be used for baking the light bounces.

    **Resolution**: The size of the area that will be affected. The bigger this area, the more voxels that will be needed in calculations.

Bake Window
------------
**Open Path**: ``BallisticNG -> Vertex Lightmapper``

The bake window is where you configure bake settings and start the bake itself.

Settings
^^^^^^^^

    **Bounce Light**: Whether GI areas will be used or not.

    **Bounce Distance**: How far bounced light will travel.

    **Bounce Intensity**: How intense the bounced light will be.

    **Bounce Cone Angle**: How wide the cone of light that is bounced will be.

    **Bounce Cone Minimum Distance**: The minimum distance between bounce cones. Avoid setting this below 30 as it will increase bake times and memory requirements.

    **Shadow Bias**: How far away from vertices shadow bake checks will happen. Reccomended you leave this at 0.05.

    **Backface Shadows**: Whether backfaces can cast shadows.

Ambient Settings
^^^^^^^^^^^^^^^^
The ambient source setting is the same as Unity's ambient modes that you can find in the lighting tab. ``Window -> Rendering -> Lighting Settings``

Setup Scene
^^^^^^^^^^^
The setup scene tab includes three buttons to configure your track for the lightmapper.

    **Expose Selection**: This will expose any selected objects to the lightmapper. If you have anything with child objects selected, you will be asked if you would also like to expose those too. It's reccomended that you select the parent of your imported scenery and expose that, allowing the lightmapper to expose all child objects too.

    **Unexpose selection**: This will hide any selected objects from the lightmapper. This will also give you the option to hide all child objects.

Upgrade Components From legacy Lightmapper
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This button will perform an upgrade of lightmapper options and light sponges in your scene. After doing this you will also need to expose all objects you want lightmapped.

Bake
^^^^

    **Use Legacy Lightmapper**: Whether to use the old lightmapper or not.

    **Bake Lighting**: Start the light bake.