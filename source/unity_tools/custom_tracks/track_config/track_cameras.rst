Track Cameras
=============
Track Cameras are locations in your track that will be used for the post-race sequence. Each camera can be setup to either look at ships or maintain a fixed orientation.

To place a camera, first drag the camera prefab into your scene. You can find this in ``BallisticNGTools -> Prefabs -> Track Camera``. From here you will want to duplicate it and move copies of it around the scene where you would like cameras to sit.

**To make this process faster:**

* Select the camera prefab in your scene
* Move the scene view camera where you would like it to be. If you want it to use a fixed orientation, also make sure you are looking where you want the in-game camera to look.
* Press Ctrl + Shift + F to move and rotate the camera to the scene view camera
* If you want to use a fixed orientation, change the behaviour option in the inspector to **fixed orientation**
* Press Ctrl + D to duplicate the camera
* Rince and repeat until you are done

After you are happy with your camera positions, that's it! The game will automatically find and register the cameras for the post-race sequence.
