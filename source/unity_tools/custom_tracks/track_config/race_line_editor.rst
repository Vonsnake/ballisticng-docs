Racing Line Editor
==================
**Open Path**: ``BallisticNG -> Track Configuration -> Racing Line Editor``

.. note::
    Before racing lines work they **must** be assigned the AI difficulty levels you want them to be used in. More details on this are below.

Racing lines define offsets along each section of your track that the AI will aim to follow. These allow you to create more human like paths that that the AI will follow for different AI difficulty levels. The racing lines that an Ai will pick are random and they will sometimes switch lines mid-race if they decide to overtake somebody.

It's important to note that racing lines are only a guide and the AI have some elements of randomness that further affect their racing lines. If you don't configure racing lines, the game will generate a centered racing line by default.

Create A New Racing Line
------------------------
To create a new racing line click either the plus button or the auto-generate new line button.

    **Plus Button**: Creates a new racing line that's centered in the middle of the track.

    **Auto-Generate New Line**: Creates a new racing line with a more optimal path. (reccomended option)

        * **Flip Results**: Flips the offsets of the generated racing line so they're on the opposite side of the track.
        * **Iterations**: How many times the algorithm will run through the track.

Edit Racing Lines
-----------------
.. note::
    If you've like to globally smooth your racing line, select the racing line and then click the smooth button under the racing line list.

To edit a racing line you first need to select it.

* Click on the racing line's name button to select it
* Clicking the (o) button next to it will toggle its visibility
* Clicking the - button next to it will remove it

Drag Mode
^^^^^^^^^

In drag mode you can just paint the offsets on the track. Hover over a section and you will see a blue cube under your mouse cursor. Hold the left mouse button down to set the offset to where your mouse cursor is.

There are a few hotkeys to aid you in painting the racing line. All operations should be done along the tracks direction.


===================   =====================================================
Hotkey                Description
===================   =====================================================
Ctrl + Left Click     | Interpolate offset between last
                      | and currently being dragged sections.

Alt + Right Click     | Create straight line between last
                      | and currently being dragged sections.

Tab                   Smoothen selection
===================   =====================================================

Handle Mode
^^^^^^^^^^^
In handle mode you select a section and then move the section with no limits.

* Hover your mouse over a section and left click
* Hold down ctrl and move your mouse left/right

As this mode doesn't limit you to the width of the track floor, you can use it to create racing lines off of the track if you have scenery extensions.

Assign Difficulty Levels To Racing Line
---------------------------------------
Before a racing line can be used by an AI it must be assigned the AI diffilculty levels that it will used in.

* Select the racing line you want to setup the difficulties on
* Click the add level button
* If you'd like to make it available to all levels, keep clicking the add level button and it will automatically populate the racing line with all levels

Test Racing Lines
-----------------
The game has a few commands that you can use to test your racing lines. Build your track, open it up in-game and then:

* Open the console using ctrl + backspace
* Enter ``debug_enabled true`` to enable debugging mode
* Enter ``gm_playercontroller ai`` to hand the player ship over to the ai
* Enter ``ai_setplayerline #``, where # is the zero indexed racing line to swap the AIs racing line to.