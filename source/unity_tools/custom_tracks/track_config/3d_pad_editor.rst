.. _unity-tools-3d-pad-editor:

3D Pad Editor
=============
.. note::
    3D pads will only be active if:

    * You're in 2159 or floor hugger with no boost/weapon tiles placed or have the mixed pads option in scene references enabled
    * You're in 2280

**Open Path**: ``BallisticNG -> Track Configuration -> 3D Pad Editor``

The 3d pad editor allows you to quickly place down 3D pads in your scene. Opening it will create and select a gameobject where you can figure settings in its inspector.

Usage
-----
Upon opening the editor any scenery floor will have a temporary mesh collider attached to it. These mesh colliders won't be saved with the scene.

By default the editor will load the flat speed and weapon pads and start in speed pad mode. You can change the pad prefabs by setting the ``Speed Pad Prefab`` and ``Weapon Pad Prefab`` fields in the inspector and then clicking ``Update Prefab``.

* Click to place a pad that alligns with the nearest section to the pads position. Hold down the ``Shift`` key when clicking to ignore the sections direction.
* After clicking you will enter rotation mode. Move the cursor around the pad to adjust its rotation and click again to commit the adjusted rotation or press the ``Escape`` key to commit without the adjusted rotation.
* Press the ``T`` key to cycle the pad mode, or use the ``Pad Type`` dropdown in the inspector.
* While holding down the ``CTRL`` key the editor will be in delete mode. Hover your mouse over a pad and click to delete it. If you make a mistake, you can undo with ``Ctrl + Z``.
* You can adjust the offset from the ground using the ``Ground Offset`` setting in the inspector.

If you want to replace pads in the scene you can use the ``Replace Prefabs`` button. This will ask you whether you want to replace speed pads, weapon pads, or both. Any pad objects that are disabled will not be replaced, so you can use this to filter out particular pads to replace.

All of your 3d pads are placed under an object called ``3d Pads``.