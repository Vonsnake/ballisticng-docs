.. _unity-tools-ref-probe-gen:

Generate Reflection Probes
==========================
**Run Path**: ``BallisticNG -> Track Configuration -> Generate Reflection Probes``

The Generate Reflection Probes tool automatically generates reflection probes around your track for Ai ships and the players ship when using the low ship reflections quality to use. These are all generated to spec with the internal tracks.