Engines Setup
=============
.. note::
    There is no limit to how many engines you can setup!

    The engines color willl always be white in the editor unless you manuallly edit the material Colors you setup for your engine will be applied in-game only.

When you create a new ship you will be given a default engine to work with. An engine is a flare and trail that you add to your ships. You do not need both a flare and trail, if you only want a flare or only want a trail then you can safely delete the trail/flares gameobject and the game will simply not include it when loading your custom ship.

.. image:: img/engines.png
    :align: right

Adding Engines
^^^^^^^^^^^^^^

To add a engine open the ship inspector and expand the **Edit Engines** foldout. Click the green **Add Engine** button to add a new engine. You will need to edit the engine by selecting the gameobjects in the hiearchy tab and moving them manually. You can select multiple engines to move, rotate and scale both the flare and trail at the same time.

If you only want either the trail or flare then all you need to do is delete the gameobject.

|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|

Deleting Engines
^^^^^^^^^^^^^^^^
To delete an engine click the red X button on the header of the engine you want to delete. The gameobjects will be automatically deleted for you. 

Customizing Engines
^^^^^^^^^^^^^^^^^^^
To set the color of your engine open the ship inspector and expand the **Edit Visuals** foldout. The first two options you will see are **Engine Bright Color** and **Engine Dim Color**. Click on the color to open up the color picker. The engine color is animated using the Dim and Bright colors, this allows you to add a faint flicker or color shift to the engines color. The animation uses a sine wave over time to calculate the blend factor.

You can also add your own textures to the engine components. To do this you will want to create a new material and use that material on the flare/trail instead. For the trail use the ``Particles -> Additive`` shader and for the flare use the ``BallisticNG -> Ships -> Player Flare`` shader. The player flare shader is only used when the player is controlling the ship, the game automatically swaps out the shader to an AI flare shader.

Custom Engine Flare
^^^^^^^^^^^^^^^^^^^

To apply the custom materials select the flare/trail gameobjects, expand the **Materials** foldout in the **Mesh Renderer** component and drop the material into the material slot. 

.. image:: img/custom_flare_material.png

Further Customization
^^^^^^^^^^^^^^^^^^^^^
.. csv-table::
    :file: tables/table_engine_settings.csv
    :header-rows: 1