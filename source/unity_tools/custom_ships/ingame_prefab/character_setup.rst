.. _unity-tools-charactersetup:

Character Setup
===============

.. note::
	This page is coming soon. If you already know your way around Unity's Mecanim system then you can drive animations with the following variables:

	Steer, Pitch, Engine and Velocity

Once complete this page will cover the setup of animations in Blender and the process of exporting them, setting up a Mecanim graph and hooking it up to the ship.