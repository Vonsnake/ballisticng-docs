.. _unity-tools-ship-physics-stats:

Physics Stats Setup
===================
.. note::
	You can edit these stats in-game. Open the console using **Ctrl + Backspace**, enter **debug_enabled true**, close the console and then press F1 twice to bring up the ship stats screen.

	Keep in mind that these settings do not save. You will need to remember them and copy them out into your Unity ship prefab.

Physics stats is where you configure how your ship handles. To access these settings open the ship inspector and expand the **Edit Stats** foldout. Keep in mind that you can quickly prototype different settings in-game but leaving the game running in the background, exporting your ship and restarting the race to reload the ship file. 

Prefab Stats
^^^^^^^^^^^^
The editor comes with all of the in-game ships stats built in. Under the foldouts for entering your own stats you will find a section called **Update To Prefab Stats**. Select a ship from the dropdown and then click **Apply Stats** to update all of the stats on your ship. 

Stat Setup
^^^^^^^^^^
.. csv-table::
	:file: tables/table_physics_stats.csv
	:header-rows: 1