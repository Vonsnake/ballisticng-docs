Ingame Prefab
============

.. toctree::
    :caption: Custom Ships

    prefab
    mesh_setup
    material_setup
    airbrakes_setup
    locations_setup
    engines_setup
    liveries_setup
    physics_stats_setup
    character_setup
    other_customization