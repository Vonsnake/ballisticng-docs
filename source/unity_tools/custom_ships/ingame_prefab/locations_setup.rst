Locations Setup
===============
.. note::
    Hold down V when moving an object to vertex snap it.

Locations are offsets on your ship defined by gameobjects that are children of the ship prefab. To setup these locations you just need to move the gameobjects in the scene.

To access locations navigate to the ship inspector and expand the Edit Locations foldout.

Effects
-------
.. image:: img/locations.png
    :align: right

Vapor Trail locations are required, navigation lights are not and you can safely delete the gameobjects.

Vapor trails locations are used to tell the game where the ships most outer wing tips are, these should be placed on the part of the ship that extends out the furthest. Navigation lights are red (left) and green (right) lights that flash in an alternating pattern, ideally you'd place these on your ships wing tips alongside the vapor trail locations.

Camera
-------
The camera location fields allow you to adjust the offset of the external and internal cameras. The cockpit cameras position is defined by the cockpit meshes pivot. You can preview your chase camera offset by clicking the **Preview Chase Camera (game tab)** button and switching to the game view.

**Scene View Gizmos** Inside the scene view you will see some sphere gizmos with abbreviated labels, these are what each label stands for: 

.. csv-table::
    :file: tables/table_locations.csv
    :header-rows: 1