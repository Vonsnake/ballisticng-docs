Materials Setup
===============
Inside of the folder where you've placed your ship assets will be a sub-folder called **Materials**. This contains any imported materials from your ship and if you've maintained file names will also have the diffuse texture automatically added for you. By default this will be using Unity's standard shader and won't have any of the other maps setup so the materials must be configured manuallly.

Ships Exteriors
^^^^^^^^^^^^^^^
First select your material in the project view and in the inspector click the **Shader** dropdown and select the ``BallisticNG -> CustomShip`` or ``BallisticNG -> CustomShipCull`` shader, depending on your preference. Select your material from the project view and in the inspector drag your maps into the appropriate **Reflection** and **Illumination** slots. If the diffuse hasn't been automatically setup then drag over your diffuse map too.

Your ships material should be configured to reflect the default livery. Liveries are applied by updating the texture maps on this material.

.. image:: img/textures_to_material.png

Cockpits
^^^^^^^^
For cockpits set the shader to ``BallisticNG -> VertexLit Transparent``, this is the transparent shader that the vanilla cockpits use. 