.. _unity-tools-airbrakessetup:

Airbrakes Setup
===============
.. note::
    There are no airbrake count restrictions, you can add as many as you like!

    Make sure your airbrakes are using the same material as the exterior mesh. This is important for assigning liveries!

Pivots
------
Before you begin configuring your airbrakes you must first make sure that your airbrakes pivots are setup. This must be done inside of your modeling software, each software has its own way of handling this so you'll need to use your softwares method of setting this up.

Take the image below as an example, the pivot is setup along the back edge of the airbrake with the Y axis pointing down it. The axis that's pointing down the edge of the airbrake is important as it will determine how you setup the rotation axis. As the Precision Deltas airbrake pivot has the Y axis ponting down the edge, this means we will be rotating our airbrake along the Y axis. 

.. image:: img/airbrake_axis.png

Create The Airbrake Definition
-------------------------------
* Head over to the ship inspector
* Expand the **Edit Airbrakes** section
* Click the green **Add Airbrake** button
* Under the **Target Components** header drag your airbrake gameobject into the **Airbrake Renderer** and **Airbrake Transform** fields.

.. image:: img/airbrake_setup.png

Configure The Airbrake
----------------------
You can now configure the airbrake using the available settings under the **Target Components** section. You might need to invert the rotation axis for the left airbrake, to do this you set the axis it's rotating along to -1 or you can use the **Quick Axis Assign** button to set this up. 

.. csv-table::
    :file: tables/table_airbrake_settings.csv
    :header-rows: 1

