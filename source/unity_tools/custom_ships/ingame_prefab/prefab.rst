Setup Prefab & Scale Helpers
============================

Create the Prefab
-----------------
To create a new ship prefab navigate to the titlebar and then navigate to ``BallisticNG -> Ships -> Create New Ship``. Once the prefab is created you will see a new default engine, vapor trail locations, navigation light locations and camera location notes.

In the hiearchy tab click on the object called **Ingame Ship**, in the inspector you will now see the main interface where you can configure everything. From this point on the tutorial will refer to this specific inspector as the **ship inspector**, when reading about this inspector you want to make sure you have the Ingame Ship object selected. 

.. image:: img/empty_ship.png

Scale Helpers
-------------
Scale helpers are ghost overlays of the vanilla ships, these are useful for scaling your own ships to the games. To activate them first make sure the ship inspector is open, change the **Scaler Helper References** option under the **Editor Settings** heading and click the **Update Scaler Helper** button to apply the changes. You may need to interact with the scene view to force it to refresh with the updated scale reference.

.. image:: img/scaler_helper.png