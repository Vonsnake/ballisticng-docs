Advanced Customization
===================

Engine Sound
^^^^^^^^^^^^
To apply an engine sound to your ship first import the audio file you want to include, you should make sure it's a .wav or .ogg file. Next open the ship importer and drag your sounds asset onto the **Engine Sound** field or click the circle icon next to it and select your audio asset from the object browser window. 

.. image:: img/engine_sound_setup.png

Custom Modules
^^^^^^^^^^^^^^
Ships in BallisticNG are handled through a system that seperates behaviours into different modules that are executed in an explicit order and can intercommunicate with each other. Custom modules allows code mods to add their own behaviour modules into the game that custom ships can then reference.

Once you have a custom module programmed in a code mod (or you've downloaded one) you reference the name that the module registers into the game and use the Custom Ship Module field in the ship inspector to tell the game which module to load. If the module doesn't exist or the name isn't correct the game won't load anything.

Keep in mind that for this system to work you will need to install and active code mods. If you're programming your own code mod for this feature it's strongly reccomended that you distribute your c# source files instead of libraries so others can inspect your code. 

Build In Modules
----------------

INTERNAL_DRAG
***********
The ship module used for drag ships. Contains several user variables to customize it (see section below for more details).

.. csv-table::
	:file: tables/vanilla_modules/internal_drag.csv
	:header-rows: 1



User Variables
^^^^^^^^^^^^^^
User Variables allow you to enter custom data into a ship that custom ship modules can use. You can add them under the **Edit User Data** section of the **Ingame Ship** inspector.

.. csv-table::
	:file: tables/table_uservar_types.csv
	:header-rows: 1