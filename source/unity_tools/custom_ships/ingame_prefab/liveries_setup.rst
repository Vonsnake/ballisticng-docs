.. _custom-ships-liveries:

Liveries Setup
==============
.. note::
	 Not all texture maps are required for each livery. At the very least you must have the diffuse map though.

To create a new livery for your ship open the ship inspector and expand the **Edit Liveries** foldout. The new livery will be created and added to the list. Use the Livery Name field to set the menus display name of the livery and then set your texture maps below. Liveries only support the game's ship and standard shader unless you use the material override. See below for more information.

To delete a livery just click the red X button to the right of the liveries header. 

.. image:: img/livery_setup.png

Campaign Unlocks
-----------------
The campaign unlock name field allows you to lock the livery behind a gold/platinum completion of a campaign. This can be either an offical or custom campaign, but the name typed here must be the name of the campaign (case sensitive). Leave this empty and the livery will always be unlocked.

Material Overrides
------------------
Material overrides allow you to set the material that will be used for a specific livery. This applies both in-game and on the menu.

Make sure you have the textures for the liveries assigned in the materials.