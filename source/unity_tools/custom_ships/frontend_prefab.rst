Frontend Prefab
=============

The frontend is the main menu and any other menus where you will be able to select your ship. It is important that you configure these so players know a little bit about your ship before selecting it.

Setting up your frontend mesh is identical to setting up your in-game ship, instead this time you want to drag your frontend mesh onto the Frontend Ship object. Once you've dragged the ship in you'll want to set the position, rotation and scale to match the in-game ship but multiply the scale by 10. Unity supports math functions in text fields so you can you can do the multiplication by adding ``*10`` to the end of your scale values. Right clicking the transform component header will also let you copy and paste transforms to and from different objects, this can be useful for very quickly getting your frontend ship configured.

For your own convenience you might also want to disable the frontend mesh's object to hide it.

Once the frontend ship is in your scene and scaled properly you will now also want to setup the reference to it so the game can read it. Open the ship inspector and expand the **Edit Frontend Data** foldout. Now drag your frontend ship object onto the **Frontend Mesh** field.

Finally you can configure your display name, display decription and out of 10 stats which will be shown on the menu. 

.. image:: ingame_prefab/img/drag_mesh_to_frontend.png

.. image:: ingame_prefab/img/assign_frontend.png