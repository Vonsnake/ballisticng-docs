Asset Requirements
===================
.. note::
    The exterior in-game and frontend ship meshes are required assets. Cockpits are not.


BallisticNG is built with low poly assets with a single material in mind. This is a restriction you will need to keep in mind when developing your custom ship. Make sure that you meet the following requirements:

* Your ship is a single mesh (sub-meshes are fine)
* Your ship only has a single material. This material also needs to be shared across the airbrakes.
* Cockpits use their own material
* Cockpit meshes should occupy the real space of your ships cockpit for VR players. The pivot of the cockpit is where the camera will be placed, so make sure it's setup in your modeling software before exporting.
* There should be two versions of your cockpit: one with and one without the canopy. This allows your ship to support the game's canopy option.
* If showing a ships exterior from inside the cockpit it must be part of the cockpit mesh and must use the external ship meshes material. In doing this you'll have 2 materials: one for the cockpit interior and the material for your ships exterior).
* Is using airbrake meshes for animation you need a second version of your ship with the airbrake meshes included in the mesh for the menu.
* Separated airbrakes need to have their pivots configured. Configuration for this is mentioned in later pages.
* The ships pivot should somewhere in the center of the the mesh. This is important for shadow generation and will be explained later.
