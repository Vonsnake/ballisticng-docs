Asset Specification
===================

If you'd like to follow BalllisticNG's asset specifications, here they are:

* Meshes are typically 150-250 tris.
* Exterior texture maps are 512x512. Cockpit interior maps are 1024x1024
* All ships have a diffuse, illum and reflection mask map.