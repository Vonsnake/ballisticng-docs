Asseting Importing
==================
Import your assets into Unity like you would any other asset. 

    * For meshes favour FBX or DAE over other formats like OBJ. OBJ doesn't store transform data and as such airbrake pivots will be lost so it's strongly reccomended you avoid OBJ.
    * For textures you can use any format you like, PNG and TGA are the typically the most used though.
    * Once you've imported your textures, click on your textures in the project view and in the inspector untick Generate Mip Maps, set Filter Mode to Point (no filtering) and click apply.
    * If your ship scale is off by multiples of 10 then it's reccomended you set the import scale factor first instead of scaling them in your ship prefab. To do this click on your meshes in the project view and in the inspector update the Scale Factor setting, then click apply.