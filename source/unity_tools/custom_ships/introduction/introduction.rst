Introduction
===============

.. note::
    Keep your ship scene files safe! These act as your ship source files.

Custom ships are prefabs made inside of Unity scenes and exported to .csf files. Unlike the previously used .shp format, these are compiled Unity assets and cannot be modified by other people once exported. Your source ship files are the unity scenes that you save them in, it's important you keep your scene files safe for this reason! If you like to keep yourself organized you can store multiple ships per scene and export them individually from that same scene.

Ships are split into two seperate prefab assets: ingame and frontend ships. The ingame ship is what's spawned in-game, everything in this prefab will be loaded into the game so that means anything you setup in addition to the required assets will also be included. While this means you get some freedom with adding extra stuff to your ship, we don't reccomended you go too overboard with intensive effects like particles for the sake of keeping the game playable for people on older hardware. The frontend ship is the asset that's loaded when the ship needs to be displayed on the menu, that's pretty much it.

Finally .csf files can be re-exported as you're playing the game! Frontend data will not be updated as that information is cached for your current play session, but the in-game prefab will update when you restart the event that you're currently playing. This can be useful for prototyping physic stats. Stat prototyping can also be done in-game. See :ref:`unity-tools-ship-physics-stats` for more information.