Importing Legacy .shp Files
===========================
.. note::
    If the .shp file is tied to the Steam Workshop then you can only import the file if you published it. Make sure you're signed into Steam when importing your files for it to verify that you are the owner.

* Create a new empty Unity scene
* On the toolbar navigate to ``BallisticNG -> Ships -> Import From .SHP File`` and select a .shp file. Your assets will now be imported into Unity and prefabs for the ship will be automatically created for you.
* Adjust the ships if you need to. You will need to adjust the frontend mesh scale at least.
* Save the scene with your ship

All of your workshop data is preserved in the import, so you can go ahead and upload the ship to update your existing ship on the workshop.